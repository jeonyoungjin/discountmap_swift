//
//  NetworkManager.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 8. 28..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import Alamofire
import SDWebImage
import Appboy_iOS_SDK

fileprivate enum ResultCode : Int {
    case kSuccess                            = 1
    case kGoWebView                          = 101
    case kOpenWebViewLayer                   = 102
    case kOperWebViewLayerWithOutCloseButton = 103
    case kNotCertificatedUserLogin           = 301
    case kErrorWithAlert                     = 401
    case kErrorWithAlert2                    = 402
    case kErrorWithAlertAndPrevPage          = 901
    case kErrorWithAlertAndLogout            = 902
    case kErrorWithAlertAndLoginPage         = 903
    case kErrorLogOutAndMainPage             = 904
}

typealias NetworkResponse = (Array<Any>, Error?) -> Void

final class NetworkManager {
    
    let shared = NetworkManager()
    
    private init() {}
    
    class func sendOrderInfo(orderId:String) {
        Alamofire.request("http://m.wemakeprice.com/m/order/get_order_info_for_ga/\(orderId)",
            method: .get, parameters: nil).responseJSON { response in
                
                if let value = response.value as? [String: AnyObject] {
                    
                    if let orderlist = value["order_info"] as? [Dictionary<String, Any>] {
                        
                        var orders = [GAOrderInfo]()
                        for order in orderlist {
                            
                            let data = GAOrderInfo.init(dict: order)
                            orders.append(data)
                        }
                        
                        let paymentInfo = GAPaymentInfo.init(dict: value["payment_info"] as! Dictionary<String, Any>)
                        
                        TrackingManager.sendOrderTrackingWithOrderList(orderList: orders, paymentInfo: paymentInfo)
                    }
                }
        }
    }
    
    class func getMyData( completion : NetworkResponse?) {
        
        Alamofire.request("\(SERVER_HOST)/my", parameters: ["token": UserPreferenceManager.getAccessToken() ?? "",
                                                            "version" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] ?? "",
                                                            "deviceId" : UserPreferenceManager.getDeviceId() ?? ""]).responseJSON { response in
            
            guard validateNetworkResponse(response: response) == true else {
                completion?([], response.error)
                return
            }
            
            let value = response.value as! [String: AnyObject]
            if let data = value["data"] as? [String: AnyObject] {
                
                let myData = MyData(dict: data)
                UserPreferenceManager.setApnsStatus(isOn: (myData.enablePush?.intValue != 0 ? true : false))
                
                if let uid = myData.uid {
                    Appboy.sharedInstance()?.changeUser("WM_" + uid)
                }
                
                completion?([myData], response.error)
            }
        }
    }
    
    // 푸시상태를 변경한다.
    class func setPushStatus(isOn: Bool, completion:NetworkResponse? = nil) {
        
        Alamofire.request("\(SERVER_HOST)/preferences/push",
            method:(isOn == true ? .post : .put),
            parameters: ["deviceId": UserPreferenceManager.getDeviceId() ?? "",
                         "mobileToken" : AppDelegate.deviceTokenString ?? "" ]).responseJSON { response in
                            
                            if let value = response.value as? [String:Any], let msg = value["message"] as? String {
                                completion?([msg], response.error)
                            }
        }
    }
    
    class func initialzeAppData( completion : NetworkResponse?) {
        Alamofire.request("\(SERVER_HOST)/init", parameters :
            ["version": Bundle.main.infoDictionary?["CFBundleShortVersionString"] ?? ""]).responseJSON { response in
                
                guard validateNetworkResponse(response: response) == true else {
                    completion?([], response.error)
                    return
                }
                
                if let value = response.value as? [String: AnyObject] {
                    
                    let data = value["data"] as! [String: AnyObject]
                    
                    // intro
                    let rawIntro = data["intro"] as! Dictionary<String, Any>
                    
                    // gnb
                    let rawGNB = data["gnb"] as! [Dictionary<String, Any>]
                    var gnbArray = [GNBData]()
                    for item in rawGNB {
                        
                        let parseItem = GNBData(dict: item)
                        
                        gnbArray.append(parseItem)
                    }
                    
                    // hotplace
                    let rawHotPlace = data["hotplaces"] as! [Dictionary<String, Any>]
                    var hotPlaceArray = [HotPlaceData]()
                    for item in rawHotPlace {
                        
                        let parseItem = HotPlaceData(dict: item)
                        hotPlaceArray.append(parseItem)
                    }
                    
                    // marker
                    let rawMarker = data["markers"] as! [Dictionary<String, Any>]
                    var markerArray = [MarkerData]()
                    for item in rawMarker {
                        
                        let parseItem = MarkerData(dict: item)
                        markerArray.append(parseItem)
                    }
                    
                    // filter category
                    let rawFilterCategory = data["categories"] as! [Dictionary<String, Any>]
                    var filterCategoryArray = [FilterCategoryData]()
                    for item in rawFilterCategory {
                        
                        let parseItem = FilterCategoryData(dict: item)
                        filterCategoryArray.append(parseItem)
                    }
                    
                    
                    //                // hash tag
                    //                let rawHashTag = data["tags"] as! [Dictionary<String, Any>]
                    //                var hashTagCategoryArray = [FilterTagData]()
                    //                for item in rawHashTag {
                    //
                    //                    let parseItem = FilterTagData(dict: item)
                    //                    hashTagCategoryArray.append(parseItem)
                    //                }
                    
                    let rawRecommend = data["recommends"] as! Dictionary<String, Any>
                    let recommend = RecommendData(dict: rawRecommend)
                    
                    completion?([gnbArray, hotPlaceArray, markerArray, filterCategoryArray, rawIntro, recommend] , response.error)
                }
        }
    }
    
    class func getDealList(param : Dictionary<String, Any>, uniqueList : NSOrderedSet, addCategory: Bool, completion : NetworkResponse?) {
        
        var metaData : DealMetaData? = nil
        var location : CLLocationCoordinate2D? = nil
        var titlePropertyName : String? = nil
        var bannerList = [BannerData]()
        var sectionTitle: String?
        var useMap: NSNumber?
        var useRanking: NSNumber?
        var originalDataList = [CategoryDealData]()
        var listColumn: NSNumber?
        var headerData: HeaderData?
        var _param = param
        
        typealias TaskBlock = () -> ()
        var recursive: TaskBlock!
        
        let newResponse : NetworkResponse = { (result, error) in
            
            guard error == nil else {
                
                print(error?.localizedDescription ?? "")
                
                completion?([originalDataList, metaData as Any, location as Any, titlePropertyName as Any, bannerList, sectionTitle as Any, useMap as Any, useRanking as Any, listColumn as Any, headerData as Any], error!)
                
                return
            }
            
            // [1] 데이터 누적
            let dealList = result[0] as! [CategoryDealData]
            if metaData != nil {
                if let newMetaData = result[1] as? DealMetaData {
                    
                    if (metaData?.page?.intValue)! < (newMetaData.page?.intValue)! {
                        metaData = newMetaData
                    } else {
                        //                            assert(false, "WOW!!!!!!!!!!!!!!")
                    }
                }
            } else {
                metaData = result[1] as? DealMetaData
            }
            
            location = result[2] as? CLLocationCoordinate2D
            titlePropertyName = result[3] as? String
            bannerList.append(contentsOf: result[4] as! [BannerData])
            sectionTitle = result[5] as? String
            useMap = result[6] as? NSNumber
            useRanking = result[7] as? NSNumber
            listColumn = result[8] as? NSNumber
            
            if let header = result[9] as? HeaderData {
                headerData = header
            }
            
            // [2] 기존 데이터 카운트 저장
            let prevDealCount = uniqueList.count
            
            
            // [3] 새로 요청한 데이터 누적
            originalDataList.append(contentsOf: dealList)
            
            // [4] 기존데이터 + 새로요청한 데이터 고유한 dealId set todtjd
            var testArray = originalDataList.map{$0.dealID} as! [NSNumber]
            testArray.append(contentsOf: uniqueList.array as! [NSNumber])
            
            let testSet = NSSet(array: testArray)
            
            if testSet.count < Int(Double(prevDealCount) + Double((metaData?.limit?.intValue)!) * 0.7) && dealList.count != 0 && ((metaData?.totalCount?.intValue)! > (metaData?.limit?.intValue)!) {
                
                recursive()
            } else {
                
                completion?([originalDataList, metaData!, result[2], result[3], bannerList, sectionTitle, useMap, useRanking, listColumn, headerData], error)
                
                return
            }
        }
        
        recursive  = {
            
            if metaData != nil {
                
                _param["page"] = NSNumber(value: (metaData?.page?.intValue)! + 1)
            }
            
            NetworkManager.getDealList(url: _param["requestURL"] as! String,
                                       parameter: ["lon" : _param["lon"] ?? CLLocationDegrees.init(0),
                                                   "lat" : _param["lat"] ?? CLLocationDegrees.init(0),
                                                   "c" : _param["c"] as! String,
                                                   "page" : _param["page"]],
                                       completion: newResponse,
                                       addCategory: addCategory)
        }
        
        recursive()
    }
    
    
    fileprivate class func getDealList(url : String, parameter : Dictionary<String, Any>, completion : NetworkResponse?, addCategory  : Bool) {
        
        var param = parameter;
        
        let lon = param["lon"] as! CLLocationDegrees
        let lat = param["lat"] as! CLLocationDegrees
        
        if Int(lon) == 0 && Int(lat) == 0 {
            
            param.removeValue(forKey: "lon")
            param.removeValue(forKey: "lat")
        }
        
        Alamofire.request(url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, parameters : param).responseJSON { response in
            
            guard validateNetworkResponse(response: response) == true else {
                
                completion?([], response.error)
                
                return
            }
            
            let value = response.value as! [String: AnyObject]
            if let data   = value["data"] as? [String: AnyObject] {
                
                guard let _ = data["deals"] else {
                    
                    completion?( [], NSError.init(domain: "", code: 0, userInfo: nil))
                    return
                }
                
                // 현재위치를 못가져 왔을때 서버에서 내려준 값으로 설정
                var serverLocationValue: CLLocationCoordinate2D?
                if let latitude  = data["lat"] as? NSNumber, let longitude = data["lon"] as? NSNumber{
                    serverLocationValue = CLLocationCoordinate2DMake(latitude.doubleValue, longitude.doubleValue)
                }
                
                
                // 딜 메타데이터
                var metaData: DealMetaData? = nil
                if let metaInfo = value["paging"] as? Dictionary<String, Any> {
                    metaData = DealMetaData(dict: metaInfo)
                }
                
                
                // 타이틀 프로퍼티
                let titlePropertyName = data["nameField"] as? String
                
                // 배너 정보
                var bannerList = [BannerData]()
                if let rawBannerList = data["banner"]?["dealListTop"] as? [Dictionary<String, Any>] {
                    
                    for bannerItem in rawBannerList {
                        
                        let banner = BannerData(dict:bannerItem)
                        bannerList.append(banner)
                    }
                }
                
                // 섹션 타이틀
                let sectionTitle = data["title"] as! String
                
                // 맵 사용여부
                let useMap = data["useMap"] as! NSNumber
                
                // 랭킹 사용여부
                let useRanking = data["useRanking"] as! NSNumber
                
                // 랭킹 사용여부
                let listColumn = data["listColumn"] as! NSNumber
                
                
                var header: HeaderData?
                if let headBanner = data["header"] {
                    
                    if headBanner is NSNull {} else {
                        header = HeaderData.init(dict: headBanner as! Dictionary<String, Any>)
                    }
                }
                
                var result = [CategoryDealData]()
                if let rawResult = data["deals"] as? [Dictionary<String, Any>] {
                    
                    for dealItem in rawResult {
                        
                        let deal = CategoryDealData(dict: dealItem)
                        result.append(deal)
                        
                        //이미지 프리패칭
                        if deal.dealID?.boolValue == true {
                            SDWebImagePrefetcher.shared().prefetchURLs([URL.init(string: deal.imageUrl!)!])
                        }
                    }
                }
                
                completion?([result, metaData ?? "", serverLocationValue, titlePropertyName!, bannerList, sectionTitle, useMap, useRanking, listColumn, header as Any], response.error)
            }
        }
    }
    
    class func getSearchResult(parameter : Dictionary<String, Any>, completion : NetworkResponse?) {
        
        var param = parameter;
        
        let lon = param["lon"] as! CLLocationDegrees
        let lat = param["lat"] as! CLLocationDegrees
        
        if Int(lon) == 0 && Int(lat) == 0 {
            
            param.removeValue(forKey: "lon")
            param.removeValue(forKey: "lat")
        }
        
        let url = "\(SERVER_HOST)/search"
        
        Alamofire.request(url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, parameters : param).responseJSON { response in
            
            guard validateNetworkResponse(response: response) == true else {
                
                completion?([], response.error)
                
                return
            }
            
            let value = response.value as! [String: AnyObject]
            if let data   = value["data"] as? [String: AnyObject] {
                
                guard let _ = data["deals"] else {
                    
                    completion?( [], NSError.init(domain: "", code: 0, userInfo: nil))
                    return
                }
                
                // 현재위치를 못가져 왔을때 서버에서 내려준 값으로 설정
                let latitude  = data["lat"] as? String
                let longitude = data["lon"] as? String
                
                let serverLocationValue = CLLocationCoordinate2DMake(Double(latitude ?? "0")!, Double(longitude ?? "0")!)
                
                // 딜 메타데이터
                var metaData: DealMetaData? = nil
                if let metaInfo = value["paging"] as? Dictionary<String, Any> {
                    metaData = DealMetaData(dict: metaInfo)
                }
                
                // 타이틀 프로퍼티
                let titlePropertyName = data["nameField"] as? String
                
                // 배너 정보
                var bannerList = [BannerData]()
                if let rawBannerList = data["banner"]?["dealListTop"] as? [Dictionary<String, Any>] {
                    
                    for bannerItem in rawBannerList {
                        
                        let banner = BannerData(dict:bannerItem)
                        bannerList.append(banner)
                    }
                }
                
                var result = [CategoryDealData]()
                if let rawResult = data["deals"] as? [Dictionary<String, Any>] {
                    
                    for dealItem in rawResult {
                        
                        let deal = CategoryDealData(dict: dealItem)
                        result.append(deal)
                    }
                }
                
                // 섹션 타이틀
                let sectionTitle = data["title"] as! String
                
                // 맵 사용여부
                let useMap = data["useMap"] as! NSNumber
                
                // 랭킹 사용여부
                let useRanking = data["useRanking"] as! NSNumber
                
                // 랭킹 사용여부
                let listColumn = data["listColumn"] as! NSNumber
                
                
                completion?([result, metaData ?? "", serverLocationValue, titlePropertyName!, bannerList, sectionTitle, useMap, useRanking, listColumn], response.error)
                
            }
        }
    }
    
    class func validateNetworkResponse(response :DataResponse<Any>) -> Bool {
        
        guard let value = response.value as? [String: AnyObject], response.error == nil else {
            
            struct Temp {
                static var isShowAlert = false
            }
            
            if Temp.isShowAlert == false {
                if let vc = UIApplication.topViewController() {
                    
                    vc.showInfoAlert(title: nil, message: "서비스 연결상태가 좋지 않습니다.\n네트워크 환경을 확인해주세요", confirmTitle: "확인", positiveHandler: {
                        Temp.isShowAlert = false
                    })
                    
                    Temp.isShowAlert = true
                }
            }
            
            return false
        }
        
        
        if let resultCode = value["result"] as? Int {
            
            switch ResultCode.init(rawValue: resultCode)! {
                
            case .kSuccess:
                return true
                
            case .kErrorWithAlert,
                 .kErrorWithAlert2,
                 .kErrorWithAlertAndPrevPage,
                 .kErrorWithAlertAndLogout,
                 .kErrorWithAlertAndLoginPage:
                
                if let vc = UIApplication.topViewController() {
                    
                    if let message = value["message"] as? String {
                        vc.showInfoAlert(message: message)
                    }
                }
                break
                
            case .kGoWebView,
                 .kOpenWebViewLayer,
                 .kOperWebViewLayerWithOutCloseButton:
                // 웹뷰
                if let webVC  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as? WebViewController,
                    let url = value["action"] as? String {
                    
                    webVC.urlString = url
                    webVC.titleText = "안내"
                    webVC.showType = .exclusive
                    
                    if ResultCode.init(rawValue: resultCode)! == .kOperWebViewLayerWithOutCloseButton {
                        webVC.backButton.isHidden = true
                    }
                    
                    if let rootVC = UIApplication.shared.delegate?.window??.rootViewController {
                        rootVC.present(webVC, animated: true, completion: nil)
                    }
                }
                break
                
            default:
                break
            }
            
        } else {
            return false
        }
        
        return false
    }
}

