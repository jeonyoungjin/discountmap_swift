//
//  AppDelegate.swift
//  DiscountMapForSwift
//
//  Created by Young jin Jeon on 2017. 8. 5..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import GoogleMaps
import MapKit
import Fabric
import Crashlytics
import Firebase
import Appboy_iOS_SDK

extension Notification.Name {
    static let ISPOpenUrlResponse = Notification.Name("ISPOpenUrlResponse")
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    static var deviceTokenString:String? = nil
    
    func registerForNotifications(forTypes types: UIUserNotificationType = [.alert, .badge, .sound], categories: Set<UIUserNotificationCategory>? = nil) {
        
        guard AppDelegate.deviceTokenString == nil else {
            return
        }
        
        AppDelegate.deviceTokenString = nil
        let settings = UIUserNotificationSettings(types: types, categories: categories)
        UIApplication.shared.registerUserNotificationSettings(settings)
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        
        if notificationSettings.types == UIUserNotificationType.init(rawValue: 0) {
            ApnsManager.setAPNSStatus(false, isLocal: true)
        } else {
            ApnsManager.setAPNSStatus(true, isLocal: true)
        }
    }

    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken token: Data) {

        AppDelegate.deviceTokenString = String(format: "%@", token as CVarArg)
        Appboy.sharedInstance()?.registerPushToken(AppDelegate.deviceTokenString ?? "")
        
        ApnsManager.setAPNSStatus(ApnsManager.getAPNSStatus())
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // 캐시설정
//        SDImageCache.shared().maxMemoryCountLimit = 1000
        SDImageCache.shared().config.maxCacheAge  = 60 * 60 * 24 * 7
        SDImageCache.shared().config.maxCacheSize = 1024 * 1024 * 200
        SDImageCache.shared().config.shouldCacheImagesInMemory = false
        
        // 구글맵 설정 
        GMSServices.provideAPIKey(GOOGLE_API_KEY)
        
        // 패브릭 등록
        Fabric.with([Crashlytics.self])
        
        // 구글 애드워즈 전환 트래킹
        ACTAutomatedUsageTracker.enableAutomatedUsageReporting(withConversionID: "848458419")
        ACTConversionReporter.report(withConversionID: "848458419", label: "ugncCP3LlnMQs-XJlAM", value: "0.00", isRepeatable: false)
        
        // 파이어베이스 등록
        FirebaseApp.configure()
        
        // 앱보이
        Appboy.start(withApiKey: "5daf0b6a-d321-4f7b-a15c-dd8f0853768b",
                     in: application,
                     withLaunchOptions: launchOptions,
                     withAppboyOptions: [ABKPushStoryAppGroupKey:"883cd4b7-cd3a-4316-a739-0d47b38b78ae",
                                         ABKAppboyEndpointDelegateKey:self])
        
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        if url.absoluteString.contains("billgate.net") == true {
            
            let nsurlString = url.absoluteString as NSString
            let urlString = nsurlString.substring(from: 10)
            let url = URL.init(string: urlString)
            
            NotificationCenter.default.post(name: Notification.Name.ISPOpenUrlResponse, object: url)
        } else {
            
            // 단순 앱 실행
//            let urlString = "discountmap://doCommand?type=1"
//                        let urlString = "discountmap://doCommand?type=2&value1=http://www.wemakeprice.com/deal/adeal/2279023/100600"
//                        let urlString = "discountmap://doCommand?type=3&value1=\("제주도".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)"
//                        let urlString = "discountmap://doCommand?type=4&value1=\("맛집직배송".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)"
//                        let urlString = "discountmap://doCommand?type=5&value1=1"
//            let url = URL.init(string: urlString)
            
            DeepLinkManager.parseURLScheme(url: url)
        }
        
        return true
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

extension AppDelegate : ABKAppboyEndpointDelegate {
    
    func getApiEndpoint(_ appboyApiEndpoint: String) -> String {
        
        return appboyApiEndpoint.replacingOccurrences(of: "dev.appboy.com", with: "sdk.iad-03.braze.com")
    }
}


