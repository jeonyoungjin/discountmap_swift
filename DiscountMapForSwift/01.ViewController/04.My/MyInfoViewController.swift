//
//  MyInfoViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 13..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

class MyInfoViewController : UIViewController {
    
    @IBOutlet weak var contentTableView: UITableView!
    @IBAction func onClickLogout(_ sender: Any) {
        
        URLCache.shared.removeAllCachedResponses()

        let cookies = HTTPCookieStorage.shared.cookies
        for cookie in cookies! {
            if cookie.domain.contains("wemakeprice") {
                HTTPCookieStorage.shared.deleteCookie(cookie)
                UserDefaults.standard.synchronize()
            }
        }
        
        UserPreferenceManager.setAccessToken(token: "")
        
        LoadingViewManager.show()
        NetworkManager.getMyData { (result, error) in
            LoadingViewManager.close()
            
            DataManager.setMyData(data: result[0] as? MyData)
            self.contentTableView.reloadSections(IndexSet.init(integer: 0), with: UITableViewRowAnimation.automatic )
        }
        
        TrackingManager.E_1710120014(action: "Logoff")
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UIApplicationDidBecomeActive,
                                               object: nil, queue: nil) { [weak self](noti) in
                                                
                                                self?.contentTableView.reloadData()
        }
    }

    @IBAction func onClickEditProfil(_ sender: Any) {
        if let data = DataManager.getMyData()?.links?.editProfile {
            
            let webVC  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            // 빌더패턴 사용하자
            webVC.urlString = data.href
            webVC.titleText = "회원정보"
            webVC.showType = .exclusive
            
            self.navigationController?.pushViewController(webVC, animated: true)
        }
        
        TrackingManager.E_1710120014(action: "Edit Profile")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.contentTableView.estimatedRowHeight = 50
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        self.contentTableView.reloadData()
    }
    
    func isLoggedIn() -> Bool {
        return DataManager.getMyData()?.id == nil ? false : true
    }
    
    func getCellCount() -> Int {
        
        if let addCount = DataManager.getMyData()?.menu?.count {
            
            return 3 + addCount
        }
        
        return 3
    }
    
}

extension MyInfoViewController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 110
        }
        
        return 56
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.getCellCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            if isLoggedIn() {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyPageLogoutCell") as! MyPageLogoutCell
                cell.backgroundColor = cell.contentView.backgroundColor
                let data = DataManager.getMyData()
                cell.userName.setTitle(data?.name, for: .normal)
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyPageLoginCell")!
                cell.backgroundColor = cell.contentView.backgroundColor
                
                return tableView.dequeueReusableCell(withIdentifier: "MyPageLoginCell")!
            }
            
        case 1 ... self.getCellCount() - 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyInfoCommonCell") as! MyInfoCommonCell

            if  indexPath.row <= self.getCellCount() - 3,  let data = DataManager.getMyData()?.menu?[indexPath.row - 1] {
                
                cell.title.text = data.name
                cell.metaData = data
                
                return cell
            } else {
                
                if indexPath.row == self.getCellCount() - 2 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "MyInfoSwitchCell") as! MyInfoSwitchCell
                    cell.title.text = "알림"
                    cell.switchView.isOn = (DataManager.getMyData()?.enablePush?.boolValue)!
                    
                    if UIApplication.shared.isRegisteredForRemoteNotifications == false {
                        cell.switchView.isEnabled = false
                        ApnsManager.setAPNSStatus(false, isLocal: true, completion: nil)
                    } else {
                        cell.switchView.isEnabled = true
                    }

                    return cell
                }
                
                if indexPath.row == self.getCellCount() - 1 {
                    return tableView.dequeueReusableCell(withIdentifier: "MyInfoVersionCell")!
                }
            }
            
        default:
            break
        }
        
        return UITableViewCell.init()
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) as? MyInfoCommonCell {
            
            if let data = cell.metaData {
             
                let webVC  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                // 빌더패턴 사용하자
                webVC.urlString = data.link?.href
                webVC.titleText = data.name
                webVC.showType = .exclusive
                
                self.navigationController?.pushViewController(webVC, animated: true)
                
                TrackingManager.E_1710120014(action: "Order List \(data.name ?? "")")
            }
        }
        
        if let cell = tableView.cellForRow(at: indexPath) as? MyInfoSwitchCell {
            if UIApplication.shared.isRegisteredForRemoteNotifications == false {
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.openURL(settingsUrl)
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier! {
        case "ShowLoginWebViewController":

            if let loginVC = segue.destination as? LoginViewController {

                if let myData = DataManager.getMyData() {

                    loginVC.urlString = (myData.links?.signin?.href)!
                    
                    TrackingManager.E_1710120014(action: "Login")
                }
            }
        default:
            break
        }
    }

}
