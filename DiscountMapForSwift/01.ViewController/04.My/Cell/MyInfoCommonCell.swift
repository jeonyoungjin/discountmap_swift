//
//  MyInfoCommonCell.swift
//  DiscountMapForSwift
//
//  Created by Young jin Jeon on 2017. 10. 23..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import UIKit

class MyInfoCommonCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
   
    var metaData: MyData_menu?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
