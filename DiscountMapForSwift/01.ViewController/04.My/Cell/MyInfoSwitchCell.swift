//
//  MyInfoSwitchCell.swift
//  DiscountMapForSwift
//
//  Created by Young jin Jeon on 2017. 10. 25..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import UIKit

class MyInfoSwitchCell: UITableViewCell {

    @IBOutlet weak var switchView: UISwitch!
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onClickSwitch(_ sender: UISwitch) {
        
        ApnsManager.setAPNSStatus(sender.isOn) { isSuccess, message in
         
            if let vc = UIApplication.topViewController() {
            
                vc.showInfoAlert(message: message ?? "")
                
            }
        }
//        NetworkManager.setPushStatus(isOn: sender.isOn)
    }
}
