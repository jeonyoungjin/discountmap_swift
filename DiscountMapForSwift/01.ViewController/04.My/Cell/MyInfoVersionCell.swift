//
//  MyInfoVersionCell.swift
//  DiscountMapForSwift
//
//  Created by Young jin Jeon on 2017. 10. 25..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import UIKit

class MyInfoVersionCell: UITableViewCell {

    @IBOutlet weak var version: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        version.text = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
