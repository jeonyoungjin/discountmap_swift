//
//  WebViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 27..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import NJKWebViewProgress

class WebViewController : UIViewController, UIGestureRecognizerDelegate, NJKWebViewProgressDelegate {
    
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var backButton: UIButton!
    
    var progressView: NJKWebViewProgressView?

    var progressProxy: NJKWebViewProgress?
    
    var webVC : WebContentViewController?
    
    var showType: WebViewShowType? {
        
        didSet(val) {
            self.webVC?.showType = val!
        }
    }
    
    var urlString: String? {
        
        didSet(val) {
            self.webVC?.urlString = val!
        }
    }
    
    var type: Int? {
        
        didSet(val) {
            self.webVC?.type = val!
        }
    }
    
    @IBOutlet weak var navigationTitle: UILabel!
    
    var titleText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        self.progressProxy = NJKWebViewProgress.init()
        self.webVC?.webView?.delegate = self.progressProxy
        self.progressProxy?.webViewProxyDelegate = self.webVC
        self.progressProxy?.progressDelegate = self
        
        let progressBarHeight = 2.0
        let navigationBarBounds = self.navigationBarView.bounds
        let barFrame = CGRect.init(x: 0, y: (navigationBarBounds.size.height - CGFloat(progressBarHeight)), width: navigationBarBounds.size.width, height: CGFloat(progressBarHeight))
        self.progressView = NJKWebViewProgressView.init(frame: barFrame)
        self.progressView?.autoresizingMask = [.flexibleWidth , .flexibleTopMargin]
        self.progressView?.isHidden  = true
        
        self.webVC?.webView.loadRequest(URLRequest.init(url: URL.init(string: self.urlString!)!))
        
        self.navigationTitle.text = titleText
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        self.navigationBarView.addSubview(self.progressView!)
    }
    @IBAction func onClickBackButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let webVC = segue.destination as? WebContentViewController {
            
            self.webVC = webVC
        }
    }
    
    func webViewProgress(_ webViewProgress: NJKWebViewProgress!, updateProgress progress: Float) {
        
        self.progressView?.isHidden = false
        self.progressView?.setProgress(progress, animated: true)
        
        let title = self.webVC?.webView.stringByEvaluatingJavaScript(from: "document.title")
        let filterTitle = title?.replacingOccurrences(of: "특가대표! 위메프 모바일", with: "")
        
        if titleText != nil, titleText != "" {
            self.navigationTitle.text = titleText
        } else {
            if filterTitle == nil || filterTitle?.count == 0 {
                self.navigationTitle.text = title
            } else {
                self.navigationTitle.text = filterTitle
            }
        }
    }
}

