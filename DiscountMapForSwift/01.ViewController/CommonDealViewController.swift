//
//  CommonDealViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 20..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

class CommonDealViewController: UIViewController, UIGestureRecognizerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        // 네비게이션바를 히든시켰을 경우 아래의 코드가 있어야 popViewController 제스처가 동작한다.
//        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
//        {
//            self.navigationController.interactivePopGestureRecognizer.delegate = self;
//            self.navigationController.interactivePopGestureRecognizer.enabled = YES;
//        }
        
//        if self.navigationController?.responds(to: #selector(interactivePopGestureRecognizer)) == true {
        
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
//        }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let id = segue.identifier else {
            
            return
        }
        
        switch id {
            
        case "EmbedViewController":
            
            print("EmbedViewController")
            
            if let vc = segue.destination as? BestViewController {
                
                vc.gnbData = DataManager.getGNBList().first
                vc.gnbData?.link?.href = "\(SERVER_HOST)/deals"
            }
            
        default: break
            
        }
    }
    
    @IBAction func onClickBackButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
