//
//  AllDealResultViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 10. 18..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import UIKit

enum ResultShowType {
    case json
    case html
}

class AllDealResultViewController: UIViewController {
    
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var locationTitle: UIButton!
    @IBOutlet weak var showMapButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    var dealViewController: DealViewController?
    var categoryMetaData: CategoryMetaData?
    var showtype : ResultShowType = .json
    
    deinit {
        print("deinit AllDealResultViewController")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(notification:)),
                                               name: Notification.Name.DealViewControllerMapButtonStatus,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(notification:)),
                                               name: Notification.Name.DealViewControllerMapButtonAlpha,
                                               object: nil)
        
        
        
        if showtype == .json {
            
            if let dealVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DealViewController") as? DealViewController {
                
                dealVC.currentCategoryMetaData = self.categoryMetaData
                
                dealVC.willMove(toParentViewController: self)
                self.containerView.addSubview(dealVC.view)
                self.addChildViewController(dealVC)
                dealVC.view.frame = self.containerView.bounds
                dealVC.didMove(toParentViewController: self)
                self.dealViewController = dealVC
            }
        } else {
            if let webVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebContentViewController") as? WebContentViewController {
                
                webVC.willMove(toParentViewController: self)
                self.containerView.addSubview(webVC.view)
                self.addChildViewController(webVC)
                webVC.view.frame = self.containerView.bounds
                webVC.didMove(toParentViewController: self)
                
                webVC.webView.loadRequest(URLRequest(url: URL(string: (categoryMetaData?.requestUrl)!)!))
            }
        }
        
        if TrackingManager.getMetaData().isSearchResultView == true {
            TrackingManager.V_1710120010()
        } else {
            TrackingManager.V_1710120013()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        TrackingManager.getMetaData().isHotPlace = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationTitle.text = self.categoryMetaData?.selectFilterCategory?.name
        if LocationManager.isExistManualLocation() {
            self.locationTitle.setTitle(" \(LocationManager.autoAddress()!)", for: .normal)
        } else {
            self.locationTitle.setTitle(" 내주변", for: .normal)
        }
        
        if let _ = self.dealViewController {
            
            self.dealViewController?.contentCollectionView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let id = segue.identifier else { return }
        
        switch id {
            
        case "DealViewController":
            if let vc = segue.destination as? DealViewController {
                
                self.dealViewController = vc
                
                // 없음 안대는뎅
                if let _ = self.categoryMetaData {
                    self.dealViewController?.currentCategoryMetaData = self.categoryMetaData
                }
            }
            
        default: break
        }
    }
    
    @IBAction func onClickBackButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickShowMapButton(_ sender: Any) {
        
        //        if let mapVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MapViewController") as? MapViewController {
        
        if let dealVC = self.childViewControllers.last as? HomeChildViewControllerProtocol, dealVC is DealViewController  {
            
            let metaData = MapViewMetaData.init(showType: .showAllDealMode,
                                                currentCategoryMetaData: dealVC.currentCategoryMetaData,
                                                categoryMetaData: dealVC.categoryMetaData, gnbData: dealVC.gnbData)
            
            NotificationCenter.default.post(name: Notification.Name.RequestShowMapView, object: metaData)
        }
    }
    
    
    func handle(notification: Notification) {
        
        switch notification.name {
        case Notification.Name.DealViewControllerMapButtonAlpha:
            if let test = notification.object as? CGFloat {
                self.showMapButton.alpha = test
            }
            return
        default:
            break
        }
        
        if let notiVC = notification.object as? DealViewController, notiVC === self.childViewControllers.last {
            self.showMapButton.isHidden = notiVC.currentCategoryMetaData.useMap?.intValue == 0 ? true : false
        }
        //        if let dealVC = self.childViewControllers.last as? DealViewController, dealVC ===  {
        //            if self.contentPageViewController?.viewControllers![0] === dealVC {
        //                self.showMapButton.isHidden = dealVC.currentCategoryMetaData.useMap.intValue == 0 ? true : false
        //            }
        //        }
    }
    
}
