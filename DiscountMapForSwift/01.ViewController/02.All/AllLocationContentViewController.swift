//
//  AllLocationContentViewController.swift
//  DiscountMapForSwift
//
//  Created by Young jin Jeon on 2017. 10. 12..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

class AllLocationContentViewController: UIViewController {
    
    @IBOutlet weak var contentCollectionView: AllLocationContentCollectionView!
    
    
    @IBAction func onClickClosdButton(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handle(notification:)), name: Notification.Name.AllLocationContentSelectLocation, object: nil)
    }
    
    func handle(notification: Notification) {
        
        self.dismiss(animated: false, completion: nil)
    }
}
