//
//  AllDealContentCollectionViewController.swift
//  DiscountMapForSwift
//
//  Created by Young jin Jeon on 2017. 10. 12..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import UIKit

extension Notification.Name {
    
    static let AllDealContentChangeHight = Notification.Name("AllDealContentChangeHight")
    static let AllDealContentSelectItem = Notification.Name("AllDealContentSelectItem")
}

class AllDealContentCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var title: UILabel!
}

class AllDealContentCollectionViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var dataList :[FilterCategoryData] = DataManager.getFilterCategory()
    
    var contentInset = UIEdgeInsets.zero
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register cell classes
        self.collectionView.register(UINib.init(nibName: "AllDealContentSectionView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "AllDealContentSectionView")

        // Do any additional setup after loading the view.
        self.collectionView.contentInset = contentInset
    }
    
    func setDataList(list: [FilterCategoryData]) {
        
        self.dataList = list
        
        self.collectionView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension AllDealContentCollectionViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return self.dataList.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // 전체보기 버튼 섹션
        if section == 0 {
            return 1
        }
        
        return (self.dataList[section-1].children?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
//        if section == 0 {
//            return CGSize.init(width: UIScreen.main.bounds.size.width, height: 0)
//        }
        
        return CGSize.init(width: UIScreen.main.bounds.size.width, height: 44)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "AllDealContentSectionView", for: indexPath) as! AllDealContentSectionView

        if indexPath.section == 0 {
            
            header.title.text = "전체"
        } else {
        
            header.title.text = self.dataList[indexPath.section-1].name
        }
        
        return header
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllDealContentCollectionViewCell", for: indexPath) as! AllDealContentCollectionViewCell
        
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor.init(red: 237/255, green: 237/255, blue: 237/255, alpha: 1).cgColor
        cell.layer.cornerRadius = 5
        
        if indexPath.section == 0, indexPath.row == 0 {
            cell.title.text = "모든상품"
        } else {
            cell.title.text = self.dataList[indexPath.section-1].children![indexPath.row].name!
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var name = ""
        if indexPath.section == 0, indexPath.row == 0 {
            name = "모든상품"
        } else {
            name = self.dataList[indexPath.section-1].children![indexPath.row].name!
        }

        var size = name.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)])
        size.width += 26
        size.height = 32

        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 16, bottom: 20, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! AllDealContentCollectionViewCell
        
        if TrackingManager.getMetaData().currentService != "" {
                TrackingManager.E_1710120012(newServiceString: cell.title.text ?? "")
                TrackingManager.E_171128005(service: cell.title.text ?? "")
        }
        
        TrackingManager.getMetaData().currentService = cell.title.text ?? ""

        let realIndexPath = IndexPath.init(row: indexPath.row, section: indexPath.section - 1)
        NotificationCenter.default.post(name: NSNotification.Name.AllDealContentSelectItem, object: [self, realIndexPath])
    }
}

