//
//  ALlLocationContentCollectionView.swift
//  DiscountMapForSwift
//
//  Created by Young jin Jeon on 2017. 10. 12..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let AllLocationContentSelectLocation = Notification.Name("AllLocationContentSelectLocation")
}

class AllLocationContentCell: UICollectionViewCell {
    
    @IBOutlet weak var title: UILabel!
}

class AllLocationContentCollectionView: UICollectionView {

    var datalist = [HotPlaceData]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.delegate = self
        self.dataSource = self
        
        self.register(UINib.init(nibName: "MyLocationSectionView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "MyLocationSectionView")
        self.register(UINib.init(nibName: "AllLocationContentSectionView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "AllLocationContentSectionView")
        
        self.setDataList()
    }
    
    func setDataList() {
        self.datalist = DataManager.getHotPlaceList()
    }
    
    func onTapMyLocationSectionView(gesture:UITapGestureRecognizer) {
        
        print("func onTapMyLocationSectionView(gesture:UITapGestureRecognizer)")
        
//        LocationManager.setManualLocation(location: nil, forceAddressName: nil, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name.AllLocationContentSelectLocation, object: "click my location")
    }
}

extension AllLocationContentCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: UIScreen.main.bounds.width/2, height: 39)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.init(width: UIScreen.main.bounds.size.width, height: 52)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0 {
            return 0
        } else {
            return (self.datalist[section - 1].children?.count)!
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.datalist.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if indexPath.section == 0 {
            
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "MyLocationSectionView", for: indexPath)
            
            let gesture = UITapGestureRecognizer.init(target: self, action: #selector(onTapMyLocationSectionView(gesture:)))
            gesture.numberOfTapsRequired = 1
            header.addGestureRecognizer(gesture)
            
            return header
            
        } else {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "AllLocationContentSectionView", for: indexPath) as! AllLocationContentSectionView
            
            header.title.text = self.datalist[indexPath.section - 1].name
            
            return header
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            LocationManager.setManualLocation(location: nil, forceAddressName: nil, completion: nil)
        } else {
            let data = self.datalist[indexPath.section - 1].children?[indexPath.row]
            LocationManager.setManualLocation(location: CLLocation.init(latitude: (data?.latitude?.doubleValue)!, longitude: (data?.longitude?.doubleValue)!), forceAddressName: data?.name, completion: nil)
        }
        
        NotificationCenter.default.post(name: NSNotification.Name.AllLocationContentSelectLocation, object: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllLocationContentCell", for: indexPath) as! AllLocationContentCell
        cell.title.text = self.datalist[indexPath.section - 1].children?[indexPath.row].name
        
        return cell
    }

}
