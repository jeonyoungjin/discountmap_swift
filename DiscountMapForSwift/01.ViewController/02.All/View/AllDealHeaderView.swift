//
//  AllDealHeaderView.swift
//  DiscountMapForSwift
//
//  Created by Young jin Jeon on 2017. 10. 11..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let AllDealHeaderClickNameButton = Notification.Name("AllDealHeaderClickNameButton")
}

class AllDealHeaderView : UIView {

    @IBOutlet weak var fullLabel: UILabel!
    @IBOutlet weak var myLocation: UIButton!
    @IBOutlet weak var locationNameRangeButton: UIButton!
    @IBOutlet weak var locationNameRangeButtonWidthConstraint: NSLayoutConstraint!
    
//    var locationNameRangeButton = UIButton.init(type: .custom)
    var nameUnderLineView = UIView.init()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.nameUnderLineView.backgroundColor = UIColor.init(red: 2/255, green: 2/255, blue: 2/255, alpha: 1)
        self.locationNameRangeButton.addSubview(self.nameUnderLineView)
    }
    
    @IBAction func onClickMyLocationButton(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected == true {
            NotificationCenter.default.post(name: Notification.Name.AllDealHeaderClickNameButton, object: true)
        } else {
            NotificationCenter.default.post(name: Notification.Name.AllDealHeaderClickNameButton, object: false)
        }
    }

    @IBAction func onClickLocationNameRangeButton(_ sender: UIButton) {
        
        NotificationCenter.default.post(name: Notification.Name.AllDealHeaderClickNameButton, object: true)
    }

    
    func setLocationName(name: String) {
        
        let name = "\(name) "
        let nameRange = NSRange.init(location: 0, length: name.count + 1)
        
        let attributeString = NSMutableAttributedString.init()
        attributeString.append(NSAttributedString.init(string: name,
                                                       attributes: [NSForegroundColorAttributeName: UIColor.init(red: 2/255, green: 2/255, blue: 2/255, alpha: 1),
                                                                    NSFontAttributeName : UIFont(name: "AppleSDGothicNeo-Regular", size: 24)!]))
        
        let attachment = NSTextAttachment.init()
        attachment.image = UIImage.init(named: "btnListArrowDown-1")
        attachment.bounds = CGRect.init(x: 0, y: 5, width: (attachment.image?.size.width)!, height: (attachment.image?.size.height)!)
        
        attributeString.append(NSAttributedString.init(attachment: attachment))
        
        
        attributeString.append(NSAttributedString.init(string: " 에서 무엇을 찾고 있나요?",
                                                       attributes: [NSForegroundColorAttributeName: UIColor.init(red: 2/255, green: 2/255, blue: 2/255, alpha: 1),
                                                                    NSFontAttributeName : UIFont(name: "AppleSDGothicNeo-Regular", size: 24)!]))

        let paragraphStyle = NSMutableParagraphStyle.init()
        paragraphStyle.lineSpacing = 7
        attributeString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSRange.init(location: 0, length: attributeString.length))
        
        self.fullLabel.attributedText = attributeString

        let width = boundingRectForCharcterRange(range: nameRange, fullString: (self.fullLabel.attributedText)!).size.width
        self.locationNameRangeButtonWidthConstraint.constant = width
        self.locationNameRangeButton.frame.size.width = width

        var underLineFrame = self.locationNameRangeButton.bounds
        underLineFrame.origin.y = underLineFrame.size.height
        underLineFrame.size.height = 1
        self.nameUnderLineView.frame = underLineFrame
        
//        self.myLocation.setTitle(name, for: .selected)
//        self.myLocation.isSelected = false
//
//        self.layoutIfNeeded()
    }

    
    func boundingRectForCharcterRange(range: NSRange, fullString: NSAttributedString) -> CGRect {
        
        let textStorage = NSTextStorage.init(attributedString: fullString)
        let layoutManager = NSLayoutManager.init()
        textStorage.addLayoutManager(layoutManager)
        
        let textContainer = NSTextContainer.init(size: fullLabel.bounds.size)
        textContainer.lineFragmentPadding = 0
        layoutManager.addTextContainer(textContainer)
        
        var glyphRange = NSRange()
        
        layoutManager.characterRange(forGlyphRange: range, actualGlyphRange: &glyphRange)
        
        return layoutManager.boundingRect(forGlyphRange: glyphRange, in: textContainer)
    }
}
