//
//  AllDealContentViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 21..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

class AllDealContentViewController: UIViewController {
    
    @IBOutlet weak var dealHeaderView: AllDealHeaderView!
    
    var contentCollectionViewController: AllDealContentCollectionViewController?
    var isManualLocation: Bool? = LocationManager.isExistManualLocation()
    
    deinit {
        print("deinit AllDealContentViewController!!")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(notification:)),
                                               name: Notification.Name.AllDealContentSelectItem,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(notification:)),
                                               name: Notification.Name.AllDealHeaderClickNameButton,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(notification:)),
                                               name: Notification.Name.AllLocationContentSelectLocation,
                                               object: nil)
        
        TrackingManager.V_1710120012()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.isManualLocation = LocationManager.isExistManualLocation()
        
        self.headerAction()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let id = segue.identifier else {
            
            return
        }
        
        switch id {
            
        case "AllDealContentCollectionViewController":
            
            if let vc = segue.destination as? AllDealContentCollectionViewController {
                
//                vc.setDataList(list: DataManager.getFilterCategory())
                vc.contentInset = UIEdgeInsets.init(top: 171, left: 0, bottom: 30, right: 0)
                self.contentCollectionViewController = vc
            }
            
        default: break
            
        }
    }
    
    
    func headerAction() {
        
        if self.isManualLocation! {
            
            self.dealHeaderView.setLocationName(name: LocationManager.shared.manualAddress!)
            self.dealHeaderView.myLocation.setTitle("내주변", for: .normal)
            self.dealHeaderView.myLocation.contentHorizontalAlignment = .left
            self.dealHeaderView.myLocation.setImage(UIImage.init(named: "iconCategoryArea"), for: .normal)
            self.dealHeaderView.myLocation.isSelected = true
            
        } else {
            
            self.dealHeaderView.setLocationName(name: "내주변")
            self.dealHeaderView.myLocation.setTitle("지역변경", for: .normal)
            self.dealHeaderView.myLocation.contentHorizontalAlignment = .right
            self.dealHeaderView.myLocation.setImage(nil, for: .normal)
            self.dealHeaderView.myLocation.isSelected = false
        }
    }

    
    func handle(notification: NSNotification) {
        
        switch notification.name {
            
        case Notification.Name.AllDealContentSelectItem:
            
            if let who = (notification.object as! [Any])[0] as? AllDealContentCollectionViewController, who === self.contentCollectionViewController {
                if  let index = (notification.object as! [Any])[1] as? IndexPath {
                    
                    let categoryMetaData = CategoryMetaData()
                    categoryMetaData.canRequestData = true
                    categoryMetaData.requestUrl = "\(SERVER_HOST)/deals"
                    
                    if index.section == -1 {
                        categoryMetaData.selectFilterCategory = FilterCategoryData_children(dict: ["name" : "모든상품"])
                        categoryMetaData.requestParameter = ""
                    } else {
                        categoryMetaData.selectFilterCategory = DataManager.getFilterCategory()[index.section].children?[index.row]
                        categoryMetaData.requestParameter = (DataManager.getFilterCategory()[index.section].children?[index.row].id)!
                    }
                    
                    let dealVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AllDealResultViewController") as! AllDealResultViewController
                    dealVC.categoryMetaData = categoryMetaData
                    dealVC.showtype = .json
                    
                    if isManualLocation == false {
                        LocationManager.setManualLocation(location: nil, forceAddressName: nil, completion: nil)
                    }
                    
                    self.navigationController?.pushViewController(dealVC, animated: true)
                    
                    
                }
            }
            
        case Notification.Name.AllDealHeaderClickNameButton:
            if let parentVC = self.parent?.navigationController {
                
                if let test = notification.object as? Bool {

                    if test == true {
                        if let locationVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AllLocationContentViewController") as? AllLocationContentViewController {
                        self.isManualLocation = true
                            parentVC.present(locationVC, animated: false, completion: {
                                
                            })
                        }
                    } else {
                    
                        self.isManualLocation = false
                        self.headerAction()
                        
                        LocationManager.initLocationWithResponse(response: nil)
                    }
                }
            }
            
        case Notification.Name.AllLocationContentSelectLocation:
            if let _ = notification.object as? String {
                self.isManualLocation = false
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.15, execute: {
                    
                    LocationManager.initLocationWithResponse(response: nil)
                })
            }
            
        default:
            break
        }
    }
}

extension AllDealContentViewController : UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
    }
}
