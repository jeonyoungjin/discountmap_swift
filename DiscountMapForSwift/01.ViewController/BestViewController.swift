//
//  BestViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 8..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

class BestViewController : UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
}

extension BestViewController : UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return 30
    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {


        return collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
    }
}

extension BestViewController : UIScrollViewDelegate {

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {

        let translation = scrollView.panGestureRecognizer.translation(in: scrollView.superview)

        print ("scrollViewWillEndDragging \(translation.y)")

        if translation.y > 0 {
            NotificationCenter.default.post(name: Notification.Name.ChangeHeaderConstraint, object: 0)
        } else {
            NotificationCenter.default.post(name: Notification.Name.ChangeHeaderConstraint, object: -50)
        }
    }
}

