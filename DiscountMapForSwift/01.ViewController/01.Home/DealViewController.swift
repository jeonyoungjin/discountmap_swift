//
//  DealViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 12..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

enum DealCollectionSection : Int {
    
    case bannerSection = 0
    case categorySection = 1
    case contentSection = 2
}

enum DealUIMode {
    
    case horizontalCellMode
    case verticalCellMode
    case webCellMode
}

extension Notification.Name {
    static let DealViewControllerMapButtonStatus = Notification.Name("DealViewControllerMapButtonStatus")
    static let DealViewControllerMapButtonAlpha = Notification.Name("DealViewControllerMapButtonAlpha")
    static let DealViewControllerSelectItem = Notification.Name("DealViewControllerSelectItem")
    static let DealViewControllerSelectBanner = Notification.Name("DealViewControllerSelectBanner")
}

class DealViewController : UIViewController, HomeChildViewControllerProtocol {
    
    @IBOutlet weak var contentCollectionView: UICollectionView!
    
    var isSendA_171128002: Bool = false
    
//    var bannerCell: UICollectionViewCell?
    var bannerCellHeight: CGFloat = 0 {
        didSet(val) {
//            UIView.performWithoutAnimation {
//                self.contentCollectionView.reloadSections(IndexSet.init(integer: 0))
//            }
        }
    }
    
    var webCell: UICollectionViewCell?
    var webCellHeight: CGFloat = 1024 {
        didSet(val) {
            
            guard webCellHeight != 1024 else {
                return
            }
            
            UIView.performWithoutAnimation {
                self.contentCollectionView.reloadSections(IndexSet.init(integer: 0))
            }
        }
    }
    
    var subCategoryCell: DealSubCategoryCollectionViewCell?
    var subCategoryCellHeight: CGFloat = 0 {
        didSet(val) {
            UIView.performWithoutAnimation {

//                    self.contentCollectionView.reloadSections(IndexSet.init(integer: 1))

            }
        }
    }
    
    var topRefreshController: UIRefreshControl?
    
    var response: NetworkResponse?
    
    // 카테고리
    var categoryMetaData: [CategoryMetaData]?
    
    // 현재 카테고리
    var currentCategoryMetaData : CategoryMetaData!
    
    // 리스트 없음 표시 셀에 붙일 뷰
    var noticeEmptyDealListView: UICollectionReusableView?
    
    // GNB 데이터
    var gnbData: GNBData?    {
        willSet(newVal) {

            self.categoryMetaData = [CategoryMetaData]()

            if let children = newVal?.children {

                for (index, child) in children.enumerated() {

                    let metaData = CategoryMetaData()
                    metaData.requestUrl = child.link?.href
                    metaData.categoryIndex = index
                    self.categoryMetaData?.append(metaData)
                }

            } else {

                let metaData = CategoryMetaData()
                metaData.requestUrl = newVal?.link?.href
                metaData.categoryIndex = 0
                self.categoryMetaData?.append(metaData)
            }

            self.currentCategoryMetaData = self.categoryMetaData?.first
        }
    }

    var uiMode: DealUIMode = .horizontalCellMode
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.contentCollectionView.alwaysBounceVertical = true
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(notification:)),
                                               name: Notification.Name.WebCellLoadComplete,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(notification:)),
                                               name: Notification.Name.DealSubCategoryChangeHeight,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(notification:)),
                                               name: Notification.Name.DealSubCategorySelectItem,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(notification:)),
                                               name: Notification.Name.ChangeLocationFromMapView,
                                               object: nil)
        
        // [전영진] 검색에 특화됨 공통으로 빠져야 함
        self.contentCollectionView.register(EmptySearchResultView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "EmptySearchResultView")

        self.response = { [weak self] (result, error) in

            // 인디케이터 제거
            LoadingViewManager.close()

            // 넘넘 많탐
            self?.currentCategoryMetaData.dealArray.append(contentsOf: result[0] as! [CategoryDealData])
            self?.currentCategoryMetaData.uniqueSet = NSOrderedSet.init(array: (self?.currentCategoryMetaData.dealArray.filter{$0.dealID?.intValue != 0}.map{$0.dealID!})!)
            self?.currentCategoryMetaData.metaData = result[1] as? DealMetaData
            self?.currentCategoryMetaData.titlePropertyName = result[3] as? String
            self?.currentCategoryMetaData.canRequestData = true
            self?.currentCategoryMetaData.bannerArray = result[4] as? [BannerData]
//            self?.currentCategoryMetaData.sectionTitle = result[5] as? String
            self?.currentCategoryMetaData.useMap = result[6] as? NSNumber
            self?.currentCategoryMetaData.useRanking = result[7] as? NSNumber
            self?.currentCategoryMetaData.listColumn  = result[8] as? NSNumber

            // GPS 좌표가 없는경우 서버에서 받아온 값으로 설정한다.
            if let serverLocation = result[2] as? CLLocationCoordinate2D, LocationManager.shared.currentLocation == nil  {
                
                LocationManager.shared.currentLocation = CLLocation.init(latitude: serverLocation.latitude , longitude: serverLocation.longitude)
                LocationManager.getAddressString(location: LocationManager.shared.currentLocation!, completion: { (address) in
                    LocationManager.shared.currentAddress = address
                })
            }

            if let header = result[9] as? HeaderData {
                self?.currentCategoryMetaData.headerData = header

                if  header.dataType == "image" {
                    if let height = header.height?.floatValue, let width = header.width?.floatValue {
                        let ratio = UIScreen.main.bounds.size.width / CGFloat(width)
                        let size = CGSize.init(width: UIScreen.main.bounds.size.width, height: CGFloat(height) * ratio + 1)
                        self?.bannerCellHeight = size.height
                    }
                }
            }

            
            if self?.currentCategoryMetaData.listColumn == 2 {
                self?.uiMode = .verticalCellMode
            }

            self?.contentCollectionView.reloadData()

            // 데이터를 가져왔을때 한번
            NotificationCenter.default.post(name: Notification.Name.DealViewControllerMapButtonStatus, object: self)

            // 데이터를 가져왔을때 한번
            // 딜이 없으면 알파를 0으로 만들어본다.
            if self?.currentCategoryMetaData.dealArray.count == 0 {
                NotificationCenter.default.post(name: Notification.Name.DealViewControllerMapButtonAlpha, object: CGFloat(0))
            } else {
                NotificationCenter.default.post(name: Notification.Name.DealViewControllerMapButtonAlpha, object: CGFloat(1))
            }
            

            if self?.currentCategoryMetaData.dealArray.count == 0 {
                TrackingManager.E_170404001(view: "Main")
            }
        }        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        TrackingManager.getMetaData().isHotPlace = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.topRefreshController == nil {
        self.topRefreshController = UIRefreshControl.init()
        self.topRefreshController?.tintColor = UIColor.init(red: 255/255, green: 73/255, blue: 73/255, alpha: 1)
        
        //        self.topRefreshController?.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        //        if #available(iOS 10.0, *) {
        //            self.contentCollectionView.refreshControl = self.topRefreshController
        //        } else {
        
        self.contentCollectionView.addSubview(self.topRefreshController!)
        //        }
            
        }
        

        
        // 뷰가 보일때 두번~
        NotificationCenter.default.post(name: Notification.Name.DealViewControllerMapButtonStatus, object: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.main.async {
            self.changeSubCategory(index: self.currentCategoryMetaData.categoryIndex)
        }
    }
    
    func refresh() {
        if (self.topRefreshController?.isRefreshing)! {
            self.topRefreshController?.endRefreshing()
            
            prepareDealReset()
            
            if LocationManager.isExistManualLocation() == false {
      
                LoadingViewManager.show()
                LocationManager.initLocationWithResponse(response: { (finished) in
                 
                    DispatchQueue.main.async{
                        self.changeSubCategory(index: self.currentCategoryMetaData.categoryIndex)
                    }
                })
            } else {
             
                DispatchQueue.main.async{
                    self.changeSubCategory(index: self.currentCategoryMetaData.categoryIndex)
                }
            }
        }
    }
    
    func prepareDealReset() {

        self.currentCategoryMetaData.headerData = nil
        self.currentCategoryMetaData.dealArray.removeAll()
        self.currentCategoryMetaData.dealArray = [CategoryDealData]()
        self.currentCategoryMetaData.uniqueSet = nil
        self.currentCategoryMetaData.uniqueSet = NSOrderedSet.init()
        
        if let categoryMetaDataList = self.categoryMetaData {
            
            for data in categoryMetaDataList {
                data.headerData = nil
                data.dealArray.removeAll()
                data.dealArray = [CategoryDealData]()
                data.uniqueSet = nil
                data.uniqueSet = NSOrderedSet.init()
            }
        }
    }
    
    func handle(notification:Notification) {
        
        // 전체 적용
        if notification.name == Notification.Name.ChangeLocationFromMapView {
            
            prepareDealReset()
            
            self.contentCollectionView.contentOffset = CGPoint.zero
            self.contentCollectionView.reloadData()
            
            return
        }
        
        // 현재 뷰만 적용 해야함
        
        if let categoryCell = self.contentCollectionView.cellForItem(at: IndexPath.init(item: 0, section: 1)) {
            
            let notiCell = (notification.object as! [Any])[0] as! UICollectionViewCell
            if categoryCell === notiCell {
                
                switch notification.name {
                    
                case Notification.Name.DealSubCategoryChangeHeight:
                    self.subCategoryCellHeight = (notification.object as! [Any])[1] as! CGFloat
                    
                case Notification.Name.DealSubCategorySelectItem:
                    let index = (notification.object as! [Any])[1] as! Int
                    
                    self.changeSubCategory(index: index)
                    TrackingManager.E_1710120006()

                default:
                    break
                }
            }
        }
        
        if let webCell = self.contentCollectionView.cellForItem(at: IndexPath.init(item: 0, section: 0)) {
            
            let notiCell = (notification.object as! [Any])[0] as! UICollectionViewCell
            if notiCell === webCell {
                
                self.webCellHeight = (notification.object as! [Any])[1] as! CGFloat
            }
        }
    }
    
    func changeSubCategory(index: Int) {
        
        if self.currentCategoryMetaData.dealArray.count == 0 {
            NotificationCenter.default.post(name: Notification.Name.DealViewControllerMapButtonAlpha, object: CGFloat(0))
        } else {
            NotificationCenter.default.post(name: Notification.Name.DealViewControllerMapButtonAlpha, object: CGFloat(1))
        }
        
        if let _ = self.categoryMetaData {
            self.currentCategoryMetaData = self.categoryMetaData![index]
        }

        if self.currentCategoryMetaData.dealArray.count == 0 {
            
            //앱보이 트래킹 플래그
            self.isSendA_171128002 = false
            
            LoadingViewManager.show()
            let location = LocationManager.autoLocation()?.coordinate
            NetworkManager.getDealList(param: ["requestURL":(self.currentCategoryMetaData.requestUrl)!,
                                               "lon": location?.longitude ??  CLLocationDegrees(0),
                                               "lat": location?.latitude ?? CLLocationDegrees(0),
                                               "c" : self.currentCategoryMetaData.requestParameter,
                                               "page": 1],
                                       uniqueList: currentCategoryMetaData.uniqueSet,
                                       addCategory: true,
                                       completion: self.response)
        } else {
            
            self.contentCollectionView.reloadData()
        }
     
        TrackingManager.getMetaData().subCategoryName = self.gnbData?.children?[index].name ?? ""
    }
}

extension DealViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "DealSectionReusableView", for: indexPath) as! DealSectionReusableView

        if indexPath.section == DealCollectionSection.contentSection.rawValue {
        
            if let title = self.currentCategoryMetaData.attributeSectionTitle {
                headerView.title.attributedText = title
            } else {
                headerView.title.text = self.currentCategoryMetaData.sectionTitle
            }
        }
        
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == DealCollectionSection.contentSection.rawValue {
            
            let cellHeight = self.bannerCellHeight + (self.webCellHeight - 1024) + self.subCategoryCellHeight
            
            if  (self.currentCategoryMetaData.sectionTitle != nil && self.currentCategoryMetaData.sectionTitle != "" ) ||
                self.currentCategoryMetaData.attributeSectionTitle != nil {
                return CGSize.init(width: UIScreen.main.bounds.size.width, height: 46 + (cellHeight == 0 ? 0 : 16))
            }
            
            if cellHeight == 0 {
                return CGSize.zero
            } else {
                return CGSize.init(width: UIScreen.main.bounds.size.width, height: self.subCategoryCellHeight == 0 ? self.bannerCellHeight == 0 ? 0 : 22 : 16)
            }
        }
        
        return CGSize.zero
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        switch self.uiMode {
        case .horizontalCellMode:
            return 5
        case .verticalCellMode:
            return 7
            
        default:
            return 0
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        switch DealCollectionSection(rawValue: section)! {

        case .bannerSection:
            // 둘중 하나만 있어야 한다.
                return UIEdgeInsets.zero
            
        case .categorySection:
            
            if self.subCategoryCellHeight == 0 {
                return UIEdgeInsets.zero
            } else {
                
                return UIEdgeInsets(top: self.bannerCellHeight == 0 ? 0 : 22,
                                    left: 0,
                                    bottom: 16,
                                    right: 0)
            }

        case .contentSection:
            return UIEdgeInsets(top: 0,//cellHeight == 0 ? 0 : 16,
                                left: self.uiMode == .verticalCellMode ? 16 : 0,
                                bottom: 0,
                                right: self.uiMode == .verticalCellMode ? 16 : 0)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if indexPath.section == DealCollectionSection.bannerSection.rawValue {
            
            if let headerData = self.currentCategoryMetaData.headerData, self.currentCategoryMetaData.headerData?.dataType == "image" {
                return CGSize.init(width: UIScreen.main.bounds.size.width, height: self.bannerCellHeight)
                
            }

            return CGSize.init(width: UIScreen.main.bounds.size.width, height: self.webCellHeight)

        } else if indexPath.section == DealCollectionSection.categorySection.rawValue {

            return CGSize.init(width: UIScreen.main.bounds.size.width, height: self.subCategoryCellHeight == 0 ? 0.1 : self.subCategoryCellHeight)

        } else {
            
            guard currentCategoryMetaData.isEmptyResult == false else {
                
                return (self.noticeEmptyDealListView?.frame.size)!
            }

            switch self.uiMode {
            case .horizontalCellMode:
                let cellWidth = UIScreen.main.bounds.size.width
                return CGSize.init(width: cellWidth, height: cellWidth * 0.546 + 71)

            case .verticalCellMode:
                let cellWidth = (UIScreen.main.bounds.size.width - (16*2+7))/2
                let ratio = cellWidth/168
                return CGSize.init(width: cellWidth, height: 295*ratio)

            case .webCellMode:
                break
            }
        }

        return CGSize.zero
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        
        if section == DealCollectionSection.bannerSection.rawValue {
            if self.currentCategoryMetaData.headerData != nil {
                return 1
            }
            return 0
            
        } else if section == DealCollectionSection.categorySection.rawValue {
            
            if let _ = self.gnbData?.children {
                return 1
            }
            
            return 0
            
        } else {
            
            guard currentCategoryMetaData.isEmptyResult == false else {
                return 1
            }
            
            if self.currentCategoryMetaData.useRanking?.boolValue == true, self.currentCategoryMetaData.uniqueSet.count > 100 {
                return 100
            }
            
            return self.currentCategoryMetaData.uniqueSet.count
        }
    }
    
    func getCategoryDealListFromOrderUniqueListIndex(index: Int) -> [CategoryDealData] {
        return self.currentCategoryMetaData.dealArray.filter{$0.dealID == (self.currentCategoryMetaData.uniqueSet.object(at: index) as! NSNumber)}
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == DealCollectionSection.bannerSection.rawValue {
            
            if let headerData = self.currentCategoryMetaData.headerData {
                
                if headerData.dataType == "html" {
                    
                    if self.webCell == nil {
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DealWebCollectionViewCell", for: indexPath) as! DealWebCollectionViewCell
                        cell.requestUrl = headerData.url
                        cell.webView.loadRequest(URLRequest.init(url: URL.init(string: headerData.url!)!))
                        self.webCell = cell
                    }
                    
                    return self.webCell!
                    
                } else {
                   let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DealBannerCollectionViewCell", for: indexPath) as! DealBannerCollectionViewCell
                    cell.bannerImage.sd_setImage(with: URL.init(string: headerData.url!), completed: { (image, error, cacheType, url) in
                    })
                    
                    return cell
                }
            }
            
        } else if indexPath.section == DealCollectionSection.categorySection.rawValue {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DealSubCategoryCollectionViewCell", for: indexPath) as! DealSubCategoryCollectionViewCell
            cell.subCategoryList = self.gnbData?.children
            self.subCategoryCell = cell
            cell.setVisibleCell(index: currentCategoryMetaData.categoryIndex)
            
            return cell
            
        } else if indexPath.section == DealCollectionSection.contentSection.rawValue {

            var cell: UICollectionViewCell?
            
            guard currentCategoryMetaData.isEmptyResult == false else {
                
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyResultCell", for: indexPath)
                cell?.addSubview(self.noticeEmptyDealListView!)
                
                return cell!
            }
            
            if self.uiMode == .horizontalCellMode {

                let horizonCell = collectionView.dequeueReusableCell(withReuseIdentifier: "BestDealCollectionViewCell", for: indexPath) as! BestDealCollectionViewCell
                horizonCell.compose(dealList: self.getCategoryDealListFromOrderUniqueListIndex(index: indexPath.row), titleProperty: self.currentCategoryMetaData.titlePropertyName!)
                horizonCell.rank.isHidden = !(self.currentCategoryMetaData.useRanking?.boolValue)!
                horizonCell.rank.setTitle("\(indexPath.row + 1)", for: .normal)
                horizonCell.setBest10Color(isBest10: indexPath.row < 10 ? true : false)
                
//                horizonCell.rank.titleLabel?.text = ""
                cell = horizonCell
                
            } else if self.uiMode == .verticalCellMode {
                let verticalCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DealContentCollectionViewCell", for: indexPath) as! DealContentCollectionViewCell
                verticalCell.compose(dealList: self.getCategoryDealListFromOrderUniqueListIndex(index: indexPath.row), titleProperty: self.currentCategoryMetaData.titlePropertyName!)
                cell = verticalCell
            }
            
            if indexPath.row == self.currentCategoryMetaData.uniqueSet.count - Int(((self.currentCategoryMetaData.metaData?.limit?.floatValue)! * 0.2)) &&
                self.currentCategoryMetaData.canRequestData == true &&
                self.currentCategoryMetaData.dealArray.count == ((self.currentCategoryMetaData.metaData?.limit?.intValue)! * (self.currentCategoryMetaData.metaData?.page?.intValue)!) {
                
                self.currentCategoryMetaData.canRequestData = false
                let location = LocationManager.autoLocation()?.coordinate
                
                LoadingViewManager.show()
                NetworkManager.getDealList(param: ["requestURL":(currentCategoryMetaData.requestUrl)!,
                                                   "lon": location?.longitude ?? 0,
                                                   "lat": location?.latitude ?? 0,
                                                   "c": currentCategoryMetaData.requestParameter,
                                                   "page": (self.currentCategoryMetaData.metaData?.page?.intValue)! + 1],
                                           uniqueList: currentCategoryMetaData.uniqueSet,
                                           addCategory: true,
                                           completion: self.response)
            }
            
            return cell!
        }
        
        return UICollectionViewCell.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard indexPath.section == DealCollectionSection.contentSection.rawValue else {
            
            if indexPath.section == DealCollectionSection.bannerSection.rawValue {
                
                let header = self.currentCategoryMetaData.headerData
                if header?.linkUrl != nil, header?.linkUrl != "" {
                 
                    NotificationCenter.default.post(name: Notification.Name.DealViewControllerSelectBanner, object: [header, gnbData?.name])
                    
                    TrackingManager.getMetaData().bannerURL = header?.linkUrl ?? ""
                    TrackingManager.V_1710120003()
                    TrackingManager.E_171128006()
                }
            }
            return
        }
        

        NotificationCenter.default.post(name: Notification.Name.DealViewControllerSelectItem, object: [self.getCategoryDealListFromOrderUniqueListIndex(index: indexPath.row).first as Any, self.isSendA_171128002])
        self.isSendA_171128002 = true
    }
}

extension DealViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        self.refresh()
    }
    

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        print(scrollView.contentOffset.y)
        if scrollView.contentOffset.y  < -120 {
            self.topRefreshController?.beginRefreshing()
        }

        let translation = scrollView.panGestureRecognizer.translation(in: scrollView.superview)

        print ("scrollViewWillEndDragging \(translation.y)")

        if translation.y > 0 {
            NotificationCenter.default.post(name: Notification.Name.ChangeHeaderConstraint, object: 0)
        } else {
            NotificationCenter.default.post(name: Notification.Name.ChangeHeaderConstraint, object: -42)
        }
    }
}

