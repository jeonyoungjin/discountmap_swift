//
//  HomeViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 7..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

class CategoryMetaData {
    
    deinit {
        print("deinit categoryMetaData!!!")
        
        self.dealArray.removeAll()
        self.bannerArray?.removeAll()
        self.uniqueSet = nil
    }
    
    init() {
        self.uniqueSet = NSOrderedSet.init()
    }
    
    // 카테고리 데이터
    //    var gnbChildData: GNBData_children?
    var categoryIndex: Int = 0
    
    // 요청한 데이터가 없는경우 
    var isEmptyResult: Bool = false
    
    // 헤드 배너
    var headerData: HeaderData?
    
    var selectFilterCategory: FilterCategoryData_children?
    
    // 요청 URL
    var requestUrl: String?
    
    // 
    var requestParameter: String = ""
    
    // 메타데이터
    var metaData: DealMetaData?
    
    // 중복제거 set
    var uniqueSet: NSOrderedSet!// = NSOrderedSet.init()
    
    // 모든 딜 array
    var dealArray = [CategoryDealData]()
    
    // 배너 array
    var bannerArray: [BannerData]?
    
    //
    var titlePropertyName: String?
    
    // 현재 페이지
    var currentPage: Int?
    
    // 무한 스크롤 플래그
    var canRequestData: Bool = false
    
    // 섹션 타이틀
    var sectionTitle: String?
    
    // 섹션 어트리뷰트 스트링
    var attributeSectionTitle: NSAttributedString?
    
    // 맵 사용여부
    var useMap: NSNumber? = NSNumber.init(value: 0)
    
    // 랭킹 사용여부
    var useRanking: NSNumber?
    
    // 컬렉션 뷰 타입
    var listColumn: NSNumber?
    
    // 스크롤 현재 offset
    var currentScrollOffSet: CGPoint?
}

protocol HomeChildViewControllerProtocol {
    var categoryMetaData: [CategoryMetaData]? { get set }
    var currentCategoryMetaData: CategoryMetaData! { get set }
    var gnbData: GNBData?  { get set }
}

extension Notification.Name {
    
    static let ChangeHeaderConstraint = Notification.Name("ChangeHeaderConstraint")
    static let ClickHomeSearchBar = Notification.Name("ClickHomeSearchBar")
}

class HomeViewController : UIViewController {
    
    @IBOutlet weak var contentCollectionView: UICollectionView!
    @IBOutlet weak var headerTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var statusBgViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var showMapButton: UIButton!
    @IBOutlet weak var fakeSearchTextField: UIButton!
    @IBOutlet weak var pagerContainerView: UIView!
    var contentPageViewController : UIPageViewController?
    var pager: PagerSlidingTabView?
    var vcArray = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.statusBgViewHeightConstraint.constant = UIApplication.shared.statusBarFrame.size.height
        
        // 페이지뷰컨트롤러 바운스 제거
        for view in self.view.subviews {
            if (view is UIScrollView) {
                let scrollView = view as! UIScrollView
                scrollView.bounces = false
                break
            }
        }
        
        let attributes = [
            NSForegroundColorAttributeName: UIColor.init(red: 149/255, green: 149/255, blue: 149/255, alpha: 1),
            NSFontAttributeName : UIFont(name: "AppleSDGothicNeo-Regular", size: 12)! // Note the !
        ]
        self.fakeSearchTextField.setAttributedTitle(NSAttributedString(string: "무엇을 찾으세요?", attributes:attributes), for: .normal)
        
        self.pager = Bundle.main.loadNibNamed("PagerSlidingTabView", owner: self, options: nil)?.first as? PagerSlidingTabView
        self.pagerContainerView.addSubview(pager!)
        self.pager?.delegate = self       
        pager?.setCategoryList(list: DataManager.getGNBList().map{$0.name!})
        
        for  (index, data) in DataManager.getGNBList().enumerated() {
            
            if data.link?.dataType == "json" {
                if let dealVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DealViewController") as? DealViewController {
                    
                    dealVC.view.tag = index
                    dealVC.gnbData = DataManager.getGNBList()[index]
                    
                    self.vcArray.append(dealVC)
                }
            } else {
                if let webVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebContentViewController") as? WebContentViewController {
                    
                    webVC.view.tag = index
                    webVC.gnbData = DataManager.getGNBList()[index]
                    
                    self.vcArray.append(webVC)
                }
            }
        }

        // 페이지 0번으로 초기화
        self.setCurrentPage(index: 0, animated: false)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(notification:)),
                                               name: Notification.Name.ChangeHeaderConstraint,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(notification:)),
                                               name: Notification.Name.WebCellClickLink,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(notification:)),
                                               name: Notification.Name.DealViewControllerMapButtonStatus,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(notification:)),
                                               name: Notification.Name.DealViewControllerMapButtonAlpha,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(notification:)),
                                               name: Notification.Name.DealViewControllerSelectBanner,
                                               object: nil)
        
    }
    
    
    @IBAction func onClickShowMapButton(_ sender: Any) {
        
        
        if let mapVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MapViewController") as? MapViewController {
            
            if let currentVC =  self.contentPageViewController?.viewControllers?.first as? HomeChildViewControllerProtocol {
                
                let metaData = MapViewMetaData.init(showType: .showDefaultMode,
                                                    currentCategoryMetaData: currentVC.currentCategoryMetaData,
                                                    categoryMetaData: currentVC.categoryMetaData,
                                                    gnbData: currentVC.gnbData)
                
                NotificationCenter.default.post(name: Notification.Name.RequestShowMapView, object: metaData)
                
                //메인 GNB 딜리스트 노출 시 , GNB명과 함께 저장 gnbData가 없으면 메인 딜리스트가 아니다
                if let _ = currentVC.gnbData {
                    TrackingManager.V_1710120001(current: "Map List")
                }
            }
        }
    }
    
    func setCurrentPage(index: Int, animated: Bool = true) {
        
        var prevVC = self.vcArray[index]
        
        if (self.contentPageViewController?.viewControllers?.count)! > 0 {
            prevVC = (self.contentPageViewController?.viewControllers?[0])!
        }
        
        self.contentPageViewController?.setViewControllers([self.vcArray[index]],
                                                           direction: prevVC.view.tag > index ? .reverse : .forward ,
                                                           animated: animated,
                                                           completion: { (finished) in
                                                            
                                                            if let _ = self.vcArray[index] as? DealViewController {
                                                                
                                                            } else {
                                                                self.showMapButton.isHidden = true
                                                            }
        })
        
        self.pager?.setPositionIndex(index: index, animated: true)
        
        TrackingManager.getMetaData().gnbName = DataManager.getGNBList()[index].name!
        TrackingManager.V_1710120001(current: "Deal List")
        TrackingManager.E_171128003()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let id = segue.identifier else {
            
            return
        }
        
        switch id {
            
        case "EmbedPageViewController":
            
            print("EmbedPageViewController")
            
            if let vc = segue.destination as? UIPageViewController {
                
                vc.delegate = self
                vc.dataSource = self
                self.contentPageViewController = vc
            }
            
        default: break
            
        }
    }
    
    func handle( notification: Notification) {
        
        switch notification.name {
        case Notification.Name.WebCellClickLink:
            
            if let test = notification.object as? [String: Any] {
                
                let webVC  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                
                webVC.urlString = test["value"] as? String
                webVC.type = test["type"] as? Int
                webVC.titleText = "이벤트"
                
                if webVC.type ==  5 {
                    webVC.urlString = "http://m.wemakeprice.com/m/deal/adeal/\(webVC.urlString!)"
                    webVC.titleText = nil
                }
                self.navigationController?.pushViewController(webVC, animated: true)
                
                TrackingManager.getMetaData().bannerURL = webVC.urlString!
                TrackingManager.V_1710120003()
                TrackingManager.E_171128006()
            }
            
        case Notification.Name.ChangeHeaderConstraint:
            UIView.animate(withDuration: 0.2,
                           delay: 0,
                           options: [.allowUserInteraction],
                           animations: {
                            
                            if let test = notification.object {
                                
                                self.headerTopConstraint.constant = CGFloat(test as! Int)
                                self.view.layoutIfNeeded()
                            }
                            
            }, completion: nil)
            
        case Notification.Name.DealViewControllerMapButtonStatus:
            if let vc = notification.object as? DealViewController {
                if self.contentPageViewController?.viewControllers![0] === vc {
                    self.showMapButton.isHidden = vc.currentCategoryMetaData.useMap?.intValue == 0 ? true : false
                }
            }
            
        case Notification.Name.DealViewControllerMapButtonAlpha:
            if let test = notification.object as? CGFloat {
                self.showMapButton.alpha = test
            }
            
        case Notification.Name.DealViewControllerSelectBanner:
            
            let paramList = notification.object as? [Any]
            if let banner = paramList?.first as? HeaderData, let gnbName = paramList?.last as? String {
                
                let webVC  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webVC.urlString = banner.linkUrl
                webVC.titleText = gnbName
                self.navigationController?.pushViewController(webVC, animated: true)
            }
            
        default:
            break
        }
    }
    
    @IBAction func onClickSearchBar(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name.ClickHomeSearchBar, object: nil)
        
        if let currentVC =  self.contentPageViewController?.viewControllers?.first as? HomeChildViewControllerProtocol {
            
            TrackingManager.getMetaData().searchFunnels = currentVC.gnbData?.name ?? ""
        }
    }
}

extension HomeViewController : PagerSlidingTabViewDelegate{
    
    func didSelectItem(index: Int) {
        
        DispatchQueue.main.async {
            self.setCurrentPage(index: index)
        }
    }
}


// 페이지뷰 컨트롤러 델리게이트
extension HomeViewController : UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        let vc =  pageViewController.viewControllers?[0]
        
        pager?.setPositionIndex(index: (vc?.view?.tag)!, animated: true)
        
        if let _ = vc as? DealViewController {
            
        } else {
            self.showMapButton.isHidden = true
        }
        
        TrackingManager.getMetaData().gnbName = DataManager.getGNBList()[(vc?.view?.tag)!].name!
        TrackingManager.V_1710120001(current: "Deal List")
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if self.vcArray.count <= 1 {
            return nil
        }
        
        var index = viewController.view.tag - 1
        if index < 0 {
            index = self.vcArray.count - 1;
        }
        
        print("viewControllerBefore \(index)")
        return self.vcArray[index];
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if self.vcArray.count <= 1 {
            return nil
        }
        
        var index = viewController.view.tag + 1
        if index >= self.vcArray.count {
            index = 0
        }
        
        print("viewControllerAfter \(index)")
        return self.vcArray[index];
        
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        
        return self.vcArray.count
    }
}

