//
//  BestDealCollectionViewCell.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 11..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class BestDealCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var distanceView: UIView!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var originalPrice: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var rank: UIButton!
    
    @IBOutlet weak var priceLeftConstraint: NSLayoutConstraint!
    var dealList: [CategoryDealData]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        distanceView.layer.cornerRadius = 47/2
        
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(onClickDistanceView))
        self.distanceView.addGestureRecognizer(gesture)
   }
    
    func onClickDistanceView(){
        print("func onClickDistanceView()")
        NotificationCenter.default.post(name: Notification.Name.RequestShowMapViewForOneItem, object: self.dealList)
    }
    
    func setBest10Color(isBest10:Bool) {
        
        if isBest10 == true {
            self.rank.backgroundColor = UIColor.init(red: 255/255, green: 73/255, blue: 73/255, alpha: 0.5)
        }
        else {
            self.rank.backgroundColor = UIColor.init(red: 102/255, green: 102/255, blue: 102/255, alpha: 0.5)
        }
    }
    
    func compose(dealList: [CategoryDealData], titleProperty:String) {

        self.dealList = dealList
        
        if let deal = dealList.first {
            
            self.bgImage.sd_setImage(with: URL.init(string: deal.imageUrl!), placeholderImage: nil)
            self.title.text = deal.value(forKey: titleProperty) as? String
            self.price.text = deal.priceDisplay
            self.originalPrice.text = deal.priceOrgDisplay
            self.distance.text = deal.distance

            self.discount.text = deal.dcRate
            if deal.dcRate != nil && deal.dcRate != "" {
                self.priceLeftConstraint.constant = 8
            } else {
                self.priceLeftConstraint.constant = 0
            }
        }
    }
}
