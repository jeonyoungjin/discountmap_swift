//
//  DealContentCollectionViewCell.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 14..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

class DealContentCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var originalPrice: UILabel!
    @IBOutlet weak var distance: UIButton!

    var dealList:[CategoryDealData]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.distance.layer.cornerRadius = 3
    }
    
    @IBAction func onClickDistanceView(_ sender: Any) {
        print("func onClickDistanceView()")
        NotificationCenter.default.post(name: Notification.Name.RequestShowMapViewForOneItem, object: self.dealList)
    }

    
    func compose(dealList: [CategoryDealData], titleProperty: String) {
        
        self.dealList = dealList
        
        if let deal = dealList.first {
            
            let attributeString = NSMutableAttributedString.init()
            attributeString.append(NSAttributedString.init(string: deal.value(forKey: titleProperty) as! String,
                                                           attributes: [NSForegroundColorAttributeName: UIColor.init(red: 2/255, green: 2/255, blue: 2/255, alpha: 1),
                                                                        NSFontAttributeName : UIFont(name: "AppleSDGothicNeo-Regular", size: 13)!]))
            
            
            let paragraphStyle = NSMutableParagraphStyle.init()
            paragraphStyle.lineSpacing = 2
            attributeString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSRange.init(location: 0, length: attributeString.length))

            self.title.attributedText = attributeString
            
            self.bgImage.sd_setImage(with: URL.init(string: deal.imageUrl!), placeholderImage: nil)
            self.discount.text = deal.dcRate
            self.price.text = deal.priceDisplay
            self.originalPrice.text = deal.priceOrgDisplay
            self.distance.setTitle("  \(deal.distance!)", for: .normal)
        }
    }

}
