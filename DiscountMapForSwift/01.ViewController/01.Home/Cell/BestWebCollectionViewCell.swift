//
//  BestWebCollectionViewCell.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 8..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

//extension Notification.Name {
//    
//    static let WebCellLoadComplete = Notification.Name("WebCellLoadComplete")
//    static let WebCellClickLink = Notification.Name("WebCellClickLink")
//}


class BestWebCollectionViewCell : UICollectionViewCell {

    // 웹뷰
    @IBOutlet weak var webView: UIWebView!
    
    let urlString = "http://m.wemakeprice.com/m/event/p/mapsample/app"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.webView.delegate = self
        self.webView.scrollView.isScrollEnabled = false
        self.webView.loadRequest(URLRequest(url: URL(string: urlString)!))
        self.webView.scalesPageToFit = false
    }
}

extension BestWebCollectionViewCell : UIWebViewDelegate, WMPLinkSpec {
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
//        NSUInteger contentHeight = [[webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.body.scrollHeight;"]] intValue];
        
        
        guard let n = NumberFormatter().number(from: webView.stringByEvaluatingJavaScript(from: "document.body.scrollHeight;")!) else { return }
        
        var frame =  self.webView.frame
        frame.size.height = 1
        self.webView.frame = frame
        
        let size = self.webView.sizeThatFits(CGSize.zero)

        let height = self.webView.scrollView.contentSize.height
        
        NotificationCenter.default.post(name: Notification.Name.WebCellLoadComplete, object: size.height)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if request.url?.absoluteString == urlString {
            return true
        }
        
        if let resultDic = getParsedData(urlScheme: (request.url?.absoluteString)!) {
            
            NotificationCenter.default.post(name: Notification.Name.WebCellClickLink, object: resultDic)
            
            return false
        }
        
        return true
    }
}


