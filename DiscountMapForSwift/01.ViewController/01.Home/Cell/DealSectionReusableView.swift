//
//  DealSectionReusableView.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 10. 17..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import UIKit

class DealSectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var title: UILabel!
}
