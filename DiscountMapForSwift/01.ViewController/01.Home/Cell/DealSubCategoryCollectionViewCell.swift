//
//  DealSubCategoryCollectionViewCell.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 14..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

extension Notification.Name {
    static let DealSubCategoryChangeHeight = NSNotification.Name("DealSubCategoryChangeHeight")
    static let DealSubCategorySelectItem = NSNotification.Name("DealSubCategorySelectItem")
}

class DealSubCategoryCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var contentCollectionView: UICollectionView!
    
    var subCategoryList: [GNBData_children]? {
        
        willSet(val) {
            
            DispatchQueue.main.async {
                
//                self.contentCollectionView.selectItem(at: IndexPath.init(row: 0, section: 0), animated: false, scrollPosition: .left)
            }
        }
        
//        didSet(val) {
//
//            DispatchQueue.main.async {
//                NotificationCenter.default.post(name: Notification.Name.DealSubCategoryChangeHeight,
//                                                object: [self, self.contentCollectionView.contentSize.height])
//            }
//        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentCollectionView.delegate = self
        self.contentCollectionView.dataSource = self
        
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: Notification.Name.DealSubCategoryChangeHeight,
                                            object: [self, self.contentCollectionView.contentSize.height])
        }
    }
    
    func setVisibleCell(index: Int) {
        
        self.contentCollectionView.selectItem(at: IndexPath.init(row: index, section: 0), animated: false, scrollPosition: .left)
    }
    
    func compose() {
        
    }
}

extension DealSubCategoryCollectionViewCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets.init(top: 0, left: 16, bottom: 0, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let cellWidth = (UIScreen.main.bounds.size.width - (16*2+4))/3
//        let ratio = cellWidth/113

        return CGSize.init(width: cellWidth, height: 32)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let list = self.subCategoryList {
            return list.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoryCell
        cell.title.text = (self.subCategoryList?[indexPath.row].name)!
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: Notification.Name.DealSubCategorySelectItem, object: [self, indexPath.row])
    }
}
