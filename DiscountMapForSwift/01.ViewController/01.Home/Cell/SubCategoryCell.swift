//
//  SubCategoryCell.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 19..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

class SubCategoryCell : UICollectionViewCell {
    
    @IBOutlet weak var title: UILabel!
    
    override var isSelected: Bool {

        willSet(selected) {

            if selected == true {

                self.title.backgroundColor = UIColor.init(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
                self.title.layer.borderWidth = 1

            } else {
                
                self.title.backgroundColor = UIColor.white
                self.title.layer.borderWidth = 0
            }
        }
    }
}
