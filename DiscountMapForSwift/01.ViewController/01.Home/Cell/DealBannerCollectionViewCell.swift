//
//  DealBannerCollectionViewCell.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 14..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

class DealBannerCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var bannerImage: UIImageView!
}
