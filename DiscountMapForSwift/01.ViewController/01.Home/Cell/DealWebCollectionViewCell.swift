
//
//  File.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 14..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

//class DealWebCollectionViewCell : UICollectionViewCell {
//
//    @IBOutlet weak var webView: UIWebView!
//
//    var requestUrl: String?
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//
//        if let url = self.requestUrl {
//
//            self.webView.loadRequest(URLRequest.init(url: URL.init(string: url)!))
//        }
//    }
//}

extension Notification.Name {
    
    static let WebCellLoadComplete = Notification.Name("WebCellLoadComplete")
    static let WebCellClickLink = Notification.Name("WebCellClickLink")
}


class DealWebCollectionViewCell : UICollectionViewCell {
    
    // 웹뷰
    @IBOutlet weak var webView: UIWebView!
    
//    let urlString = "http://m.wemakeprice.com/m/event/p/mapsample/app"
    var requestUrl: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.webView.delegate = self
        self.webView.scrollView.isScrollEnabled = false
        self.webView.scalesPageToFit = false
        
        
//        if let url = self.requestUrl {
//            
//            self.webView.loadRequest(URLRequest.init(url: URL.init(string: url)!))
//        }
    }
}

extension DealWebCollectionViewCell : UIWebViewDelegate, WMPLinkSpec {
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        //        NSUInteger contentHeight = [[webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.body.scrollHeight;"]] intValue];
        
        
        guard let n = NumberFormatter().number(from: webView.stringByEvaluatingJavaScript(from: "document.body.scrollHeight;")!) else { return }
        
        var frame =  self.webView.frame
        frame.size.height = 1
        self.webView.frame = frame
        
        let size = self.webView.sizeThatFits(CGSize.zero)
        
        let height = self.webView.scrollView.contentSize.height
        
        NotificationCenter.default.post(name: Notification.Name.WebCellLoadComplete, object: [self,size.height])
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if request.url?.absoluteString == self.requestUrl {
            return true
        }
                
        if let resultDic = getParsedData(urlScheme: (request.url?.absoluteString)!) {
            
            NotificationCenter.default.post(name: Notification.Name.WebCellClickLink, object: resultDic)
            
            return false
        }
        
        return true
    }
}

