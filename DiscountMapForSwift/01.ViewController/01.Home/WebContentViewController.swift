//
//  WebContentViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 14..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

enum WebViewShowType {
    case exclusive
    case unrestricted
}

protocol WMPLinkSpec {
    
}

extension WMPLinkSpec {
    
    // 이벤트 배너 데이터 파싱
    func getParsedData(urlScheme: String) -> [String: Any]? {
        
        if let range = urlScheme.range(of: "wemakeprice_json_action=") {
            
            if let json =  urlScheme.substring(from: range.upperBound).removingPercentEncoding {
                if let data = json.data(using: .utf8) {
                    do {
                        
                        let dic = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                        return dic
                        
                    } catch {
                        
                        return nil
                    }
                }
            }
        }
        
        return nil
    }
    
    // cmd 액션 정의
}

extension WebContentViewController {

    func runCmdAction(urlString: String) -> Bool {
        
        guard let separate = urlString.split(separator: "?") as [String.SubSequence]?, separate.last?.contains("cmd=") == true else {
            return true
        }
        
        let parsedParam = separate.last?.split(separator: "&")
        var paramDic = [String:String]()
        
        for param in parsedParam! {
            
            let result = param.split(separator: "=") as [Substring]
            
            if paramDic[String(describing: result.first!)] == nil {
                paramDic[String(describing: result.first!)] = String(describing: result.last!)
            }
            
        }
        
        switch paramDic["cmd"] {
        case "blank_in"?:
            let webVC  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            webVC.urlString = paramDic["r_url"]
            webVC.titleText = paramDic["title"]?.removingPercentEncoding
            
//            if let animate = paramDic["animate"] {
//
//                if animate == "push" {
                    self.navigationController?.pushViewController(webVC, animated: true)
//                }
//                else if animate == "modal" {
//                    self.present(webVC, animated: true, completion: nil)
//                }
//
//            }
            
        case "blank"?:
            break
            
        case "dealInfo"?:
            let webVC  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            webVC.urlString = "http://m.wemakeprice.com/m/deal/adeal/\(paramDic["deal_id"]!)"
            self.navigationController?.pushViewController(webVC, animated: true)

        case "close"?:
            
            if self.navigationController != nil {
                self.navigationController?.popViewController(animated: true)
            } else {
                dismiss(animated: true, completion: nil)
            }
            
            guard self.navigationController?.viewControllers != nil, self.navigationController?.viewControllers.count != 0 else {
                break
            }
            
            if let script = paramDic["script"] {
                for vc in (self.navigationController?.viewControllers)! {
                    if let webVC = vc as? WebViewController {
                        
                        webVC.webVC?.webView.reload()
                        webVC.webVC?.webView.stringByEvaluatingJavaScript(from: script.removingPercentEncoding!)
                    }
                }
            }
            
        case "map"?:
            let deal = CategoryDealData.init(dict: ["latitude":paramDic["lat"]!,
                                                    "longitude":paramDic["lng"]!,
                                                    "markerID":0])
            NotificationCenter.default.post(name: Notification.Name.RequestShowMapViewClearMode, object: [deal])
            
        default:
            break
        }
        
        return false
    }
}


class WebContentViewController : UIViewController {
    
    var gnbData: GNBData? {
        
        didSet(newVal) {
            self.urlString = (gnbData?.link?.href)!
            if let accessToken = UserPreferenceManager.getAccessToken(), accessToken != "" {
                self.urlString = urlString.replacingOccurrences(of: "MEMBER_TOKEN", with: accessToken)
            } else {
                self.urlString = urlString.replacingOccurrences(of: "MEMBER_TOKEN", with: "")
            }
            
            self.webView.loadRequest(URLRequest(url: URL(string: self.urlString)!))
            
            if gnbData?.name?.lowercased() == "there" || gnbData?.name?.contains("데얼") == true || gnbData?.name?.contains("해외여행") == true {
                NotificationCenter.default.addObserver(forName: Notification.Name.ISPOpenUrlResponse, object: nil, queue: nil, using: { [weak self] (noti) in
                    
                    if let url = noti.object as? URL {
                        self?.webView.loadRequest(URLRequest(url: url))
                    }
                })
            }
        }
    }
    
    var showType: WebViewShowType = .exclusive
    
    var urlString: String = ""
    
    var type: Int {
        get {
            return 0
        }
        
        set(newValue) {
            self.showType = (type == 2) ? .unrestricted : .exclusive
        }
    }
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView.delegate = self
        self.webView.scrollView.delegate = self
        
        
//        self.webView.loadRequest(URLRequest(url: URL(string: "http://www.naver.com")!))
    }
    
    func loginSuccessHandler(notification: Notification) {
        
        if let url = notification.object as? String {
        
            self.webView.loadRequest(URLRequest.init(url: URL.init(string: url)!))
        } else {
            self.webView.reload()
        }
        
        // 한번 쓰고 버린다.
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UserLoginSuccess, object: nil)
    }
}

extension WebContentViewController : UIWebViewDelegate, WMPLinkSpec {
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if request.url?.absoluteString == urlString { //}|| request.url?.absoluteString.contains("m.wemakeprice.com") == false {
            return true
        }
        
        print (request.url?.absoluteString)
        
        if let resultDic = getParsedData(urlScheme: (request.url?.absoluteString)!) {
            
            if self.showType == .unrestricted {
                
                let webVC  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webVC.urlString = resultDic["value"] as? String
                webVC.type = resultDic["type"] as? Int
                
                if let nav = self.navigationController {
                    
                    nav.pushViewController(webVC, animated: true)
                    var webVcCount = 0
                    var removeVCList = [WebViewController]()
                    for vc in nav.viewControllers.reversed() {
                        
                        if vc is WebViewController {
                            
                            // 3개 이후부터 제거
                            if webVcCount > 3 {
                                removeVCList.append(vc as! WebViewController)
                            }
                            
                            webVcCount += 1
                        }
                    }
                    
                    for vc in removeVCList {
                        vc.removeFromParentViewController()
                    }
                }
            } else {
                
                self.urlString = resultDic["value"] as! String
                self.webView.loadRequest(URLRequest.init(url: URL.init(string: self.urlString)!))
            }
            
            return false
        }
        
        // 로그인 컨트롤러 생성
        if request.url?.absoluteString.contains("/signin?") == true {
            if let loginVC  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {

                // rurl
//                loginVC.urlString = DataManager.getMyData()?.links?.signin?.href
                loginVC.urlString = request.url?.absoluteString
                self.navigationController?.present(loginVC, animated: true, completion: nil)
                
                NotificationCenter.default.addObserver(self, selector: #selector(loginSuccessHandler(notification:)), name: Notification.Name.UserLoginSuccess, object: nil)
            }
            return false
        }
        
        if request.url?.absoluteString.contains("/member/login/?") == true {
            if let loginVC  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
                
                loginVC.urlString = DataManager.getMyData()?.links?.signin?.href
                self.navigationController?.present(loginVC, animated: true, completion: nil)
                
                NotificationCenter.default.addObserver(self, selector: #selector(loginSuccessHandler(notification:)), name: Notification.Name.UserLoginSuccess, object: nil)
            }
            
            return false
        }
        
        // 위메프 cmd 데이터
        if runCmdAction(urlString: (request.url?.absoluteString)!) == false {
            return false
        }
        
        // 타이틀 변경
        if request.url?.absoluteString.contains("order.wemakeprice.com/m/order/pay") == true {
            if let me = self.parent as? WebViewController {
                me.titleText = "결제하기"
            }
        }
        
        if TrackingManager.getMetaData().isOrderClick == true {
            
            TrackingManager.getMetaData().isOrderClick = false
            TrackingManager.V_170810002()
            TrackingManager.E_170810001(label: (request.url?.absoluteString)!)
        }
        
        // 트래킹 데이터 추출
        if request.url?.absoluteString.contains("m.wemakeprice.com/m/order/confirm_cart_order") == true {
            
            if let subString = request.url?.absoluteString.split(separator: "/").last {
                
                NetworkManager.sendOrderInfo(orderId: String(subString))
            }
            
        } else if request.url?.absoluteString.contains("order.wemakeprice.com/m/order/pay") == true {
            TrackingManager.V_170810001()
            
        } else if request.url?.absoluteString.contains("order.wemakeprice.com/pay/gateway") == true {
            
            TrackingManager.getMetaData().isOrderClick = true
        }
        
        return true
    }
}

extension String {
    //: ### Base64 encoding a string
    func base64Encoded() -> String? {
        if let data = self.data(using: .utf8) {
            return data.base64EncodedString()
        }
        return nil
    }
    //: ### Base64 decoding a string
    func base64Decoded() -> String? {
        if let data = Data(base64Encoded: self) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
}

extension WebContentViewController : UIScrollViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let translation = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
        
        print ("scrollViewWillEndDragging \(translation.y)")
        
        if translation.y > 0 {
            NotificationCenter.default.post(name: Notification.Name.ChangeHeaderConstraint, object: 0)
        } else {
            NotificationCenter.default.post(name: Notification.Name.ChangeHeaderConstraint, object: -42)
        }
    }
}
