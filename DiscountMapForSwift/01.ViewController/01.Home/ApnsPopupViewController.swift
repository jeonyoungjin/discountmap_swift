//
//  ApnsPopupViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2018. 1. 7..
//  Copyright © 2018년 Young jin Jeon. All rights reserved.
//

import UIKit

class ApnsPopupViewController: UIViewController {

    var completion:(()->())?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickAllow(_ sender: Any) {
        
        self.dismiss(animated: true) { [weak self] in
        
            if let delegate =  UIApplication.shared.delegate as? AppDelegate {
                delegate.registerForNotifications()
                self?.completion?()
            }
        }
    }
    
    @IBAction func onClickReject(_ sender: Any) {
        
        self.dismiss(animated: true) { [weak self] in
            self?.completion?()
        }
    }
}
