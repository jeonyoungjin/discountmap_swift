//
//  BestViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 8..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

class BestViewController : UIViewController, HomeChildViewControllerProtocol {

    var categoryMetaData: [CategoryMetaData]?
    
    var currentCategoryMetaData: CategoryMetaData!

    // GNB 데이터
    var gnbData: GNBData?

}

//    var categoryMetaData: [CategoryMetaData]?
//
//    var currentCategoryMetaData: CategoryMetaData!
//
//
//    @IBOutlet weak var collectionView: UICollectionView!
//
//    var categoryMetaData = CategoryMetaData()
//
//    var contentWebCellHeight: CGFloat = 1024
//
//    var response: NetworkResponse?
//
//    var webCell: BestWebCollectionViewCell?
//
//    @IBOutlet weak var contentCollectionView: UICollectionView!
//
//    // GNB 데이터
//    var gnbData: GNBData?
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        NotificationCenter.default.addObserver(self, selector: #selector(handle(notification:)), name: Notification.Name.WebCellLoadComplete, object: nil)
//
//        self.response = { [weak self] (result, error) in
//
//            self?.categoryMetaData.dealArray.append(contentsOf: result[0] as! [CategoryDealData])
//            self?.categoryMetaData.uniqueSet = NSOrderedSet.init(array: (self?.categoryMetaData.dealArray.map{$0.dealID})!)
//            self?.categoryMetaData.metaData = result[1] as? DealMetaData
//            self?.categoryMetaData.titlePropertyName = result[3] as? String
//            self?.categoryMetaData.canRequestData = true
//
//            self?.contentCollectionView.reloadData()
//        }
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//
//        let location = LocationManager.autoLocation()?.coordinate
//        NetworkManager.getDealList(param: ["requestURL":(self.gnbData?.link?.href)!,
//                                           "lon": location?.longitude ?? CLLocationDegrees.init(0),
//                                           "lat": location?.latitude ?? CLLocationDegrees.init(0),
//                                           "page": 1],
//                                   uniqueList: categoryMetaData.uniqueSet,
//                                   addCategory: true,
//                                   completion: self.response)
//
//
//    }
//
//    func handle(notification: Notification) {
//
//        self.contentWebCellHeight = notification.object as! CGFloat
//
////        self.collectionView.reloadData()
////        self.contentCollectionView.performBatchUpdates({
//                self.contentCollectionView.reloadSections(IndexSet.init(integer: 0))
////            self.contentCollectionView.reloadItems(at: [IndexPath.init(row: 0, section: 0)])
////        }, completion: { (finished:Bool) -> Void in
////        })
//
//    }
//}
//
//extension BestViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        if indexPath.row == 0 {
//            return CGSize.init(width: UIScreen.main.bounds.size.width, height: self.contentWebCellHeight)
//        } else {
//            return CGSize.init(width: UIScreen.main.bounds.size.width, height: 276)
//        }
//    }
//
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//
//        return self.categoryMetaData.uniqueSet.count
//    }
//
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        if indexPath.row == 0 {
//
//            if let webCell = self.webCell {
//                return webCell
//            } else {
//                self.webCell = collectionView.dequeueReusableCell(withReuseIdentifier: "WebContentCell", for: indexPath) as! BestWebCollectionViewCell
//                return self.webCell!
//            }
//        }
//
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BestDealCell", for: indexPath) as! BestDealCollectionViewCell
//        cell.compose(dealList: self.getCategoryDealListFromOrderUniqueListIndex(index: indexPath.row-1))
//        cell.rank.setTitle(String.init(indexPath.row), for: .normal)
//
//        if indexPath.row == self.categoryMetaData.uniqueSet.count - Int(((self.categoryMetaData.metaData?.limit?.floatValue)! * 0.2)) &&
//            self.categoryMetaData.canRequestData == true &&
//            self.categoryMetaData.dealArray.count == (self.categoryMetaData.metaData?.limit?.intValue)! * (self.categoryMetaData.metaData?.page?.intValue)! {
//
//            self.categoryMetaData.canRequestData = false
//
//            let location = LocationManager.autoLocation()?.coordinate
//            NetworkManager.getDealList(param: ["requestURL":(self.gnbData?.link?.href)!,
//                                               "lon": location?.longitude ?? 0,
//                                               "lat": location?.latitude ?? 0,
//                                               "page": (self.categoryMetaData.metaData?.page?.intValue)! + 1],
//                                       uniqueList: categoryMetaData.uniqueSet,
//                                       addCategory: true,
//                                       completion: self.response)
//        }
//
//        return cell
//    }
//
//    func getCategoryDealListFromOrderUniqueListIndex(index: Int) -> [CategoryDealData] {
//        return self.categoryMetaData.dealArray.filter{$0.dealID == (self.categoryMetaData.uniqueSet.object(at: index) as! NSNumber)}
//    }
//}
//
//extension BestViewController : UIScrollViewDelegate {
//
//    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//
//        let translation = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
//
//        print ("scrollViewWillEndDragging \(translation.y)")
//
//        if translation.y > 0 {
//            NotificationCenter.default.post(name: Notification.Name.ChangeHeaderConstraint, object: 0)
//        } else {
//            NotificationCenter.default.post(name: Notification.Name.ChangeHeaderConstraint, object: -52)
//        }
//    }
//}
//
