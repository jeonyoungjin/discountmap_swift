//
//  TutorialViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 21..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

class TutorialViewController : UIViewController {
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var scrollViewWidthConstraint: NSLayoutConstraint!
    
    
    @IBAction func onClickMultiButton(_ sender: Any) {
        
        if self.pageController.currentPage == 0 {
            
            self.scrollView.setContentOffset(CGPoint.init(x: self.scrollViewWidthConstraint.constant/2, y: 0), animated: true)
        }
        else if self.pageController.currentPage == 1 {
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scrollViewWidthConstraint.constant = UIScreen.main.bounds.size.width * 2
        self.startButton.layer.borderWidth = 1
        self.startButton.layer.borderColor = UIColor.init(red: 138/255, green: 144/255, blue: 148/255, alpha: 0.8).cgColor
        self.startButton.layer.cornerRadius = self.startButton.frame.size.height / 2
    }
}

extension TutorialViewController : UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        struct Temp {
            static var previousPage = 0
        }
        
        let pageWidth = scrollView.frame.size.width
        let fractionalPage = scrollView.contentOffset.x / pageWidth
        let page = lround(Double(fractionalPage))
        
        if Temp.previousPage != page {
            Temp.previousPage = page
            self.pageController.currentPage = page
            
            if self.pageController.currentPage == 0 {
                self.startButton.setTitle("다음", for: .normal)
            } else {
                self.startButton.setTitle("시작하기", for: .normal)
            }
        }
    }
}
