//
//  MapViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 13..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import Sheriff
import SDWebImage

enum MapShowType {
    
    case showFilterMode    // 타이틀, GPS, 재검색
    case showOneItemMode   // 타이틀, GPS
    case showAllDealMode   // 타이틀, GPS, 재검색, 필터선택
    case showDefaultMode   // 타이틀, 서브타이틀, GPS, 재검색, 필터선택
    case showClaerMode     // 마커 하나만 달랑 표시
}

class MapViewMetaData {
    var showType: MapShowType = .showFilterMode
    var currentCategoryMetaData : CategoryMetaData!
    var categoryMetaData : [CategoryMetaData]?
    var gnbData : GNBData?
    
    init(showType:MapShowType, currentCategoryMetaData: CategoryMetaData, categoryMetaData: [CategoryMetaData]?, gnbData: GNBData?) {
        
        self.showType = showType
        self.currentCategoryMetaData = currentCategoryMetaData
        self.categoryMetaData = categoryMetaData
        self.gnbData = gnbData        
    }
}

fileprivate var offImageKey: UInt8 = 0
fileprivate var onImageKey: UInt8 = 1

extension Notification.Name {
    static let ChangeLocationFromMapView = Notification.Name("ChangeLocationFromMapView")
}

extension GMSMarker {
    
    var onImage: UIImage? {
        get {
            return objc_getAssociatedObject(self, &onImageKey) as? UIImage
        }
        set(newValue) {
            objc_setAssociatedObject(self, &onImageKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    var offImage: UIImage? {
        get {
            return objc_getAssociatedObject(self, &offImageKey) as? UIImage
        }
        set (newValue) {
            objc_setAssociatedObject(self, &offImageKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}

class MapViewController : UIViewController, UIGestureRecognizerDelegate, HomeChildViewControllerProtocol {
    
    var flagMarker: GMSMarker?
    var prevSelectedDealMarker: GMSMarker?
    var showType: MapShowType = .showFilterMode
    var filterBadgeView: GIBadgeView?
    var markerInfoDic = [NSValue: [CategoryDealData]]()
    var markerDic = [NSValue: GMSMarker]()
    var pager: PagerSlidingTabView?
    var filterdDealArray = [CategoryDealData]()
    
    // HomeChildViewControllerProtocol
    
    var categoryMetaData: [CategoryMetaData]?
    var currentCategoryMetaData: CategoryMetaData!
    var gnbData: GNBData?
    
    deinit {
        print("deinit MapViewController!!!")
    }
    
    
    @IBOutlet weak var titleButton: UIButton!
    @IBOutlet weak var blurPopupView: MapBlurView!
    @IBOutlet weak var storeCollectionView: MapViewStoreCollectionView!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var gpsButton: UIButton!
    @IBOutlet weak var pagerContainerView: UIView!
    @IBOutlet weak var pagerContainerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var researchButton: UIButton!
    
    @IBAction func onClickBackButton(_ sender: Any) {
        
        self.dismiss(animated: true){
            
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    @IBAction func onClickTitleButton(_ sender: Any) {
        
        if showType == .showAllDealMode {
            
            if self.blurPopupView.alpha == 1 {
                closeBlurPopup()
            } else {
            
                self.showBlurPopup()
            }
        }
    }
    
    
    @IBAction func onClickReSearch(_ sender: UIButton) {
        
        NotificationCenter.default.post(name: Notification.Name.ChangeLocationFromMapView, object: nil)
        
        sender.isHidden = true
        
        let coordinate = self.mapView.camera.target
        let location = CLLocation.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
        LocationManager.setManualLocation(location: location,
                                          forceAddressName: nil) {
      
                                            self.changeTitleLabel()
        }
        
        // 마커 제거
        self.mapView.clear()
        self.addFlagMarker()
        
        self.requestDealList()
        
    }
    
    @IBAction func onClickCurrentLocation(_ sender: UIButton) {
        
//        showBlurPopup()
        
        if LocationManager.getIsCanReadGPSStatus() {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                sender.isSelected = true
            })
            
            self.flagMarker = nil
            self.researchButton.isHidden = false
            
            let location = self.mapView.myLocation
            self.mapView.animate(toLocation: CLLocationCoordinate2DMake((location?.coordinate.latitude)!, (location?.coordinate.longitude)!))
        } else {
            
            LocationManager.initLocationWithResponse(response: nil)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // You might want to check if this is your embed segue here
        // in case there are other segues triggered from this view controller.
        
        let vc = segue.destination
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        vc.view.layoutIfNeeded()
    }
    
    func getDealList() -> [CategoryDealData] {
        
        if self.filterdDealArray.count != 0 {
            return self.filterdDealArray
        }
        
        self.filterdDealArray = self.currentCategoryMetaData.dealArray.filter{$0.dealID?.intValue != 0}
        
        return self.filterdDealArray
    }
    
    func setMapViewMetaData( metaData: MapViewMetaData) {
        
        self.showType = metaData.showType
        self.currentCategoryMetaData = metaData.currentCategoryMetaData
        self.categoryMetaData = metaData.categoryMetaData
        self.gnbData = metaData.gnbData
    }
    
    func setMapViewMetaData( type: MapShowType,
                             currentMetaData: CategoryMetaData,
                             categoryMetaData: [CategoryMetaData]?,
                             gnbData: GNBData?) {
        
        self.showType = type
        self.currentCategoryMetaData = currentMetaData
        self.categoryMetaData = categoryMetaData
        self.gnbData = gnbData
    }
    
    func handle(notification: Notification) {

        switch notification.name {
        case Notification.Name.CloseMapViewBlurPopup:
            self.closeBlurPopup()
        
        case Notification.Name.AllDealContentSelectItem:
                if  let index = (notification.object as! [Any])[1] as? IndexPath {

                    self.currentCategoryMetaData.canRequestData = true
                    self.currentCategoryMetaData.requestUrl = "\(SERVER_HOST)/deals"
                    
                    // 모든상품(-1)은 파라미터 없음
                    if index.section == -1 {
                        self.currentCategoryMetaData.selectFilterCategory = FilterCategoryData_children(dict: ["name" : "모든상품"])
                        self.currentCategoryMetaData.requestParameter = ""
                        
                    } else {
                        self.currentCategoryMetaData.selectFilterCategory = DataManager.getFilterCategory()[index.section].children?[index.row]
                        self.currentCategoryMetaData.requestParameter = (DataManager.getFilterCategory()[index.section].children?[index.row].id)!
                    }
                    
                    
                    self.didSelectItem(index: 999)
                    self.changeTitleLabel()

                    DispatchQueue.main.async {
                        self.closeBlurPopup()
                    }
                    
                    requestDealList()
            }

        default:
            break
        }
    }
    
    func showBlurPopup(animated: Bool = true) {
        
        self.blurPopupView.alpha = 0
        UIView.animate(withDuration: animated ? 0.25 : 0, animations: {
            
            self.blurPopupView.alpha = 1
        }) { (finish) in
            
            self.blurPopupView.containerView.isHidden = false
        }
    }
    
    func closeBlurPopup(animated: Bool = true) {
        self.blurPopupView.containerView.isHidden = true
        self.blurPopupView.alpha = 1
        UIView.animate(withDuration: animated ? 0.25 : 0, animations: {
            
            self.blurPopupView.alpha = 0
        }) { (finish) in
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        TrackingManager.getMetaData().isShowMapView = true
        
        if LocationManager.getIsCanReadGPSStatus() == true {
            self.gpsButton.isSelected = true
        }
        
        if LocationManager.isExistManualLocation() == true {
            self.addFlagMarker()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        TrackingManager.getMetaData().isShowMapView = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true

        NotificationCenter.default.addObserver(self, selector: #selector(handle(notification:)), name:Notification.Name.AllDealContentSelectItem , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handle(notification:)), name:Notification.Name.CloseMapViewBlurPopup , object: nil)

        self.closeBlurPopup(animated: false)

        self.storeCollectionView.storeViewDelegate = self
        self.mapView.delegate = self

        let location = LocationManager.autoLocation()
        let camera = GMSCameraPosition.camera(withLatitude: location?.coordinate.latitude ?? 0,
                                              longitude: location?.coordinate.longitude ?? 0,
                                              zoom: 15)
        self.mapView.camera = camera
        self.mapView.isMyLocationEnabled = true
        //        self.infoLabel.text = self.subTitle;

        self.filterBadgeView = GIBadgeView.init()
        self.filterBadgeView?.isUserInteractionEnabled = false

        //        [self.filterButton addSubview:self.filterBadgeView];

        self.createMarkerInfo()

        // 타이틀 변경
        self.changeTitleLabel()

        if let gnbChildren = self.gnbData?.children {

            // 페이저 빠짐 ㅠ 고생했는뎀
//            self.pager = Bundle.main.loadNibNamed("PagerSlidingTabView", owner: self, options: nil)?.first as? PagerSlidingTabView
//            self.pagerContainerView.addSubview(pager!)
//            self.pager?.delegate = self

            let list = gnbChildren.map{$0.name}
            pager?.setCategoryList(list: list as! [String])

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                DispatchQueue.main.async {
                    self.pager?.setPositionIndex(index: self.currentCategoryMetaData.categoryIndex, animated:false)
                }
            })

        } else {
            self.pagerContainerViewHeightConstraint.constant = 0
        }
        
        self.changeUIMode()
    }
    
    func changeUIMode() {
        
        switch showType {
            
        case .showOneItemMode:
            self.researchButton.alpha = 0
            
        case .showClaerMode:
            self.researchButton.alpha = 0
            self.gpsButton.alpha = 0
            
            
        default:
            break
        }
    }
    
    func changeTitleLabel() {
        
        switch showType {
            
        case .showDefaultMode:
            self.titleButton.setTitle("지도보기", for: .normal)
            
//            if let gnb = gnbData {
//                self.subTitle.text = gnb.name
//            }
            
        case .showAllDealMode:
            
            
            let attributeString = NSMutableAttributedString.init()
            attributeString.append(NSAttributedString.init(string: LocationManager.isExistManualLocation() == true ? LocationManager.autoAddress()! : "내주변",
                                                           attributes: [NSForegroundColorAttributeName: UIColor.init(red: 149/255, green: 149/255, blue: 149/255, alpha: 1),
                                                                        NSFontAttributeName : UIFont(name: "AppleSDGothicNeo-Regular", size: 16)!]))
            
            attributeString.append(NSAttributedString.init(string: " #\(self.currentCategoryMetaData.selectFilterCategory?.name ?? "")",
                                                           attributes: [NSForegroundColorAttributeName: UIColor.init(red: 52/255, green: 120/255, blue: 246/255, alpha: 1),
                                                                        NSFontAttributeName : UIFont(name: "AppleSDGothicNeo-Regular", size: 16)!]))

            self.titleButton.setAttributedTitle(attributeString, for: .normal)
            
        default:
            break
            
        }
        
    }
    
    
    func createStoreCollectionView() {
        
        self.storeCollectionView.dealList = self.getDealList()
        self.storeCollectionView.titleProperty = self.currentCategoryMetaData.titlePropertyName ?? "storeName"
        
        self.storeCollectionView.performBatchUpdates({
            self.storeCollectionView.reloadSections(IndexSet.init(integer: 0))
            self.storeCollectionView.setContentOffset(CGPoint.zero, animated: false)
        }, completion: nil)
    }
    
    func isShowVariousPin(key: NSValue) -> Bool {
        
        if let dealList = self.markerInfoDic[key] {
            
            let markerIDs = dealList.map{$0.markerID}
            let set = NSSet.init(array: markerIDs as! [NSNumber])
            
            return set.count > 1
        }
        
        return false
    }
    
    func getMarker(at index: Int) -> GMSMarker? {
        
        if self.getDealList().count > index {
//        if let data = self.getDealList()[index] {
            
            let coordi = CGPoint.init(x: CGFloat(self.getDealList()[index].latitude?.floatValue ?? 0), y: CGFloat(self.getDealList()[index].longitude?.floatValue ?? 0))
            let key = NSValue.init(cgPoint: coordi)
            
            return self.markerDic[key]
        }
        
        return nil
    }
    
    func addFlagMarker() {
        
        self.flagMarker = GMSMarker.init()
        self.flagMarker?.appearAnimation = .pop
        self.flagMarker?.position = (LocationManager.autoLocation()?.coordinate)!
        self.flagMarker?.iconView = UIImageView.init(image: UIImage.init(named: "flag"))
        self.flagMarker?.map = self.mapView
        self.flagMarker?.zIndex = 1
    }
    
    func convertMarkerImageSize(image: UIImage) -> UIImage {
        
        return UIImage.init(cgImage: image.cgImage!,
                                        scale: 3.9,
                                        orientation: image.imageOrientation)
        
        
    }
    
    func showMarker(_ animated: Bool = true) {
        
        let markerInfoArry = self.markerInfoDic.keys
        var bounds = GMSCoordinateBounds.init()
        
        self.prevSelectedDealMarker = nil
        self.markerDic = [NSValue: GMSMarker]()
        
        for value in markerInfoArry {
            
            let markerInfo = self.markerInfoDic[value]
            
            let position: CGPoint? = value.cgPointValue

            let marker = GMSMarker.init()
            marker.appearAnimation = animated ? .pop : .none
            marker.position = CLLocationCoordinate2D.init(latitude: Double.init(position?.x ?? 0),
                                                          longitude: Double.init(position?.y ?? 0))
            
            self.markerDic.updateValue(marker, forKey: value)
            
            if self.isShowVariousPin(key: value) == true {
                
                marker.onImage = UIImage.init(named: "pinVariousOn")
                marker.offImage = UIImage.init(named: "pinVariousOff")
                
                marker.icon = marker.offImage
            } else {
                
                var offUrlString = ""
                var onUrlString = ""
                let markerData = DataManager.getMarkerList().filter{ ($0.id?.intValue)! == (markerInfo?.first?.markerID?.intValue)!}.first
                if let marker = markerData {
                    offUrlString = (marker.icons?.off)!
                    onUrlString = (marker.icons?.on)!
                }
                
                if let cachedOffImage = SDImageCache.shared().imageFromCache(forKey: offUrlString) {

                    marker.offImage = convertMarkerImageSize(image:cachedOffImage)
                    marker.icon = marker.offImage
                } else {

                    
                    SDWebImageManager.shared().imageDownloader?.downloadImage(with: URL.init(string: offUrlString),
                                                                              options: .highPriority,
                                                                              progress: nil,
                                                                              completed: { [weak self] (image, data, error, finished) in
                                                                                
                                                                                if let img = image {
                                                                                    
                                                                                    SDImageCache.shared().store(img, forKey: offUrlString, completion: {
                                                                                    
                                                                                        marker.offImage = self?.convertMarkerImageSize(image: img)
                                                                                        marker.icon = marker.offImage
                                                                                        
                                                                                        if self?.prevSelectedDealMarker === marker {
                                                                                            marker.icon = marker.onImage
                                                                                        }
                                                                                    })
                                                                                }
                    })
                }
                
                if let cachedOnImage = SDImageCache.shared().imageFromCache(forKey: onUrlString) {

                    marker.onImage = convertMarkerImageSize(image:cachedOnImage)
                } else {
                    SDWebImageManager.shared().imageDownloader?.downloadImage(with: URL.init(string: onUrlString),
                                                                              options: .highPriority,
                                                                              progress: nil,
                                                                              completed: { [weak self] (image, data, error, finished) in
                                                                                
                                                                                if let img = image {
                                                                                    
                                                                                    SDImageCache.shared().store(img, forKey: onUrlString, completion: {
                                                                                    
                                                                                        marker.onImage = self?.convertMarkerImageSize(image: img)
                                                                                        
                                                                                        if self?.prevSelectedDealMarker === marker {
                                                                                            marker.icon = marker.onImage
                                                                                        }

                                                                                    })
                                                                                }
                    })
                }
            }
            marker.map = self.mapView
            
            
            bounds = bounds.includingCoordinate(marker.position)
        }
        
        if animated == true {
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 60))
        } else {
            self.mapView.moveCamera(GMSCameraUpdate.fit(bounds, withPadding: 60))
        }
        
        if self.markerDic.count == 1 {
            if self.mapView.camera.zoom > 14.7 {
                self.mapView.animate(toZoom: 14.7)
            }
        }
        
        
        // 첫번째 마커 셋팅
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if let marker = self.getMarker(at: 0) {
                self.onClickMarker(marker: marker, forceSelect: true)
            }
//        }
    }
    
    func createMarkerInfo(animated: Bool = true) {
        
        let queue = DispatchQueue.init(label: "lock")
        queue.sync { [weak self] in
            
            self?.mapView.clear()
            self?.flagMarker?.map = self?.mapView
            
            if let prevMarker = self?.prevSelectedDealMarker {
                prevMarker.map = nil
            }
            
            self?.prevSelectedDealMarker = nil
            self?.markerDic.removeAll()
            self?.markerInfoDic.removeAll()
            
            let globalQueue = DispatchQueue.global(qos: .default)
            globalQueue.async {
                
                if let getDealList = self?.getDealList() {
                    
                    for data in getDealList {
                        
                        //                        var coordi = CLLocationCoordinate2DMake(data.latitude?.doubleValue ?? 0, data.longitude?.doubleValue ?? 0)
                        //                        let coordi = CGPoint.init(x: data.latitude?.doubleValue ?? 0, y: data.longitude?.doubleValue ?? 0)
                        let coordi = CGPoint.init(x: CGFloat(data.latitude?.floatValue ?? 0), y: CGFloat(data.longitude?.floatValue ?? 0))
                        
                        let key = NSValue.init(cgPoint: coordi)
                        
                        if let _ = self?.markerInfoDic.index(forKey: key) {
                        } else {
                            self?.markerInfoDic.updateValue([CategoryDealData](), forKey: key)
                        }
                        
                        self?.markerInfoDic[key]?.append(data)
                    }
                    
                    DispatchQueue.main.async {
                        if self?.showType != .showClaerMode {
                            self?.createStoreCollectionView()
                        }
                        self?.showMarker(animated)
                    }
                }
            }
        }
    }
    
    func getMarker(index: Int) -> GMSMarker? {
        
        let data = self.getDealList()[index]
        let coordi = CGPoint.init(x: CGFloat(data.latitude?.floatValue ?? 0), y: CGFloat(data.longitude?.floatValue ?? 0))
        let key = NSValue.init(cgPoint: coordi)
        
        return self.markerDic[key]
    }
    
    func requestDealList() {
        LoadingViewManager.show()
        let location = LocationManager.autoLocation()?.coordinate
        NetworkManager.getDealList(param: ["requestURL":(self.currentCategoryMetaData.requestUrl)!,
                                           "lon": location?.longitude ?? 0,
                                           "lat": location?.latitude ?? 0,
                                           "c" : self.currentCategoryMetaData.requestParameter,
                                           "page": 1],
                                   uniqueList: currentCategoryMetaData.uniqueSet,
                                   addCategory: true) { [weak self] (result, error) in
                                    
                                    LoadingViewManager.close()
                                    
                                    // 넘넘 많탐
                                    self?.filterdDealArray.removeAll()
                                    self?.currentCategoryMetaData.dealArray.removeAll()
                                    self?.currentCategoryMetaData.dealArray.append(contentsOf: result[0] as! [CategoryDealData])
                                    self?.currentCategoryMetaData.uniqueSet = NSOrderedSet.init(array: (self?.currentCategoryMetaData.dealArray.filter{$0.dealID?.intValue != 0}.map{$0.dealID!})!)
                                    self?.currentCategoryMetaData.metaData = result[1] as? DealMetaData
                                    self?.currentCategoryMetaData.titlePropertyName = result[3] as? String
                                    self?.currentCategoryMetaData.canRequestData = true
                                    self?.currentCategoryMetaData.bannerArray = result[4] as? [BannerData]
                                    self?.currentCategoryMetaData.sectionTitle = result[5] as? String
                                    self?.currentCategoryMetaData.useMap = result[6] as? NSNumber
                                    self?.currentCategoryMetaData.useRanking = result[7] as? NSNumber
                                    self?.currentCategoryMetaData.listColumn  = result[8] as? NSNumber
                                    
                                    if let header = result[9] as? HeaderData {
                                        self?.currentCategoryMetaData.headerData = header
                                    }
                                    
                                    self?.createMarkerInfo()
        }
    }
}

extension MapViewController : PagerSlidingTabViewDelegate {
    
    func didSelectItem(index: Int) {
        
        guard index != self.currentCategoryMetaData.categoryIndex else{
            return
        }
        
        if self.categoryMetaData != nil {
            pager?.setPositionIndex(index: index)
            self.currentCategoryMetaData = self.categoryMetaData![index]
        }
        
        if self.getDealList().count == 0
        {
            // 리퀘스트
            requestDealList()
            
        } else {
            
            createMarkerInfo()
        }
    }
}

extension MapViewController : MapViewStoreCollectionViewDelegate {
    func currentCisibleCell(index: Int) {
        
        if let marker = self.getMarker(at: index) {
            self.onClickMarker(marker: marker, forceSelect: true)
        }
    }
    
    func didSelectStoreCell(index: Int) {
        
        NotificationCenter.default.post(name: Notification.Name.DealViewControllerSelectItem, object: [self.getDealList()[index], true])
//        let dealData = self.getDealList()[index]
//        let webVC  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//        // 빌더패턴 사용하자
//        webVC.urlString = dealData.linkUrl!
//        webVC.titleText = dealData.dealName
//        webVC.showType = .exclusive
//
//        //메인 GNB 딜리스트 노출 시 , GNB명과 함께 저장 gnbData가 없으면 메인 딜리스트가 아니다
//        if let _ = gnbData {
//            TrackingManager.V_1710120002()
//        }
//
        
//        self.navigationController?.pushViewController(webVC, animated: true)
    }
}

extension MapViewController : GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        
        if gesture == true {
            
            self.researchButton.isHidden = false
            self.gpsButton.isSelected = false
        }
    }
    
    func onClickMarker(marker: GMSMarker, forceSelect: Bool = false) {
        
        self.prevSelectedDealMarker?.zIndex = 0
        
        if self.prevSelectedDealMarker !== marker {
            
            self.prevSelectedDealMarker?.icon = self.prevSelectedDealMarker?.offImage
            
            marker.map = nil
            marker.appearAnimation = .pop
            marker.map = self.mapView
        }
        
        marker.icon = marker.onImage
        marker.zIndex = INT_MAX
        self.mapView.animate(toLocation: marker.position)
        
        let location = CGPoint.init(x: marker.position.latitude, y: marker.position.longitude)
        let key = NSValue.init(cgPoint: location)
        let storeArray = self.markerInfoDic[key]
        
        if forceSelect == false,  let index = self.getDealList().index(of: (storeArray?.first)!){
            
            self.storeCollectionView .setPage(index: index, forceVisible: false)
        }
        
        var selectedlist = [NSNumber]()
        for deal in storeArray! {
            selectedlist.append(NSNumber.init(value: (self.getDealList().index(of: deal))!))
        }
        
        self.storeCollectionView.setSelectedList(list: selectedlist)
        self.prevSelectedDealMarker = marker
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        if marker === self.flagMarker {
            return true
        }
        
        if self.showType != .showClaerMode {
            self.onClickMarker(marker: marker)
        }
        
        return true
    }
}
