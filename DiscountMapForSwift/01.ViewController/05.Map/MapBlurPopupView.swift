//
//  MapBlurPopupView.swift
//  DiscountMapForSwift
//
//  Created by Young jin Jeon on 2017. 10. 24..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let CloseMapViewBlurPopup = Notification.Name("CloseMapViewBlurPopup")
}

class MapBlurPopupView: UIView {

    
    @IBAction func onClickClosePopup(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name.CloseMapViewBlurPopup, object: nil)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
