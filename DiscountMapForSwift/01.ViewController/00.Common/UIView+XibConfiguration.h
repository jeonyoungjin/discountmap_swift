//
//  UIView+XibConfiguration.h
//  DiscountMap
//
//  Created by we on 2017. 6. 12..
//  Copyright © 2017년 we. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView(XibConfiguration)
@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable NSInteger borderWidth;
@property (nonatomic) IBInspectable NSInteger cornerRadius;
@property (nonatomic) IBInspectable BOOL masksToBounds;
@end
