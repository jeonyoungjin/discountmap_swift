//
//  CategorySliderView.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 11..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

protocol CategorySliderDelegate: class {
    func categorySliderDidScroll()
    func categoryDidSelect(index: Int)
}

class CategorySliderView : UICollectionView {
    
//    var dataArray: [GNBData]?
    var dataArray: [String]?
    var cellArray = [CategorySliderCollectionViewCell]()
    var fitScreenWidth = false
    
    weak open var sliderDelegate: CategorySliderDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.delegate = self
        self.dataSource = self
     
        self.register(UINib(nibName:"CategorySliderCollectionViewCell", bundle:nil), forCellWithReuseIdentifier: "CategorySliderCollectionViewCell")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func setCategoryList(list: Array<String>) {
        
        self.dataArray = list
        if let test = self.dataArray {
            
            for (index, _) in test.enumerated() {
                
                let lastSectionIndex = 0
                let lastRowIndex = index
                
                let path = IndexPath(item: lastRowIndex, section: lastSectionIndex)
                let cell = self.dequeueReusableCell(withReuseIdentifier: "CategorySliderCollectionViewCell", for: path)
                
                self.cellArray.append(cell as! CategorySliderCollectionViewCell)
            }
        }

        self.reloadData()
    }
    
    public func getCell(index: Int) -> CategorySliderCollectionViewCell {

        return self.cellArray[index]
    }
    
    public func allCellSelected(selected: Bool) {
        
        for cell in self.cellArray {
            cell.isSelected = selected
        }
    }

}


extension CategorySliderView : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
   
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.isTracking || scrollView.isDragging {
            self.sliderDelegate?.categorySliderDidScroll()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
 
        return (self.dataArray?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.sliderDelegate?.categoryDidSelect(index: indexPath.row)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        if fitScreenWidth {
            return 0
        }
        
        return 22
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.cellArray[indexPath.row]
        
        cell.title.text = self.dataArray?[indexPath.row]
        
        return cell
    }
    
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if fitScreenWidth {
            
            let width = (UIScreen.main.bounds.size.width - 42) / CGFloat((self.dataArray?.count)!)
            
            return CGSize(width: width, height: self.frame.size.height - 0.5)
        
        } else {

            if let name = self.dataArray?[indexPath.row] as NSString? {
                
                var size = name.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)])
//                size.width += 10
                size.height = self.frame.size.height - 0.5
                
                return size
            }
        }
        
        return CGSize(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }
}


