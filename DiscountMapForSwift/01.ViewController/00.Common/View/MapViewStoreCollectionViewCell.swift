//
//  MapViewStoreCollectionViewCell.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 26..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

class MapViewStoreCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var contentImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.init(white: 0, alpha: 0.5).cgColor
        self.layer.shadowOffset = CGSize.init(width: 0.5, height: 0.5)
        self.layer.shadowOpacity = 0.5
    }
    
    override func prepareForReuse() {
        self.isSelected = false
    }
    
    override var isSelected: Bool {
        willSet(newValue) {
            if newValue == true {
                self.layer.borderWidth = 1
            } else {
                self.layer.borderWidth = 0
            }
        }
    }
}
