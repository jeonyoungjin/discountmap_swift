//
//  PagerSlidingTabView.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 11..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

protocol  PagerSlidingTabViewDelegate: class {
    func didSelectItem(index: Int)
}

class PagerSlidingTabView : UIView {
    
    @IBOutlet weak var pointerView: UIView!
    @IBOutlet weak var categorySliderView: CategorySliderView!
    
    @IBOutlet weak var pointerViewOriginX: NSLayoutConstraint!
    @IBOutlet weak var pointerViewWidth: NSLayoutConstraint!
    
    var pointerOriginX : CGFloat = 0.0
    weak var delegate: PagerSlidingTabViewDelegate?
    
    static let CLIP_CELL_OFFSET : CGFloat = 50
    

    func setCategoryList(list: Array<String>) {

        self.categorySliderView.setCategoryList(list: list)
        
        //편의상 이곳에서 제약 추가
        self.superview?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view": self]));
        self.superview?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view": self]));
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.pointerView.alpha = 0
        
        self.categorySliderView.sliderDelegate = self

        // 하단 라인 생성
        DispatchQueue.main.async {
            
            let bottomBorder = CALayer()
            bottomBorder.frame = CGRect.init(x: 0, y: self.frame.size.height-0.5, width: UIScreen.main.bounds.size.width, height: 0.5)
            bottomBorder.backgroundColor = UIColor.init(red: 221/255, green: 221/255, blue: 221/255, alpha: 1).cgColor
            self.layer.insertSublayer(bottomBorder, at: 0)
            
            self.layer.masksToBounds = false
        }
    }
    
    public func setPositionIndex(index :Int, animated: Bool = true) {
        
        let cell = self.categorySliderView.getCell(index: index)
        
        self.pointerOriginX = cell.frame.origin.x
        self.categorySliderView.allCellSelected(selected: false)
        
        UIView.performWithoutAnimation {
            self.categorySliderView.reloadSections(IndexSet(integer: 0))
        }
        
        UIView.animate(withDuration: animated ? 0.5 : 0,
                       delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 1,
                       options:[.curveLinear, .curveEaseInOut],
                       animations: {
                        
                        self.pointerView.alpha = 1
                        
                        cell.isSelected = true
                        
                        let willMoveContentOffsetX = self.pointerOriginX - PagerSlidingTabView.CLIP_CELL_OFFSET
                        
                        if (self.categorySliderView.contentSize.width - willMoveContentOffsetX) > self.frame.size.width {
                            
                            if willMoveContentOffsetX > 0 {
                                self.categorySliderView.setContentOffset(CGPoint(x: willMoveContentOffsetX, y: cell.frame.origin.y), animated: false)
                            } else {
                                self.categorySliderView.setContentOffset(CGPoint(x: 0, y: cell.frame.origin.y), animated: false)
                            }
                        } else {
                           
                            let x = self.categorySliderView.contentSize.width-self.frame.size.width
                            if x > 0 {
                                self.categorySliderView.setContentOffset(CGPoint(x: x, y: cell.frame.origin.y), animated: false)
                            }
                            
                            
                        }
                        
                        self.pointerViewOriginX.constant = cell.frame.origin.x - self.categorySliderView.contentOffset.x
                        self.pointerViewWidth.constant = cell.frame.size.width
                        
                        self.layoutIfNeeded()
                        
        },
                       completion : nil)
        
    }
    
}

extension PagerSlidingTabView : CategorySliderDelegate {
    
    func categorySliderDidScroll() {
        
        self.pointerViewOriginX.constant = -self.categorySliderView.contentOffset.x + self.pointerOriginX
        self.layoutIfNeeded()
    }
    
    func categoryDidSelect(index: Int) {
 
        self.delegate?.didSelectItem(index: index)
    }
}
