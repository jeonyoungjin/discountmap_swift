//
//  MapViewStoreCollectionView.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 26..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

protocol MapViewStoreCollectionViewDelegate: class {
    func currentCisibleCell(index: Int)
    func didSelectStoreCell(index: Int)
}

class MapViewStoreCollectionView : UICollectionView {
    
    var currentCell: UICollectionViewCell?
    var screenRatio: CGFloat = 0
    var selectedCellList : [NSNumber]?
    var cellSize: CGSize = CGSize.init(width: 0, height: 0)
    var dealList: [CategoryDealData]?
    var titleProperty: String = "storeName"
    
    weak var storeViewDelegate: MapViewStoreCollectionViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.delegate = self
        self.dataSource = self
        self.decelerationRate = UIScrollViewDecelerationRateFast
    }
    
    func setSelectedList(list: [NSNumber]) {
        self.selectedCellList = list
        
        UIView.performWithoutAnimation {
            self.reloadSections(IndexSet.init(integer: 0))
        }
    }
    
    func setPage(index: Int, forceVisible: Bool, animated: Bool = false) {
        
        let indexPath = IndexPath.init(row: index, section: 0)
        
        self.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: animated)
        
        if forceVisible == true {
            self.selectedCellList = [NSNumber.init(value: index)]
        }
    }
}

extension MapViewStoreCollectionView: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        guard scrollView.isTracking == true else {
            return
        }
        
        let translation = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
        
//        if let width = self.cellSize.width {
        
            let pageW = self.cellSize.width * screenRatio + 10
            let page = CGFloat(Int(scrollView.contentOffset.x / pageW))
            
            var value: CGFloat = 0
        
            if abs(velocity.x) != 0 {
                
                if velocity.x > 0 {
                    value = 1
                } else  if velocity.x < 0 {
                    value = -1
                }
                
                if abs((pageW * page) - scrollView.contentOffset.x) < 20 {
                    value = 0
                }
                
            } else {
                
                if translation.x == 0 {
                    return
                }
                
                if translation.x > 0 {
                    value = 1
                }
                
                if abs(translation.x) < self.cellSize.width * screenRatio / 2 {
                    value = 0
                }
            }
            
            let newOffset = (page * pageW) + (pageW * value) + ((velocity.x < 0 && velocity.x != 0) ? self.cellSize.width * self.screenRatio + 10 : 0)
            let target = CGPoint.init(x: newOffset, y: 0)
            targetContentOffset.pointee = target
            
            let currentPage = Int(newOffset / pageW)
            if currentPage < (self.dealList?.count)! {
                self.storeViewDelegate?.currentCisibleCell(index: currentPage)
            }
//        }
    }
}

extension MapViewStoreCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.dealList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let deal = self.dealList?[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MapViewStoreCollectionViewCell", for: indexPath) as! MapViewStoreCollectionViewCell
        
        cell.contentImage.sd_setImage(with: URL.init(string: (deal?.imageUrl)!))
        cell.title.text = deal?.value(forKey: self.titleProperty) as? String
        cell.price.text = deal?.priceDisplay
        
        cell.isSelected = false
        if let list = self.selectedCellList , let index = list.index(of: NSNumber.init(value: indexPath.row)) {
            
            if index < list.count {
                cell.isSelected = true
            }
//            if list.index(of: NSNumber.init(value: indexPath.row))! < list.count {
//                cell.isSelected = true
//            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        self.cellSize = (collectionViewLayout as! UICollectionViewFlowLayout).itemSize
        
        self.screenRatio = 1
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.screenRatio *= 1.3
        }
        
        if UIScreen.main.bounds.size.width < 375 {
            self.screenRatio = UIScreen.main.bounds.size.width / 375
        }

        return CGSize.init(width: self.cellSize.width * self.screenRatio, height: self.cellSize.height * self.screenRatio)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
        for cell in self.visibleCells {
            
            if cell != self.cellForItem(at: indexPath) {
                cell.isSelected = false
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.setPage(index: indexPath.row, forceVisible: false)
        }
        
        self.storeViewDelegate?.currentCisibleCell(index: indexPath.row)
        self.storeViewDelegate?.didSelectStoreCell(index: indexPath.row)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let inset = (UIScreen.main.bounds.size.width - self.cellSize.width * screenRatio) / 2
        
        return UIEdgeInsets.init(top: 10, left: inset, bottom: 16, right: inset)
    }
}
