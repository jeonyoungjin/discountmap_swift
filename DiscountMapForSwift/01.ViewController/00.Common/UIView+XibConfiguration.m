//
//  UIView+XibConfiguration.m
//  DiscountMap
//
//  Created by we on 2017. 6. 12..
//  Copyright © 2017년 we. All rights reserved.
//

#import "UIView+XibConfiguration.h"

@implementation UIView(XibConfiguration)

@dynamic borderColor,borderWidth,cornerRadius,masksToBounds;

-(void)setBorderColor:(UIColor *)borderColor{
    [self.layer setBorderColor:borderColor.CGColor];
}

-(void)setBorderWidth:(NSInteger)borderWidth{
    [self.layer setBorderWidth:borderWidth];
}

-(void)setCornerRadius:(NSInteger)cornerRadius{
    [self.layer setCornerRadius:cornerRadius];
}

- (void)setMasksToBounds:(BOOL)masksToBounds{
    [self.layer setMasksToBounds:masksToBounds];
}

@end
