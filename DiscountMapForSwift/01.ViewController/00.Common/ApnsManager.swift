//
//  ApnsManager.swift
//  DiscountMapForSwift
//
//  Created by we on 2018. 1. 7..
//  Copyright © 2018년 Young jin Jeon. All rights reserved.
//

import Foundation
import Appboy_iOS_SDK

final class ApnsManager {
    
    private init(){}
    
    static func setAPNSStatus(_ isOn: Bool, isLocal: Bool = false, completion:((Bool, String?)->())? = nil) {
        
        if isLocal == true {
//            KFKeyValueStore.save(bool: isOn, forKey: CuppingKey.alarmKey)
            UserPreferenceManager.setApnsStatus(isOn: isOn)
            return
        }
        
        NetworkManager.setPushStatus(isOn: isOn, completion: { result, error in
            
            if error != nil {
                completion?(false, result.first as? String)
            } else {
                UserPreferenceManager.setApnsStatus(isOn: isOn)
                Appboy.sharedInstance()?.user.setCustomAttributeWithKey("Push_Status", andStringValue: isOn == true ? "ON" : "OFF")
                DataManager.getMyData()?.enablePush = (isOn == true ? NSNumber.init(value: 1):NSNumber.init(value: 0))
                
                completion?(true, result.first as? String)
            }
        })
    }
    
    static func getAPNSStatus() -> Bool {
        
        return UserPreferenceManager.getApnsStatus()
    }
}
