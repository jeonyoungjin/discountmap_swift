//
//  ViewController.swift
//  DiscountMapForSwift
//
//  Created by Young jin Jeon on 2017. 8. 5..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import UIKit
import SDWebImage

enum TabBarType : Int {
    
    case best   = 0
    case all    = 1
    case search = 2
    case my     = 3
}

extension NSNotification.Name {
    
    static let SearchFieldBeginEditing = NSNotification.Name("SearchFieldBeginEditing")
    static let RequestShowHomeView = NSNotification.Name("RequestShowHomeView")
    static let RequestShowMapView = Notification.Name("RequestShowMapView")
    static let RequestShowMapViewForOneItem = Notification.Name("RequestShowMapViewForOneItem")
    static let RequestShowMapViewClearMode = Notification.Name("RequestShowMapViewClearMode")
}


class MainViewController: UIViewController {
    
    @IBOutlet weak var tabCollectionView: UICollectionView!
    
    var contentPageViewController : UIPageViewController?
    var contentTabBarController : UITabBarController?
    
    var vcArray = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NotificationCenter.default.addObserver(self, selector: #selector(handle(notification:)), name: Notification.Name.ClickHomeSearchBar, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handle(notification:)), name: Notification.Name.DealViewControllerSelectItem, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handle(notification:)), name: Notification.Name.RequestShowMapView, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handle(notification:)), name: Notification.Name.RequestShowMapViewForOneItem, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handle(notification:)), name: Notification.Name.RequestShowMapViewClearMode, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handle(notification:)), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handle(notification:)), name: Notification.Name.RequestShowHomeView, object: nil)
        
        
//        if UserPreferenceManager.isShowApnsPopup() == false {
//            DispatchQueue.main.async {
//                self.performSegue(withIdentifier: "ShowApnsPopup", sender: nil)
//            }
//        } else {
//
            if let delegate =  UIApplication.shared.delegate as? AppDelegate {
                delegate.registerForNotifications()
            }
//        }

        
        DispatchQueue.main.async(){

            self.setCurrentPage(index: 0, forceChange: true)
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        // 메모리가 부족할 경우 캐시를 날려라!
        SDImageCache.shared().clearMemory()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let id = segue.identifier else { return }

        switch id {
            
        case "EmbedPageViewController":
            if let vc = segue.destination as? UIPageViewController {
                
                self.contentPageViewController = vc
                self.contentPageViewController?.delegate = self
                self.contentPageViewController?.dataSource = self
                
                // 페이지뷰컨트롤러 바운스 제거
                for view in (self.contentPageViewController?.view.subviews)! {
                    if (view is UIScrollView) {
                        let scrollView = view as! UIScrollView
                        scrollView.bounces = false
                        break
                    }
                }
            }
            
        case "ShowTabBar":
            if let vc = segue.destination as? UITabBarController {
                self.contentTabBarController = vc
            }
            
        case "ShowMapViewController":
            if let vc = segue.destination as? UINavigationController {
                
                if let mapVC = vc.viewControllers[0] as? MapViewController, let metaData = sender as? MapViewMetaData {
                    mapVC.setMapViewMetaData(metaData: metaData)
                }
            }
        
        case "ShowApnsPopup":
            if let vc = segue.destination as? ApnsPopupViewController {
                vc.completion = { [weak self] in
                    if UserPreferenceManager.isShowTutorial() == false {
                    
                        self?.performSegue(withIdentifier: "ShowTutorial", sender: nil)
                    }
                }
            }
        default: break
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        deepLinkAction()
    }

    func handle( notification: Notification) {
        
        switch notification.name {
        case Notification.Name.RequestShowHomeView:
            self.setCurrentPage(index: 0)
            
        case Notification.Name.ClickHomeSearchBar:
            TrackingManager.getMetaData().isClickTopSearch = true
            self.setCurrentPage(index: 2)
            
        case Notification.Name.DealViewControllerSelectItem:
            
            if let objectArray = notification.object as? [Any], let dealData = objectArray.first as? CategoryDealData, let isSendA_171128002 = objectArray.last as? Bool {
//            if let dealData = notification.object as? CategoryDealData {
                let webVC  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                // 빌더패턴 사용하자
                webVC.urlString = dealData.linkUrl!
                webVC.titleText = dealData.dealName
                webVC.showType = .exclusive
                
                if let nc = UIApplication.topViewController()?.navigationController {
                    nc.pushViewController(webVC, animated: true)
                } else {
                    self.navigationController?.pushViewController(webVC, animated: true)
                }

                switch(self.contentTabBarController?.selectedIndex) {
                    
                case TabBarType.best.rawValue?:
                    TrackingManager.V_1710120002()
                    TrackingManager.E_1710120004(deal:dealData)
                    
                    if isSendA_171128002 == false {
                       TrackingManager.A_171128002(prefix: "")
                       TrackingManager.E_171128002()
                    }
                    
                
                case TabBarType.all.rawValue?:
                    TrackingManager.V_1710120014()
                    TrackingManager.E_1710120011(deal: dealData)
                    if isSendA_171128002 == false {
                        TrackingManager.A_171128002(prefix: "All_Menu")
                    }
                    
                case TabBarType.search.rawValue?:
                    TrackingManager.V_1710120011()
                    TrackingManager.E_1710120010(deal: dealData)
                    if isSendA_171128002 == false {
                        TrackingManager.A_171128002(prefix: "Search")
                    }
                    
//                case TabBarType.my.rawValue:
                default:
                    break
                }
            }
            
        case Notification.Name.RequestShowMapView:
            TrackingManager.getMetaData().isShowMapView = true
            
            self.performSegue(withIdentifier: "ShowMapViewController", sender: notification.object)
            
            switch(self.contentTabBarController?.selectedIndex) {
                
            case TabBarType.best.rawValue?:
                break
            case TabBarType.all.rawValue?:
                TrackingManager.V_1710120013()
                break
            case TabBarType.search.rawValue?:
                TrackingManager.V_1710120010()
                
            //                case TabBarType.my.rawValue:
            default:
                break
            }
                        
        case Notification.Name.RequestShowMapViewForOneItem:
            if let dealList = notification.object as? [CategoryDealData] {

                let categoryMetaData = CategoryMetaData.init()
                categoryMetaData.dealArray = dealList

                let mapMetaData = MapViewMetaData.init(showType: .showOneItemMode,
                                                    currentCategoryMetaData: categoryMetaData,
                                                    categoryMetaData: nil,
                                                    gnbData: nil)
                
                self.performSegue(withIdentifier: "ShowMapViewController", sender: mapMetaData)
            }
            
        case Notification.Name.RequestShowMapViewClearMode:
            if let dealList = notification.object as? [CategoryDealData] {
                
                let categoryMetaData = CategoryMetaData.init()
                categoryMetaData.dealArray = dealList
                
                let mapMetaData = MapViewMetaData.init(showType: .showClaerMode,
                                                       currentCategoryMetaData: categoryMetaData,
                                                       categoryMetaData: nil,
                                                       gnbData: nil)
                
                self.performSegue(withIdentifier: "ShowMapViewController", sender: mapMetaData)
            }
            
        case Notification.Name.UIApplicationDidBecomeActive:
            deepLinkAction()
            
        default:
            break
        }
    }
    
    func deepLinkAction() {
        if DeepLinkManager.canRun == true {
            DeepLinkManager.canRun = false
            
            switch DeepLinkManager.info?.type {
            case "1"?:
                // 랜딩
                break
            case "2"?:
                // 웹뷰 랜딩
                let webVC  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webVC.urlString = DeepLinkManager.info?.value1
                
                DispatchQueue.main.async { [weak self] in
                    self?.navigationController?.pushViewController(webVC, animated: true)
                }
                
                break
            
            case "3"?:
                // 검색결과 이동
                setCurrentPage(index: 2)                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.25, execute: {
                    if self.contentTabBarController?.selectedIndex == 2 {
                        if let SearchNC = self.contentTabBarController?.viewControllers?[2] as?  UINavigationController {
                            for child in SearchNC.childViewControllers {
                                if let searchVC = child as? SearchViewController {
                                    
                                    searchVC.searchTextField.text = DeepLinkManager.info?.value1
                                    searchVC.onClickSearchButton("")
                                }
                            }
                        }
                    }
                })
                
                break
            case "4"?:
                
                setCurrentPage(index: 0)
                DispatchQueue.main.async {
                    if let homeVC = self.contentTabBarController?.viewControllers?.first as?  HomeViewController {
                        
                        //
                        for (index, gnb) in DataManager.getGNBList().enumerated() {
                       
                            if gnb.name == DeepLinkManager.info?.value1 {
                                homeVC.setCurrentPage(index: index)
                                break
                            }
                        }
                        return
                    }
                }
                
                break
            case "5"?:
                
                if DeepLinkManager.info?.value1 == "1" {
                    setCurrentPage(index: 1)
                } else if DeepLinkManager.info?.value1 == "2" {
                    setCurrentPage(index: 3)
                }
                
                break
                
            default:
                break
            }
        }
    }
    
    
    func setCurrentPage(index: Int, forceChange: Bool = false) {
        
        guard self.contentTabBarController?.selectedIndex != index || forceChange == true else {
            return
        }
        
        // 전체보기는 초기화
        if index == TabBarType.all.rawValue {
            if let nc = self.contentTabBarController?.viewControllers![index] as? UINavigationController {
                nc.popToRootViewController(animated: false)
            }
        }
        // 검색은 재 생성
        if index == TabBarType.search.rawValue {
            
            let searchVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchNavigationController") as! UINavigationController
            self.contentTabBarController?.viewControllers![index] = searchVC
            
//            if let nc = self.contentTabBarController?.viewControllers![index] as? UINavigationController{
//
//                if let rootVC = nc.viewControllers.first as? SearchViewController {
//
//                    if rootVC.searchTextField != nil {
//                        rootVC.searchTextField.text = ""
//                    }
//                    rootVC.navigationController?.navigationController?.popToRootViewController(animated: false)
//                }
//                nc.popToRootViewController(animated: false)
//            }
        }
        
        // 내정보
        if index == TabBarType.my.rawValue {
            
            if let nc = self.contentTabBarController?.viewControllers![index] as? UINavigationController {
                nc.popToRootViewController(animated: false)
            }
        }
    
        // 하이라이트 제거
        for cell in self.tabCollectionView.visibleCells {
            cell.isHighlighted = false
            cell.isSelected = false
        }
        
        // 선택한 녀석 하이라이트 생성
        let currentCell = self.tabCollectionView.cellForItem(at: IndexPath.init(row: index, section: 0))
        currentCell?.isHighlighted = true
        
        
        let fromView = self.contentTabBarController?.selectedViewController?.view
        let toView = self.contentTabBarController?.viewControllers![index].view
        
        let viewSize = (toView?.frame)!
        fromView?.superview?.addSubview(toView!)
        
        // [전영진] 전체보기 present 애니메이션
        if index == TabBarType.all.rawValue {
            toView?.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.size.height, width: viewSize.size.width, height: viewSize.size.height)
            
            UIView.animate(withDuration: 0.5,
                           delay: 0,
                           usingSpringWithDamping: 0.95,
                           initialSpringVelocity: 1,
                           options: UIViewAnimationOptions.curveEaseInOut,
                           animations: {
                            toView?.frame = CGRect.init(x: 0, y: 0, width: viewSize.size.width, height: viewSize.size.height)
            }, completion: { finished  in
                
            })
        }
        
        self.contentTabBarController?.selectedIndex = index
        
        if self.contentTabBarController?.selectedIndex == TabBarType.search.rawValue {
            TrackingManager.getMetaData().searchFunnels = "NAV Bar"
        } else if self.contentTabBarController?.selectedIndex == TabBarType.my.rawValue {
            TrackingManager.V_1710120015()
        }
        
        if forceChange != true {
            TrackingManager.E_1710120013(index: index)
        }
    }
}

// 탭바 델리게이트 (컬렉션뷰로 구성)
extension MainViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: 50, height: 44)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let ratioSpace = 30 * (UIScreen.main.bounds.size.width / 375)
        return UIEdgeInsets.init(top: 0, left: ratioSpace, bottom: 0, right: ratioSpace)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    
        let ratioSpace = 30 * (UIScreen.main.bounds.size.width / 375)
        let totalCellWidth = 50*4 + ratioSpace*2
        let totalWithSpace = UIScreen.main.bounds.size.width - CGFloat(totalCellWidth)
        return totalWithSpace / 4
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch TabBarType(rawValue: indexPath.row)!  {
        case .best:
            return collectionView.dequeueReusableCell(withReuseIdentifier: "TabBarHomeCell", for: indexPath)
        case .all :
            return collectionView.dequeueReusableCell(withReuseIdentifier: "TabBarListCell", for: indexPath)
        case .search:
            return collectionView.dequeueReusableCell(withReuseIdentifier: "TabBarSearchCell", for: indexPath)
        case .my:
            return collectionView.dequeueReusableCell(withReuseIdentifier: "TabBarMyCell", for: indexPath)
        }        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        
        if self.contentTabBarController?.selectedIndex == 0 && indexPath.row == 0 {
         
            if let homeVC = self.contentTabBarController?.viewControllers?.first as?  HomeViewController {
                
                homeVC.setCurrentPage(index: 0)
                
                return
            }
        } else if self.contentTabBarController?.selectedIndex == 1 && indexPath.row == 1 {
            
            if let AllNC = self.contentTabBarController?.viewControllers?[1] as?  UINavigationController {
                for child in AllNC.childViewControllers {
                    if !(child is AllDealContentViewController) {
                    
                        child.navigationController?.popViewController(animated: true)
                    }
                }
                
                return
            }
        } else if self.contentTabBarController?.selectedIndex == 2 && indexPath.row == 2 {
            if let SearchNC = self.contentTabBarController?.viewControllers?[2] as?  UINavigationController {
                for child in SearchNC.childViewControllers {
                    if let searchVC = child as? SearchViewController {
                        // 이거슨 핫플레이스 빼기용
                        searchVC.navigationController?.popViewController(animated: true)
                        // 이거슨 결과뷰 빼기용
                        searchVC.innerNavigationController?.popViewController(animated: true)
                        
                        // 뺏으면 지도보기는 숨기자
                        searchVC.showMapButton.isHidden = true
                    }
                }
                return
            }
        } else if self.contentTabBarController?.selectedIndex == 3 && indexPath.row == 3 {
            
            if let myInfoNC = self.contentTabBarController?.viewControllers?[3] as?  UINavigationController {
            
                if !(myInfoNC.viewControllers.last is MyInfoViewController) {
                    myInfoNC.popViewController(animated: true)
                }
            
                return
            }
        }
        
        self.setCurrentPage(index: indexPath.row)
    }
}

// 페이지뷰 컨트롤러 델리게이트
extension MainViewController : UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return nil
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return nil
    }
}
