//
//  HomeViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 7..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

extension Notification.Name {
    
    static let ChangeHeaderConstraint = Notification.Name("ChangeHeaderConstraint")
}

class HomeViewController : UIViewController {
    
    @IBOutlet weak var contentCollectionView: UICollectionView!
    
    var contentPageViewController : UIPageViewController?
    
    var vcArray = [UIViewController]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let homeVC  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BestViewController") as? BestViewController {
            
            self.vcArray.append(homeVC)
            
            self.contentPageViewController?.setViewControllers(self.vcArray,
                                                               direction: .forward,
                                                               animated: false,
                                                               completion: { (finished) in
                                                                
            })
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let id = segue.identifier else {
            
            return
        }
        
        switch id {
            
        case "EmbedPageViewController":
            
            print("EmbedPageViewController")
            
            if let vc = segue.destination as? UIPageViewController {
                
                vc.delegate = self
                vc.dataSource = self
                self.contentPageViewController = vc
            }
            
        default: break
            
        }
    }
}


// 페이지뷰 컨트롤러 델리게이트
extension HomeViewController : UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return nil
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return nil
    }
}

