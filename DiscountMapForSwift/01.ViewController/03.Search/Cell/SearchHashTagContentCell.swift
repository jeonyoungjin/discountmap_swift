//
//  File.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 25..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

class SearchHashTagContentCell : UICollectionViewCell {
    
    @IBOutlet weak var bottomSeparatorView: UIView!
    @IBOutlet weak var leftSeparatorView: UIView!
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var bottomLeftConstraint: NSLayoutConstraint!
}
