//
//  SearchHashTagCell.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 25..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let SearchHashTagCellChangeHeight = Notification.Name("SearchHashTagCellChangeHeight")
    static let SearchHashTagCellSelectItem = Notification.Name("SearchHashTagCellSelectItem")
}

class SearchHashTagCell : UITableViewCell {
    
    var dataArray = DataManager.getRecommendData()
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottomLeftConstraint: NSLayoutConstraint!
    
    deinit {
        print("deinit SearchHashTagCell!!!")
    }
    
    static var test = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.setTagData()
    }
    
    func setTagData() {
        
        self.collectionView.reloadData()
        
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: NSNotification.Name.SearchHashTagCellChangeHeight, object: self.collectionView.contentSize.height)
        }
    }
}


extension SearchHashTagCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let count = self.dataArray?.keywords?.count {
            if count % 2 == 0 {
                return count
            }
            
            return count + 1
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchHashTagContentCell", for: indexPath) as! SearchHashTagContentCell
        
        if let count = self.dataArray?.keywords?.count, count > indexPath.row {
            if let keyword = self.dataArray?.keywords?[indexPath.row] {
                cell.title.text = keyword.keyword
            }
        } else {
            cell.title.text = ""
        }
        
        cell.leftSeparatorView.isHidden = indexPath.row % 2 == 0 ? true : false
        cell.bottomLeftConstraint.constant = indexPath.row % 2 == 0 ? 0 : -1
        cell.bottomSeparatorView.isHidden = false

        
        if let count = self.dataArray?.keywords?.count {
            
            // 홀수일 경우 빈 셀 1개 추가
            let realCount = count + (count % 2 == 0 ? 0 : 1)
            
            if indexPath.row >= realCount - 2 {
                cell.bottomSeparatorView.isHidden = true
            }
        }
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: (UIScreen.main.bounds.width - 32 - 1) / CGFloat(2), height: 44)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if let cell = collectionView.cellForItem(at: indexPath) as? SearchHashTagContentCell {
            
            if cell.title.text == "" {
                return
            }
            
            TrackingManager.getMetaData().isHotPlace = true
            TrackingManager.E_1710120009(searchString: cell.title.text!)

        }
        
        NotificationCenter.default.post(name: Notification.Name.SearchHashTagCellSelectItem, object: indexPath)
    }

}
