//
//  SearchResultZeroCell.swift
//  DiscountMapForSwift
//
//  Created by Young jin Jeon on 2017. 10. 29..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import UIKit

class EmptySearchResultView: UICollectionReusableView {

    @IBOutlet weak var infoText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setInfoText(searchText: String) {
        
        let attributeString = NSMutableAttributedString.init()
        attributeString.append(NSAttributedString.init(string: "'\(searchText)'",
                                                       attributes: [NSForegroundColorAttributeName: UIColor.init(red: 255/255, green: 73/255, blue: 73/255, alpha: 1),
                                                                    NSFontAttributeName : UIFont(name: "AppleSDGothicNeo-Regular", size: 16)!]))
        
        attributeString.append(NSAttributedString.init(string: "에 대한 검색결과가 없습니다.",
            attributes: [NSForegroundColorAttributeName: UIColor.init(red: 102/255, green: 102/255, blue: 102/255, alpha: 1),
                         NSFontAttributeName : UIFont(name: "AppleSDGothicNeo-Regular", size: 16)!]))

        self.infoText.attributedText = attributeString
    }

}
