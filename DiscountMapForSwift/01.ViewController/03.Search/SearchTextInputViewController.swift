//
//  SearchTextInputViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 10. 17..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import UIKit

class SearchTextInputViewController: UIViewController {
    
    @IBOutlet weak var contentTableView: UITableView!
    
    var hashTagCellHeight: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(notification:)),
                                               name: Notification.Name.SearchHashTagCellChangeHeight,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(notification:)),
                                               name: Notification.Name.SearchHashTagCellSelectItem,
                                               object: nil)

    }
    
    deinit {
        print("deinit SearchTextInputViewController!!!")
    }

    func handle(notification: Notification) {
        
        switch notification.name {
        case Notification.Name.SearchHashTagCellChangeHeight:
            
            if self.hashTagCellHeight == 0 {
                self.hashTagCellHeight = notification.object as! CGFloat
                //            UIView.performWithoutAnimation {
                
                self.contentTableView.reloadSections(IndexSet.init(integer: 0), with: .none)
                //            }
            }
            
        case Notification.Name.SearchHashTagCellSelectItem:
            let indexPath = notification.object as! IndexPath

            if let keyword = DataManager.getRecommendData()?.keywords?[indexPath.row] {
                
                LocationManager.setManualLocation(location: CLLocation.init(latitude: (keyword.latitude?.doubleValue)!, longitude: (keyword.longitude?.doubleValue)!), forceAddressName: keyword.keyword, completion: nil)
            
                let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AllDealResultViewController") as! AllDealResultViewController
                let categoryData = CategoryMetaData()
                categoryData.requestUrl = keyword.link?.href
                categoryData.selectFilterCategory = FilterCategoryData_children(dict: ["name" :"모든상품"])
                vc.categoryMetaData = categoryData
                
                if let type = keyword.link?.dataType, type == "html" {
                    vc.showtype = .html
                    
                } else {
                    vc.showtype = .json
                }
                
                self.navigationController?.navigationController?.pushViewController(vc, animated: true)
            }
        default:
            break
        }
    }
    
}

extension SearchTextInputViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        } else {
            return DataManager.getHotPlaceList().count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "SearchHashTagCell")!
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchContentCell") as! SearchContentCell
        cell.title.text = DataManager.getHotPlaceList()[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            return 50
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return self.hashTagCellHeight
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            
            let header = Bundle.main.loadNibNamed("SearchViewSectionHeader", owner: nil, options: nil)![0] as! SearchViewSectionHeader
            return header
        }
        
        //        let request = MKLocalSearchRequest()
        //        request.naturalLanguageQuery = searchBarText
        //        request.region = mapView.region
        //        let search = MKLocalSearch(request: request)
        //
        //        search.startWithCompletionHandler { response, _ in
        //            guard let response = response else {
        //                return
        //            }
        //            self.matchingItems = response.mapItems
        //            self.tableView.reloadData()
        //        }
        
        
        return nil
    }
}
