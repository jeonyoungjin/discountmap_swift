//
//  SearchViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 13..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit

class TintTextField: UITextField {
    
    var tintedClearImage: UIImage? = UIImage.init(named: "iconSearchDelete")
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        tintClearImage()
    }
    
    private func tintClearImage() {
        for view in subviews {
            if view is UIButton {
                let button = view as! UIButton
                if let _ = button.image(for: .highlighted) {
                    button.setImage(tintedClearImage, for: .normal)
                    button.setImage(tintedClearImage, for: .highlighted)
                }
            }
        }
    }
}

class SearchViewController : UIViewController {
    
    var prevStatusbarStyle: UIStatusBarStyle?
    
    struct Temp {
        static var prevSearchText = ""
    }
    
    @IBOutlet weak var showMapButton: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    var innerNavigationController: UINavigationController?
    var dimdView : UIView?
    var emptySearchResultView: EmptySearchResultView?
    
    deinit {
        print("deinit SearchViewController!!!")
        self.innerNavigationController?.popToRootViewController(animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var attributes = [
            NSForegroundColorAttributeName: UIColor.init(red: 149/255, green: 149/255, blue: 149/255, alpha: 1),
            NSFontAttributeName : UIFont(name: "AppleSDGothicNeo-Regular", size: 12)! // Note the !
        ]
        searchTextField.attributedPlaceholder = NSAttributedString(string: "지역명 + 키워드로 검색해보세요.", attributes:attributes)
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let button = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 75, height: keyboardToolbar.frame.size.height))
        button.titleLabel?.textAlignment = .right
        attributes = [
            NSForegroundColorAttributeName: UIColor.init(red: 38/255, green: 38/255, blue: 38/255, alpha: 1),
            NSFontAttributeName : UIFont(name: "AppleSDGothicNeo-Regular", size: 14)! // Note the !
        ]
        button.setAttributedTitle(NSAttributedString(string: " 검색 취소", attributes:attributes), for: .normal)
        button.addTarget(searchTextField, action: #selector(resignFirstResponder), for: .touchUpInside)
        
        let addButton = UIBarButtonItem.init(customView: button)
        keyboardToolbar.setItems([flexibleSpace,addButton], animated: false)
        keyboardToolbar.backgroundColor = UIColor.white
        searchTextField.inputAccessoryView = keyboardToolbar
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: .UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillhide),
                                               name: .UIKeyboardWillHide,
                                               object: nil)
        
        self.dimdView = UIView.init(frame: UIScreen.main.bounds)
        dimdView?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(onTapDimdView))
        gesture.numberOfTapsRequired = 1
        dimdView?.addGestureRecognizer(gesture)
                
        self.emptySearchResultView = Bundle.main.loadNibNamed("EmptySearchResultView", owner: nil, options: nil)?.first as? EmptySearchResultView
        self.emptySearchResultView?.frame = CGRect.init( x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 50 - 44 - 20)
        
        TrackingManager.V_1710120009()
    }
    
    func onTapDimdView() {
        
        self.keyboardWillhide()
        self.searchTextField.resignFirstResponder()
    }
    
    func keyboardWillShow() {
        
        if let dealVC = self.innerNavigationController?.viewControllers.last as? DealViewController {
            
            self.dimdView?.alpha = 0
            dealVC.view.addSubview(self.dimdView!)
            
            UIView.animate(withDuration: 0.15, animations: {
                self.dimdView?.alpha = 1
            })
        }
    }
    
    func keyboardWillhide() {
        
        if let _ = self.innerNavigationController?.viewControllers.last as? DealViewController {
            self.dimdView?.alpha = 1
            UIView.animate(withDuration: 0.15, animations: {
                self.dimdView?.alpha = 0
            }, completion: { (finished) in
                self.dimdView?.removeFromSuperview()
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.prevStatusbarStyle = UIApplication.shared.statusBarStyle
    }
    
    @IBAction func onClickSearchField(_ sender: UITextField) {
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let _ = self.innerNavigationController?.viewControllers.last as? HomeChildViewControllerProtocol {
            
        } else {
            
            self.searchTextField.becomeFirstResponder()
        }
        
        TrackingManager.getMetaData().isSearchResultView = true
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.searchTextField.resignFirstResponder()
        TrackingManager.getMetaData().isSearchResultView = false
        TrackingManager.getMetaData().isClickTopSearch = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let id = segue.identifier else { return }
        
        switch id {
            
        case "InnerNavigationController":
            if let vc = segue.destination as? UINavigationController {
                
                self.innerNavigationController = vc
            }
            
        default: break
        }
    }
    
    @IBAction func onClickBackButton(_ sender: Any) {

        
        Temp.prevSearchText = ""
        
        if let _ = self.innerNavigationController {
            
            if self.innerNavigationController?.visibleViewController is DealViewController {
                
                self.innerNavigationController?.popViewController(animated: true)
                self.searchTextField.becomeFirstResponder()
                self.showMapButton.isHidden = true
            } else {
                
                NotificationCenter.default.post(name: Notification.Name.RequestShowHomeView, object: nil)
            }
        }
    }
    @IBAction func onClickShowMapButton(_ sender: Any) {
        
        if let dealVC = self.innerNavigationController?.viewControllers.last as? HomeChildViewControllerProtocol, dealVC is DealViewController  {
            
            let metaData = MapViewMetaData.init(showType: .showFilterMode,
                                                currentCategoryMetaData: dealVC.currentCategoryMetaData,
                                                categoryMetaData: dealVC.categoryMetaData, gnbData: dealVC.gnbData)
            
            NotificationCenter.default.post(name: Notification.Name.RequestShowMapView, object: metaData)
        }
    }
    
    @IBAction func textFieldSearch(_ sender: Any) {
        
        print("textFieldSearch")
        
        self.onClickSearchButton(sender)
    }
    
    @IBAction func onClickSearchButton(_ sender: Any) {
        
        guard self.searchTextField.text != "" else {
            return
        }
        // 서치컨트롤러는 고유하다는 가정
        guard self.searchTextField.text != Temp.prevSearchText else {
            return
        }
        
        TrackingManager.getMetaData().searchString = self.searchTextField.text!
        TrackingManager.E_1710120009(searchString: self.searchTextField.text!)
        TrackingManager.C_1710120001(searchString: self.searchTextField.text!)
        TrackingManager.E_171128004()
        
        
        self.searchTextField.resignFirstResponder()
        
        Temp.prevSearchText = searchTextField.text!
        
        let requestUrl = "\(SERVER_HOST)/search?keyword=\(self.searchTextField.text!)"
        let categoryMetaData = CategoryMetaData()
        let location = (LocationManager.autoLocation()?.coordinate)!
        
        NetworkManager.getDealList(param: ["requestURL": requestUrl,
                                           "lon": location.longitude,
                                           "lat": location.latitude,
                                           "c": categoryMetaData.requestParameter,
                                           "page": 1],
                                   uniqueList: categoryMetaData.uniqueSet,
                                   addCategory: false) { [weak self] (result, error) in
                                    
                                    let dealList = result[0] as? [CategoryDealData]
                                    categoryMetaData.dealArray.append(contentsOf: dealList!)
                                    categoryMetaData.uniqueSet = NSOrderedSet.init(array: (categoryMetaData.dealArray.filter{$0.dealID?.intValue != 0}.map{$0.dealID!}))
                                    categoryMetaData.metaData = result[1] as? DealMetaData
                                    categoryMetaData.titlePropertyName = result[3] as? String
                                    categoryMetaData.canRequestData = true
                                    categoryMetaData.bannerArray = result[4] as? [BannerData]
                                    
                                    if error != nil || dealList?.count == 0 || categoryMetaData.uniqueSet.count == 0 {
                                        categoryMetaData.isEmptyResult = true
                                    }

                                    
                                    if categoryMetaData.isEmptyResult == false {
                                        let attributeString = NSMutableAttributedString.init()
                                        attributeString.append(NSAttributedString.init(string: self?.searchTextField.text ?? "",
                                                                                       attributes: [NSForegroundColorAttributeName: UIColor.init(red: 2/255, green: 2/255, blue: 2/255, alpha: 1),
                                                                                                    NSFontAttributeName : UIFont(name: "AppleSDGothicNeo-Medium", size: 14)!]))
                                        
                                        attributeString.append(NSAttributedString.init(string:" 검색결과",
                                                                                       attributes: [NSForegroundColorAttributeName: UIColor.init(red: 89/255, green: 89/255, blue: 89/255, alpha: 1),
                                                                                                    NSFontAttributeName : UIFont(name: "AppleSDGothicNeo-Regular", size: 14)!]))
                                        
                                        categoryMetaData.attributeSectionTitle = attributeString
                                        
                                    } else {
                                        
                                        self?.emptySearchResultView?.setInfoText(searchText: self?.searchTextField.text ?? "")
                                    }
                                    
                                    if let test = result[6] as? NSNumber {
                                        categoryMetaData.useMap = test
                                    }
                                    
                                    categoryMetaData.useRanking = result[7] as? NSNumber
                                    categoryMetaData.listColumn  = result[8] as? NSNumber
                                    categoryMetaData.requestUrl = requestUrl
                                    
                                    self?.showMapButton.isHidden = !(categoryMetaData.useMap?.boolValue)!
                                    
                                    // 이미 있다면 리프래시
                                    if let dealVC =  self?.innerNavigationController?.viewControllers.last as? DealViewController {
                                                                                
                                        dealVC.currentCategoryMetaData = categoryMetaData
                                        dealVC.changeSubCategory(index: categoryMetaData.categoryIndex)
                                        //                                                        dealVC.contentCollectionView.contentOffset = CGPoint.zero
                                        dealVC.contentCollectionView.setContentOffset(CGPoint.zero, animated: true)
                                        
                                        
                                    } else { // 없다면 생성
                                        
                                        let dealVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DealViewController") as! DealViewController
                                        dealVC.currentCategoryMetaData = categoryMetaData
                                        dealVC.noticeEmptyDealListView = self?.emptySearchResultView
                                        self?.innerNavigationController?.pushViewController(dealVC, animated: true)
                                        
                                        dealVC.view.layoutIfNeeded()
                                    }
                                    
                                    // 트래킹
                                    TrackingManager.V_1710120010()
                                    

        }
    }
}
