//
//  LoginViewController.swift
//  DiscountMapForSwift
//
//  Created by Young jin Jeon on 2017. 10. 23..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let UserLoginSuccess  = Notification.Name("UserLoginSuccess")
}

class LoginViewController: UIViewController {

    @IBOutlet weak var loginWebView: UIWebView!
    
    var urlString: String?
    var isPolicyView: Bool = false
    
    @IBAction func onClickBackButton(_ sender: Any) {
        
        // 강제로 끄면 쿠키 지운다
        URLCache.shared.removeAllCachedResponses()
        let cookies = HTTPCookieStorage.shared.cookies
        for cookie in cookies! {
            if cookie.domain.contains("wemakeprice") {
                HTTPCookieStorage.shared.deleteCookie(cookie)
                UserDefaults.standard.synchronize()
            }
        }
     
        if self.navigationController != nil {
            
            self.navigationController?.popViewController(animated: true)
        } else {
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    deinit {
        print("LoginViewController deinit")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let url = self.urlString {
            
            self.loginWebView.loadRequest(URLRequest.init(url: URL.init(string: url)!))
        } else {
            print(" -{---------> loadRequest fail")
        }
        
        LoadingViewManager.show()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func getKeyVals(urlString: String?) -> Dictionary<String, String>? {
        var results = [String:String]()
        let keyValues = urlString?.components(separatedBy: "&")
        
        if (keyValues?.count)! > 0 {
            for pair in keyValues! {
                let kv = pair.components(separatedBy: "=")
                if kv.count > 1 {
                    results.updateValue(kv[1], forKey: kv[0])
                }
            }
        }
        return results
    }
}

extension LoginViewController : UIWebViewDelegate {

    func webViewDidFinishLoad(_ webView: UIWebView) {
        LoadingViewManager.close()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        LoadingViewManager.close()
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {

        if request.url?.absoluteString.contains("signup/agree") == true {
            self.loginWebView.scrollView.isScrollEnabled = false
        }
        
        if request.url?.absoluteString.contains("/signin/complete") == true {

            let param = self.getKeyVals(urlString: request.url?.absoluteString.components(separatedBy: "?").last)

            // 액세스토큰 저장
            if let token = param?["token"] {
                UserPreferenceManager.setAccessToken(token: token)
                // 마이페이지 갱신
                NetworkManager.getMyData(completion: { (result, error) in

                    DataManager.setMyData(data: result[0] as? MyData)

                    if let rurl = param?["rurl"] {
                        let url = rurl.padding(toLength: ((rurl.characters.count+3)/4)*4,
                                               withPad: "=",
                                               startingAt: 0)
                        if let decryptUrl = url.base64Decoded() {
                            NotificationCenter.default.post(name: Notification.Name.UserLoginSuccess, object: decryptUrl)
                        }
                    }
                    
                    NotificationCenter.default.post(name: Notification.Name.UserLoginSuccess, object: nil)

                    DispatchQueue.main.async {
                        if self.navigationController != nil {
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                })
            }

            return false
        }
        
        if request.url?.absoluteString.hasSuffix("policy/location") == true {
            
            if isPolicyView == true {
                return true
                
            } else {
                if let loginVC  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
                    
                    loginVC.isPolicyView = true
                    loginVC.urlString = (request.url?.absoluteString)!
                    self.present(loginVC, animated: true, completion: nil)
                    
                    return false
                }
            }
        }

        return true
    }
}


