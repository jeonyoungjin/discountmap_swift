//
//  SplashViewController.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 8. 8..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

enum SplashAnimationType : Int {
    
    case none           = 0
    case scaleUp        = 1
    case scaleDown      = 2
    case slideLeft      = 3
    case slideRight     = 4
}


class SplashViewController : UIViewController {
    
    @IBOutlet weak var splashImageView: UIImageView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var logoBottomImageView: UIImageView!
    @IBOutlet weak var splashLeftConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        UIApplication.shared.setStatusBarStyle(.default, animated: false)
        
        NetworkManager.initialzeAppData { (result, error) in
            
            TrackingManager.sendView(trackingId: "V-170301001", value: "Intro")
            guard error == nil else {
                self.showDefaultSplash()
                return
            }
            
            NetworkManager.getMyData { (result, error) in
                if error == nil {
                    DataManager.setMyData(data: result[0] as? MyData)
                }
            }
            
            DataManager.setGNBList(list: result[0] as! [GNBData])
            DataManager.setHotPlaceList(list: result[1] as! [HotPlaceData])
            DataManager.setMarkerList(list: result[2] as! [MarkerData])
            DataManager.setFilterCategory(list: result[3] as! [FilterCategoryData])
            DataManager.setRecommendData(list: result[5] as? RecommendData)
            
            let introDic = result[4] as! [String : Any]
            
            if let _ = error {
                
                self.showDefaultSplash()
            
            } else {
                
                self.prepareSplash(info: introDic, complete: { (animType) in
                    
                    var fixImageWidth: CGFloat = 0
                    
                    if self.splashImageView.image != nil {
                        
                        let ratio = self.view.bounds.size.height / (self.splashImageView.image?.size.height)!
                        fixImageWidth = self.splashImageView.image!.size.width * ratio - UIScreen.main.bounds.size.width
                    }
                    
                    switch animType {
                    case .scaleUp:
                        self.splashImageView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        
                    case .scaleDown:
                        self.splashImageView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        
                    case .slideLeft:
                        self.splashLeftConstraint.constant = fixImageWidth/2
                        
                    case .slideRight:
                        self.splashLeftConstraint.constant = -(fixImageWidth/2)
                        
                    default: break
                    }
                    
                    
                    //GPS 초기화
                    LocationManager.initLocationWithResponse(delayUntilAuthrized: true, response:{ (isSuccess) in
                        
                        TrackingManager.E_1710120001()
                        TrackingManager.E_1710120002()
                        TrackingManager.E_1710120003()
                        
                        if isSuccess == true {
                            TrackingManager.A_171128001()
                        }
                        

                        print("**** LocationManager.initLocationWithResponse")
                        
                        UIView.animate(withDuration: 3,
                                       delay: 0.3,
                                       options: .curveEaseInOut,
                                       animations: {
                                        
                                        switch animType {
                                        case .scaleUp:
                                            self.splashImageView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                            
                                        case .scaleDown:
                                            self.splashImageView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                            
                                        case .slideLeft:
                                            self.splashLeftConstraint.constant = fixImageWidth/2
                                            
                                        case .slideRight:
                                            self.splashLeftConstraint.constant = -(fixImageWidth/2)
                                            
                                        default: break
                                        }
                                        
//                                        self.view.layoutIfNeeded()
                                        
                        }, completion: { (finished) in
                            
//                            if finished == true {
                            
                                print("if finished == true {")
                                
                                self.showHomeViewController()
//                            }
                        })
                    })
                })
            }
        }
    }
    
    
    func showDefaultSplash() {
        
        self.view.backgroundColor = UIColor.init(colorLiteralRed: 241/255, green: 64/255, blue: 70/255, alpha: 1)
        self.logoImageView.image = UIImage.init(named: "iconSplashLogo")
        self.logoBottomImageView.image = UIImage.init(named: "iconSplashWmp")
    }
    
    func prepareSplash(info: [String:Any], complete:@escaping (SplashAnimationType) -> ()) {
        
        let introData = IntroData.init(dict: info)
        var backgroundURL = introData.background?.imageUrl //introData.background?["imageUrl"] as? String
        var logoURL = introData.logo?.imageUrl //introData.logo?["imageUrl"] as? String
        var logoBottomURL = introData.logoBottom?.imageUrl //introData.logoBottom?["imageUrl"] as? String
        
        SDWebImageManager.shared().cachedImageExists(for: URL.init(string: backgroundURL!)) { (isExist) in
            
            if isExist {
                
                self.splashImageView.sd_setImage(with: URL.init(string: backgroundURL!), completed: { (image, error, cacheType, url) in
                    
                    self.logoImageView.sd_setImage(with: URL.init(string: logoURL!))
                    self.logoBottomImageView.sd_setImage(with: URL.init(string: logoBottomURL!))
                    
                    if let _ = error {
                        complete(.none)
                    } else {
                        complete(SplashAnimationType(rawValue: ((introData.background?.effect)!).intValue)!)
                    }
                })
                
              
                // 첫실행이거나 이미지가 변경된 경우                
            } else {
                
                SDWebImagePrefetcher.shared().prefetchURLs([URL.init(string: backgroundURL!)!, URL.init(string: logoURL!)!, URL.init(string: logoBottomURL!)!])
                
                if let prevInfo = UserPreferenceManager.getSplashImageInfo() {
                   
                    let prevData    = IntroData.init(dict: prevInfo)
                    backgroundURL   = prevData.background?.imageUrl
                    logoURL         = prevData.logo?.imageUrl
                    logoBottomURL   = prevData.logoBottom?.imageUrl

                    self.splashImageView.sd_setImage(with: URL.init(string: backgroundURL!), completed: { (image, error, cacheType, url) in
                        
                        self.logoImageView.sd_setImage(with: URL.init(string: logoURL!))
                        self.logoBottomImageView.sd_setImage(with: URL.init(string: logoBottomURL!))
                        
                        if let _ = error {
                            complete(.none)
                        } else {
                            complete(SplashAnimationType(rawValue: ((introData.background?.effect)!).intValue)!)
                        }
                    })
                } else {

                    self.showDefaultSplash()
                    complete(.none)
                }
            }
        }
    }
    
    func showHomeViewController() {
        self.performSegue(withIdentifier: "ShowMainViewController", sender: nil)
    }
}
