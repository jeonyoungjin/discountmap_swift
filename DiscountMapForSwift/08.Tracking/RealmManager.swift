////
////  RealmManager.swift
////  DiscountMapForSwift
////
////  Created by we on 2018. 1. 7..
////  Copyright © 2018년 Young jin Jeon. All rights reserved.
////
//
//import Foundation
//import RealmSwift
//
//protocol CuppingRealmCommonModel {
//    var timeinterval:TimeInterval{ get }
//}
//
//class CuppingRealmCommon : Object, CuppingRealmCommonModel {
//    
//    @objc dynamic var timeinterval = Date().timeIntervalSince1970
//}
//
//// 최근 방문 지역
//class LatestLocation : CuppingRealmCommon {
//    
//    @objc dynamic var locationName = ""
//    
//    override static func primaryKey() -> String? {
//        return "locationName"
//    }
//}
//
//
//// 최근 딜클릭 위치
//class LatestDealClickLocation : CuppingRealmCommon {
//    
//    @objc dynamic var infoString = ""
//}
//
//
//// 최근 주문 메뉴
//class LatestOrderMenu : CuppingRealmCommon {
//    
//    @objc dynamic var productId = ""
//    @objc dynamic var productName = ""
//    
//    var productInfo:String {
//        get {
//            return productId + "_" + productName
//        }
//    }
//}
//
//
//final class RealmManager {
//    
//    let realm = try! Realm()
//    
//    private init(){
//        
//        // 마이그레이션 코드
//        //        let syncServerURL = URL(string: "realm://localhost:9080/Dogs")!
//        //        // schemaVersion이나 migrationBlock을 설정할 필요가 없습니다
//        //        let config = Realm.Configuration(syncConfiguration: SyncConfiguration(user: user, realmURL: syncServerURL))
//        //
//        //        let realm = try! Realm(configuration: config)
//    }
//    
//    static let shared = RealmManager.init()
//    
//    // 마지막 딜클릭 위치 저장
//    static let addLastDealClickLocationName:(String)->() = { value in
//        
//        let data = LatestDealClickLocation.init(value: ["infoString":value])
//        var locationArray:Results<LatestDealClickLocation>? = nil
//        
//        try! RealmManager.shared.realm.write {
//            RealmManager.shared.realm.add(data)
//        }
//    }
//    
//    // 마지막 딜클릭 위치 최근 10개 반환
//    static let getLastDealClickLocationName:()->([String]) = {
//        
//        var lastDealClickArray:Results<LatestDealClickLocation>? = nil
//        lastDealClickArray = RealmManager.shared.realm.objects(LatestDealClickLocation.self).sorted(byKeyPath: "timeinterval", ascending:false)
//        
//        try! RealmManager.shared.realm.write {
//            for item in lastDealClickArray! {
//                if (lastDealClickArray?.count)! > 10 {
//                    RealmManager.shared.realm.delete((lastDealClickArray?.last)!)
//                }
//            }
//        }
//        
//        return Array.init(lastDealClickArray!).map{$0.infoString}
//    }
//    
//    // 마지막 방문지역을 저장한다.
//    static let addLocationName:(String)->() = { value in
//        
//        let data = LatestLocation.init(value: ["locationName":value])
//        var locationArray:Results<LatestLocation>? = nil
//        
//        try! RealmManager.shared.realm.write {
//            RealmManager.shared.realm.add(data, update: true)
//        }
//    }
//    
//    
//    // 마지막 방문지역을 저장과 동시에 최근 3개를 리턴한다.
//    static let getLocationName:()->([String]) = {
//        
//        var locationArray:Results<LatestLocation>? = nil
//        locationArray = RealmManager.shared.realm.objects(LatestLocation.self).sorted(byKeyPath: "timeinterval", ascending:false)
//        
//        try! RealmManager.shared.realm.write {
//            for item in locationArray! {
//                if (locationArray?.count)! > 10 {
//                    RealmManager.shared.realm.delete((locationArray?.last)!)
//                }
//            }
//        }
//        
//        return Array.init(locationArray!).map{$0.locationName}
//    }
//    
//    
//    // 마지막 주문 저장과 동시에 최근 5개를 리턴한다.
//    static let addOrderMenu:(String, String)->() = { productId, productName in
//        
//        let data = LatestOrderMenu.init(value: ["productId":productId, "productName":productName])
//        var orderArray:Results<LatestOrderMenu>? = nil
//        
//        try! RealmManager.shared.realm.write {
//            RealmManager.shared.realm.add(data)
//        }
//    }
//    
//    static let getOrderMenu:()->([String]) = {
//        
//        var orderArray:Results<LatestOrderMenu>? = nil
//        
//        orderArray = RealmManager.shared.realm.objects(LatestOrderMenu.self).sorted(byKeyPath: "timeinterval", ascending:false)
//        
//        try! RealmManager.shared.realm.write {
//            for item in orderArray! {
//                if (orderArray?.count)! > 10 {
//                    RealmManager.shared.realm.delete((orderArray?.last)!)
//                }
//            }
//        }
//        
//        return Array.init(orderArray!).map{$0.productInfo}
//    }
//}
//
//
