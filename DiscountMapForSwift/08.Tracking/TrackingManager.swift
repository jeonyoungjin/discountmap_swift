//
//  TrackingManager.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 20..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import UIKit
import Appboy_iOS_SDK


fileprivate let GA_KEY = "UA-93160681-1"

enum TrackingEventCategory : String {
    case status         = "Status"
    case buttonClick    = "Button Click"
    case dealClick      = "Deal Click"
    case bannerClick    = "Banner Click"
    case dealImpr       = "Deal Impr"
    case scroll         = "Scroll"
    case error          = "Error"
    case search         = "Search"
}

class TrackingMetaData {
    
    var hitParameterDic :[String:String]?
    var listIndex = 0
    var isHotPlace    = false
    var isTopMapClick = false
    var isOrderClick  = false
    var isShowMapView = false
    var isSearchResultView = false
    var isClickTopSearch = false
    var searchString     = ""
    var currentService   = ""
    var gnbName          = ""
    var subCategoryName  = ""
    var currentLinkURL   = ""
    var searchFunnels    = ""
    var bannerURL        = ""
    var appboyInflowRoute = ""
}

class TrackingManager {
    
    static let shared = TrackingManager()
    private init(){
        
        self.logView = UITextView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height/2))
        self.logView.isEditable = false
        self.logView.alpha = 0.5
        self.logView.text = ""
        self.logView.isExclusiveTouch = false
        self.logView.isUserInteractionEnabled = false
    }
    
    var metaData: TrackingMetaData = TrackingMetaData()
    var logView: UITextView!
    
    class func getMetaData() -> TrackingMetaData {
        return shared.metaData
    }
    
    
    class func sendView(trackingId: String, value: String?){
        
        let tracker = GAI.sharedInstance().tracker(withTrackingId: GA_KEY)
        tracker?.set(kGAIDescription, value: value)
        
        tracker?.send(GAIDictionaryBuilder.createScreenView().build() as! [AnyHashable : Any]!)
        
        showTrackingTitle(title: trackingId, message: value ?? "")
    }
    
    class func sendEvent(trackingId: String, category: TrackingEventCategory, action: String?, label: String?, value: NSNumber?) {
        
        let tracker = GAI.sharedInstance().tracker(withTrackingId: GA_KEY)
        
        let builder = GAIDictionaryBuilder.createEvent(withCategory: category.rawValue, action: action, label: label, value: value)
        tracker?.send(builder?.build() as! [AnyHashable : Any])
        
        showTrackingTitle(title: trackingId, message: "[\(category.rawValue  )] [ACTION : \(action ?? "")] [LABEL : \(label ?? "")] [VALUE : \(value?.stringValue ?? "")]")
    }
    
    class func showTrackingTitle(title: String, message: String) {
        #if DEBUG
//            TrackingManager.sendLoger(message: "[\(title)] \(message)")
        #endif
    }
    
    class func sendLoger(message: String) {
        
        UIApplication.shared.keyWindow?.addSubview(shared.logView)
        shared.logView.text = "\(shared.logView.text!) \n \(message)"
        
        if shared.logView.text.count > 0 {
            
            let bottomRange = NSMakeRange(shared.logView.text.count - 1, 1)
            
            shared.logView.scrollRangeToVisible(bottomRange)
        }
    }
    
    class func getTrackingLocationName() -> String {
        
        return LocationManager.isExistManualLocation() == true ? LocationManager.shared.manualAddress!  : "내주변"
    }
    
    class func getGPSStatusString() -> String {
        
        return LocationManager.getIsCanReadGPSStatus() == true ? "ON" : "OFF"
    }
    
    class func getAddressString() -> String {
        
        if LocationManager.autoLocation() == nil {
            return "GPS OFF"
        }
        
        return LocationManager.autoAddress() ?? ""
    }
    
    class func getCurrentListName() -> String {
        
        if TrackingManager.getMetaData().isHotPlace == true, TrackingManager.getMetaData().isShowMapView == true {
            return "Map List"
        }
        
        if TrackingManager.getMetaData().isHotPlace == true {
            return "Hot List"
        } else if TrackingManager.getMetaData().isShowMapView == true {
            return "Map List"
        }
        return "Deal List"
    }
    
    class func sendOrderTrackingWithOrderList(orderList:[GAOrderInfo], paymentInfo: GAPaymentInfo) {
        
        let tracker = GAI.sharedInstance().tracker(withTrackingId: GA_KEY)
        let builder = GAIDictionaryBuilder.createEvent(withCategory: "Ecommerce",
                                                       action: "Purchase", label: nil,
                                                       value: nil)!
        
        for order in orderList {
            
            let product = GAIEcommerceProduct.init()
            product.setId(order.deal_id!)
            product.setName(order.deal_name!)
            product.setCategory(order.category_norm!)
            product.setBrand("WMP")
            product.setCouponCode(order.sale_coupon_info_no)
            
            // 앱보이 트래킹
            TrackingManager.A_171128003(orderMenu:"\(order.deal_id ?? "")_\(order.deal_name ?? "")")
            
            let price = Int(order.price!)!
            if let qty = Int(order.qty!), qty != 0 {
                product.setPrice(NSNumber.init(value: price/qty))
            } else {
                product.setPrice(NSNumber.init(value: price))
            }
            
            builder.add(product)
        }
        
        // 앱보이 전송
        TrackingManager.E_171128001(orderList:orderList)
        
        let action = GAIEcommerceProductAction.init()
        action.setAction(kGAIPAPurchase)
        action.setTransactionId(paymentInfo.payment_id)
        action.setRevenue(NSNumber.init(value: Int(paymentInfo.price_total!)!))
        action.setShipping(NSNumber.init(value: Int(paymentInfo.price_ship!)!))
        action.setCouponCode(paymentInfo.coupon_id)
        
        builder.setProductAction(action)
        
        tracker?.set(kGAIScreenName, value: "transaction")
        tracker?.send(builder.build() as! [AnyHashable : Any])
        
        showTrackingTitle(title: "transaction", message: "payment id \(paymentInfo.payment_id ?? "")")
    }
    
    class func E_170404001(view:String) {
        
        TrackingManager.sendEvent(trackingId: "E-170404001",
                                  category: .error,
                                  action: "Main - List none",
                                  label: "\(LocationManager.autoAddress() ?? "") - iOS - \(Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String ?? "")",
                                  value: nil)
    }
    
    class func E_170810001(label:String) {
        
        TrackingManager.sendEvent(trackingId: "E-170810001",
                                  category: .status,
                                  action: "Payment Info",
                                  label: label,
                                  value: nil)
    }
    
    class func E_1710120001() {
        
        TrackingManager.sendEvent(trackingId: "E-1710120001",
                                  category: .status,
                                  action: "Intro - Map",
                                  label: TrackingManager.getAddressString(),
                                  value: nil)
    }
    
    class func E_1710120002() {
        
        TrackingManager.sendEvent(trackingId: "E-1710120002",
                                  category: .status,
                                  action: "Intro - GPS - \(TrackingManager.getGPSStatusString())",
            label: nil,
            value: nil)
    }
    
    class func E_1710120003() {
        
        var loginStatus = "Logout"
        if let accessToken = UserPreferenceManager.getAccessToken(), accessToken != "" {
            loginStatus = "Login"
        }
        TrackingManager.sendEvent(trackingId: "E-1710120003",
                                  category: .status,
                                  action: "Intro - WMP_ID - \(loginStatus)",
            label: nil,
            value: nil)
    }
    
    class func E_1710120004(deal:CategoryDealData) {
        
        TrackingManager.sendEvent(trackingId: "E-1710120004",
                                  category: .dealClick,
                                  action: "Main - \(TrackingManager.getMetaData().gnbName) - Deal Click",
            label: deal.dealID?.stringValue,
            value: deal.price)
    }
    
    class func E_1710120006() {
        
        TrackingManager.sendEvent(trackingId: "E-1710120006",
                                  category: .buttonClick,
                                  action: "Main - \(TrackingManager.getMetaData().gnbName) - Sub Cate - \(TrackingManager.getMetaData().subCategoryName)",
            label: nil,
            value: nil)
    }
    
    class func E_1710120009(searchString:String) {
        
        TrackingManager.sendEvent(trackingId: "E-1710120009",
                                  category: .search,
                                  action: "Search Term - \(getMetaData().isHotPlace == true ? "Hot Place" : "Text Box")",
                                  label: searchString,
                                  value: nil)
    }
    
    class func E_1710120010(deal:CategoryDealData) {
        
        TrackingManager.sendEvent(trackingId: "E-1710120010",
                                  category: .dealClick,
                                  action: "Search - Deal Click",
                                  label: deal.dealID?.stringValue,
                                  value: deal.price)
    }
    
    
    class func E_1710120011(deal:CategoryDealData) {
        
        TrackingManager.sendEvent(trackingId: "E-1710120011",
                                  category: .dealClick,
                                  action: "All Menu - Deal Click",
                                  label: deal.dealID?.stringValue,
                                  value: deal.price)
    }
    
    // 시점을 잘 잡아야한다 ㅋ
    class func E_1710120012(newServiceString:String) {
        
        TrackingManager.sendEvent(trackingId: "E-1710120012",
                                  category: .buttonClick,
                                  action: "All Menu - Map - Switch",
                                  label: "\(TrackingManager.getMetaData().currentService) - \(newServiceString)",
            value: nil)
    }
    
    class func C_1710120001(searchString:String) {
        
        let tracker = GAI.sharedInstance().tracker(withTrackingId: GA_KEY)
        
        tracker?.set(GAIFields.customDimension(for: 1), value: searchString)
        tracker?.set(kGAIScreenName, value: "C-1710120001")
        tracker?.send(GAIDictionaryBuilder.createScreenView().set(GAIFields.customDimension(for: 1), forKey: "C-1710120001").build() as! [AnyHashable : Any]!)
        
        TrackingManager.showTrackingTitle(title: "C-1710120001", message: "\(searchString) [1]")
    }
    
    class func E_1710120013(index:Int) {
        
        guard TrackingManager.getMetaData().isClickTopSearch == false else {
            return
        }
        
        var currentPage = ""
        switch index {
        case 0:
            currentPage = "Main"
        case 1:
            currentPage = "All Menu"
        case 2:
            currentPage = "Search"
        case 3:
            currentPage = "My Page"
        default:
            break
        }
        
        TrackingManager.sendEvent(trackingId: "E-1710120013",
                                  category: .buttonClick,
                                  action: "NAV Bar - \(currentPage)",
            label: nil,
            value: nil)
    }
    
    class func E_1710120014(action: String) {
        
        TrackingManager.sendEvent(trackingId: "E-1710120014",
                                  category: .buttonClick,
                                  action: "My Page - \(action)",
            label: nil,
            value: nil)
    }
    
    class func E_1710120015(label: String) {
        
        TrackingManager.sendEvent(trackingId: "E-1710120015",
                                  category: .status,
                                  action: "GPS Switch",
                                  label: label,
                                  value: nil)
    }
    
    class func E_171128001(orderList:[GAOrderInfo]) {
    
        for order in orderList {
            let property:[String : Any] = ["Order_Category" : getMetaData().appboyInflowRoute,
                                           "Order_Location" : LocationManager.autoAddress() ?? "",
                                           "Order_Item_Name": order.deal_name ?? ""]
            
            if let price = Int(order.price ?? "1"), let qty = Int(order.qty ?? "1") {

                Appboy.sharedInstance()?.logPurchase("Order",
                                                     inCurrency: "KRW",
                                                     atPrice: NSDecimalNumber.init(value: price/qty),
                                                     withQuantity: UInt.init(qty),
                                                     andProperties: property)
            }
        }
        
        getMetaData().appboyInflowRoute = ""
    }
    
    class func E_171128002() {
        
        Appboy.sharedInstance()?.logCustomEvent("GNB_Deal_Click_\(getMetaData().gnbName)")
    }
    
    class func E_171128003() {
        
        Appboy.sharedInstance()?.logCustomEvent("GNB_Click_\(getMetaData().gnbName)")
    }
    
    class func E_171128004() {
        
        Appboy.sharedInstance()?.logCustomEvent("Search_Click_\(getMetaData().searchString)")
    }
    
    class func E_171128005(service:String) {
        
        Appboy.sharedInstance()?.logCustomEvent("All_Menu_Click_\(service)")
    }
    
    class func E_171128006() {
        Appboy.sharedInstance()?.logCustomEvent("All_Menu_Click_\(getMetaData().bannerURL)")
    }
    
    class func A_171128001() {

        Appboy.sharedInstance()?.user.addToCustomAttributeArray(withKey: "Last_Location", value: LocationManager.autoAddress() ?? "")
    }
    
    class func A_171128002(prefix:String) {

        var str = ""
        if prefix == "All_Menu" {
            str = prefix + "_" + (LocationManager.isExistManualLocation() == true ? (LocationManager.autoAddress() ?? "") : "내주변") + "_" + getMetaData().currentService
        } else if prefix == "Search" {
            str = prefix + "_" + getMetaData().searchString
        } else {
            if getMetaData().subCategoryName == "" {
                str = getMetaData().gnbName
            } else {
                str = getMetaData().gnbName + "_" + getMetaData().subCategoryName
            }
        }
        
//        RealmManager.addLastDealClickLocationName(str)
//        Appboy.sharedInstance()?.user.setCustomAttributeArrayWithKey("Last_Conversion_Menu", array: RealmManager.getLastDealClickLocationName())
        Appboy.sharedInstance()?.user.addToCustomAttributeArray(withKey: "Last_Conversion_Menu", value: str)
        
    }
    
    class func A_171128003(orderMenu:String) {

//        Appboy.sharedInstance()?.user.setCustomAttributeArrayWithKey("Last_Order_Item", array: RealmManager.getOrderMenu())
        Appboy.sharedInstance()?.user.addToCustomAttributeArray(withKey: "Last_Order_Item", value: orderMenu)
    }
    
    
    class func V_170301001() {
        TrackingManager.sendView(trackingId: "V-170301001", value: "Intro")
    }
    
    class func V_170810001() {
        TrackingManager.sendView(trackingId: "V-170810001", value: "Order")
    }
    
    class func V_170810002() {
        TrackingManager.sendView(trackingId: "V-170810002", value: "Payment")
    }
    
    class func V_170810003() {
        TrackingManager.sendView(trackingId: "V-170810003", value: "Transaction")
    }
    
    class func V_1710120001(current:String) {
        TrackingManager.sendView(trackingId: "V-1710120001", value: "Main - \(TrackingManager.getMetaData().gnbName) - \(current)")
    }
    
    class func V_1710120002() {
        
        if getMetaData().subCategoryName == "" {
       
            TrackingManager.sendView(trackingId: "V-1710120002", value: "Main - \(TrackingManager.getMetaData().gnbName) - \(TrackingManager.getCurrentListName()) - Deal Click")
            TrackingManager.getMetaData().appboyInflowRoute = getMetaData().gnbName
        } else {

            TrackingManager.sendView(trackingId: "V-1710120002", value: "Main - \(TrackingManager.getMetaData().gnbName) - \(TrackingManager.getCurrentListName()) - \(TrackingManager.getMetaData().subCategoryName) - Deal Click")
            TrackingManager.getMetaData().appboyInflowRoute = getMetaData().gnbName + " > " + getMetaData().subCategoryName
        }
    }
    
    class func V_1710120003() {
        TrackingManager.sendView(trackingId: "V-1710120003", value: "Main - \(TrackingManager.getMetaData().gnbName) - Banner - \(TrackingManager.getMetaData().bannerURL)")
    }
    
    class func V_1710120009() {
        TrackingManager.sendView(trackingId: "V-1710120009", value: "Search - \(TrackingManager.getMetaData().searchFunnels)")
    }
    
    class func V_1710120010() {
        TrackingManager.sendView(trackingId: "V-1710120010", value: "Search - \(TrackingManager.getMetaData().searchFunnels) - \(TrackingManager.getCurrentListName())")
    }
    
    class func V_1710120011() {
        TrackingManager.sendView(trackingId: "V-1710120011", value: "Search - \(TrackingManager.getMetaData().searchFunnels) - \(TrackingManager.getCurrentListName()) - Deal Click")
        getMetaData().appboyInflowRoute = "검색 >" + getMetaData().searchString
    }
    
    class func V_1710120012() {
        TrackingManager.sendView(trackingId: "V-1710120012", value: "All Menu - \(TrackingManager.getTrackingLocationName())")
    }
    
    class func V_1710120013() {
        TrackingManager.sendView(trackingId: "V-1710120013", value: "All Menu - \(TrackingManager.getTrackingLocationName()) - \(TrackingManager.getMetaData().currentService) - \(TrackingManager.getCurrentListName())")
    }
    
    class func V_1710120014() {
        TrackingManager.sendView(trackingId: "V-1710120014", value: "All Menu - \(TrackingManager.getTrackingLocationName()) - \(TrackingManager.getMetaData().currentService) - \(TrackingManager.getCurrentListName()) - Deal Click")
        getMetaData().appboyInflowRoute = "전체메뉴 > " + getMetaData().currentService
    }
    
    class func V_1710120015() {
        TrackingManager.sendView(trackingId: "V-1710120015", value: "My Page")
    }
}
