//
//  AppInfoManager.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 8. 28..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import SDWebImage

final class AppInfoManager {
    
    static let shared = AppInfoManager()
    
    private init() {
    }
    
    public private(set) var gnbList         : Array<GNBData>?
    public private(set) var hotplaceList    : Array<HotPlaceData>?
    public private(set) var markerList      : Array<MarkerData>?
    public private(set) var filterCategoryList : Array<FilterCategoryData>?
    public private(set) var hashTagList     : Array<FilterTagData>?
    
    func setGNBList(gnbList : Array<GNBData>?) {
        self.gnbList = gnbList
    }
    
    func setHotplaceList(hotplaceList : Array<HotPlaceData>?) {
        self.hotplaceList = hotplaceList
    }
    
    func setMarkerList(markerList : Array<MarkerData>?) {
        self.markerList = markerList
        
        let onImageURLList = markerList?.map({$0.icons?.on})
        SDWebImagePrefetcher.shared().prefetchURLs(onImageURLList as? [URL])

    }
    
    func setFilterCategoryList(filterCategoryList : Array<FilterCategoryData>?) {
        self.filterCategoryList = filterCategoryList
    }
    
    func setHashTagList(hashTagList : Array<FilterTagData>?) {
        self.hashTagList = hashTagList
    }
}
