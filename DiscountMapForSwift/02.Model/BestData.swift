//
//  BestData.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 10. 15..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

class BestData: SuperModel {
    
    var nameField: String?
    var deals: [CategoryDealData]?
    
    override init(dict: Dictionary<String, Any>) {
        super.init(dict: dict)
        
        if let dealList = dict["deals"] as? Array<Any>{
            
            self.deals  = [CategoryDealData]()
            for var data in dealList {
                
                let dealData = CategoryDealData.init(dict: data as! [String:Any])
                self.deals?.append(dealData)
            }
        }
    }
}
