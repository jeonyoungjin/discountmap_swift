//
//  BannerData.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 8. 28..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

final class BannerData_image : SuperModel {
    
}

final class BannerData : SuperModel {

    var image   : Dictionary<String, AnyObject>?
    var linkUrl : String?
    var title   : String?
}
