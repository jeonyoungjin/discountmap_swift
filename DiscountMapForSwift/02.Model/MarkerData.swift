//
//  MarkerData.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 8. 28..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

final class MarkerData_icons : SuperModel {
    
    var on: String?
    var off: String?
}

final class MarkerData : SuperModel {
    
    override init(dict: Dictionary<String, Any>) {
        super.init(dict: dict)
        
        if let data = dict["icons"] as? [String: Any] {
            self.icons = MarkerData_icons.init(dict: data)
        }
    }
    
    var id : NSNumber?
    var icons : MarkerData_icons?
}
