//
//  DealMetaData.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 8. 28..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

final class DealMetaData : SuperModel {
    
    var limit   : NSNumber?
    var page    : NSNumber?
    var totalCount : NSNumber?
}
