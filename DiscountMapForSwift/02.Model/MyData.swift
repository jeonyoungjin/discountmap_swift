//
//  MyData.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 10. 15..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

class MyData_links : SuperModel {
    
    var signin: MyData_signin?
    var signout: MyData_signout?
    var editProfile: MyData_editProfile?
    
    override init(dict: Dictionary<String, Any>) {
        super.init(dict: dict)
        
        if let signin = dict["signin"] as? [String:Any] {
            self.signin = MyData_signin(dict: signin)
        }
        
        if let signout = dict["signout"] as? [String:Any] {
            self.signout = MyData_signout(dict: signout)
        }
        
        if let profile = dict["editProfile"] as? [String:Any] {
            self.editProfile = MyData_editProfile(dict: profile)
        }
    }
}

class MyData_editProfile : SuperModel {
    
    var href: String?
    var rel: String?
    var dataType: String?
}


class MyData_signin : SuperModel {
    
    var href: String?
    var rel: String?
    var dataType: String?
}

class MyData_signout : SuperModel {
    
    var href: String?
    var rel: String?
    var dataType: String?
}

class MyData_link : SuperModel {
    
    var href: String?
    var dataType: String?
}


class MyData_menu : SuperModel {

    var name: String?
    var link: MyData_link?
    
    override init(dict: Dictionary<String, Any>) {
        super.init(dict: dict)
        
        if let data = dict["link"] as? [String: Any]{
            self.link = MyData_link.init(dict: data)
        }
    }
}

class MyData : SuperModel {
    
    var name: String?
    var links: MyData_links?
    var enablePush: NSNumber?
    var menu: [MyData_menu]?
    var id: String?
    var uid: String?

    override init(dict: Dictionary<String, Any>) {
        super.init(dict: dict)
        
        if let menuList = dict["menu"] as? Array<Any>{
            
            self.menu  = [MyData_menu]()
            for var data in menuList {
                
                let menuData = MyData_menu.init(dict: data as! [String:Any])
                self.menu?.append(menuData)
            }
        }
        
        if let data = dict["links"] as? [String: Any]{
            self.links = MyData_links.init(dict: data)
        }
    }
}
