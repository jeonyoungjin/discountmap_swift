//
//  HotPlaceData.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 8. 28..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

final class HotPlaceData_children : SuperModel {
   
    var name : String?
    var latitude : NSNumber?
    var longitude: NSNumber?
}

final class HotPlaceData : SuperModel {
    
    var name: String?
    var children: [HotPlaceData_children]?

    override init(dict: Dictionary<String, Any>) {
        super.init(dict: dict)
    
        if let childList = dict["children"] as? Array<Any>{
            
            self.children  = [HotPlaceData_children]()
            for var data in childList {
                
                let childData = HotPlaceData_children.init(dict: data as! [String:Any])
                self.children?.append(childData)
            }
        }
    }
}
