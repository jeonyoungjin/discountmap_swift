//
//  RecommendData.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 10. 15..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

final class RecommendData_link : SuperModel {
    
    var href: String?
    var rel: String?
    var dataType: String?
}

final class RecommendData_keywords : SuperModel {
    
    var keyword: String?
    var imageUrl: String?
    var link: RecommendData_link?
    var latitude: NSNumber?
    var longitude: NSNumber?
    
    override init(dict: Dictionary<String, Any>) {
        super.init(dict: dict)
        
        if let data = dict["link"] as? [String: Any]{
            self.link = RecommendData_link.init(dict: data)
        }
    }
}

final class RecommendData : SuperModel {
    
    var title: String?
    var keywords: [RecommendData_keywords]?
    
    override init(dict: Dictionary<String, Any>) {
        super.init(dict: dict)
 
        if let childList = dict["keywords"] as? Array<Any>{
            
            self.keywords  = [RecommendData_keywords]()
            for var data in childList {
                
                let keywordData = RecommendData_keywords.init(dict: data as! [String:Any])
                self.keywords?.append(keywordData)
            }
        }
    }
}
