//
//  IntroData.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 19..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

final class IntroData_background : SuperModel {
    var effect: NSNumber?
    var imageUrl: String?
}

final class IntroData_logoBottom : SuperModel {
    var imageUrl: String?
}

final class IntroData_logo : SuperModel {
    var imageUrl: String?
}

final class IntroData : SuperModel {
    
    override init(dict: Dictionary<String, Any>) {
        super.init(dict: dict)
        
        if let data = dict["background"] as? [String: Any]{
            self.background = IntroData_background.init(dict: data)
        }
        
        if let data = dict["logo"] as? [String: Any]{
            self.logo = IntroData_logo.init(dict: data)
        }

        if let data = dict["logoBottom"] as? [String: Any]{
            self.logoBottom = IntroData_logoBottom.init(dict: data)
        }
    }
    
    var background   : IntroData_background?
    var logo         : IntroData_logo?
    var logoBottom   : IntroData_logoBottom?
}
