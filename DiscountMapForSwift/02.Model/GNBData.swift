//
//  GNBData.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 8. 28..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

final class GNBData_link : SuperModel {
    
    var href: String?
    var dataType: String?
}

final class GNBData_children : SuperModel {
    
    var name : String?
    var link : GNBData_link?

    override init(dict: Dictionary<String, Any>) {
        super.init(dict: dict)
        
        if let data = dict["link"] as? [String: Any]{
            self.link = GNBData_link.init(dict: data)
        }
    }
}


final class GNBData : SuperModel {
    
    var link : GNBData_link?
    var name : String?
    var children : [GNBData_children]?
    
    override init(dict: Dictionary<String, Any>) {
        super.init(dict: dict)

        if let childList = dict["children"] as? Array<Any>{

            self.children  = [GNBData_children]()
            for var data in childList {
                
                let childData = GNBData_children.init(dict: data as! [String:Any])
                self.children?.append(childData)
            }
        }
        
        if let data = dict["link"] as? [String: Any]{
            self.link = GNBData_link.init(dict: data)
        }
    }
}
