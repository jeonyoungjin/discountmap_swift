//
//  FilterTagData.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 8. 28..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

final class FilterTagData : SuperModel {
    
    var icons : Dictionary<String, AnyObject>?
    var id : String?
    var name : String?
}
