//
//  SuperModel.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 8. 24..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

class SuperModel : NSObject {
    
    init(dict : Dictionary<String, Any>) {
        super.init()
        
        for (key, value) in dict {
            
            if self.responds(to: NSSelectorFromString(key)) {
            
                if value is NSNull {
                    continue
                }

                if self.value(forKey: key) is NSArray ||
                    self.value(forKey: key) is NSDictionary ||
                    self.value(forKey: key) is SuperModel {

                    self.setValue(nil, forKey: key)
                } else {
                
                    self.setValue(value, forKey: key)
                }
            }
        }
    }    
}
