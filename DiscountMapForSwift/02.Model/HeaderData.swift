//
//  HeaderData.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 10. 18..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import UIKit

class HeaderData: SuperModel {

    var dataType: String?
    var url: String?
    var linkUrl: String?
    var width: NSNumber?
    var height: NSNumber?
}
