//
//  TestModel.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 8. 22..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

class TestModel : SuperModel {
    
    var dealID      : NSNumber?
    var dealName    : String?
    var storeName   : String?
    var price       : NSNumber?
    var priceDisplay: String?
    var priceOrg    : NSNumber?
    var dcRate      : String?
    var imageUrl    : String?
    var longitude   : NSNumber?
    var latitude    : NSNumber?
    var distance    : String?
    var linkUrl     : String?
    var markerID    : NSNumber?
}
