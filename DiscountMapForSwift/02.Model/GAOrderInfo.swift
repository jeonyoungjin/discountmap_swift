//
//  GAOrderInfo.swift
//  DiscountMapForSwift
//
//  Created by Young jin Jeon on 2017. 10. 31..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import UIKit

class GAOrderInfo: SuperModel {

    var order_id: String?
    var deal_id: String?
    var deal_name: String?
    var category_disp: String?
    var category_norm: String?
    var theme_id: String?
    var sale_coupon_info_no: String?
    var price: String?
    var qty: String?
}
