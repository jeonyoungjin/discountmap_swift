//
//  FilterCategoryData.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 8. 28..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation

final class FilterCategoryData_link : SuperModel {
    
    var href: String?
    var rel: String?
    var dataType: String?
}

final class FilterCategoryData_children : SuperModel {
    
    var id: String?
    var name: String?
    var listColumn: NSNumber?
}

final class FilterCategoryData : SuperModel {
    
    var id : String?
    var name : String?
    var listColumn : NSNumber?
    var link: FilterCategoryData_link?
    var children : [FilterCategoryData_children]?

    override init(dict: Dictionary<String, Any>) {
        super.init(dict: dict)
        
        if let childList = dict["children"] as? Array<Any>{
            
            self.children  = [FilterCategoryData_children]()
            for var data in childList {
                
                let childData = FilterCategoryData_children.init(dict: data as! [String:Any])
                self.children?.append(childData)
            }
        }
        
        if let data = dict["link"] as? [String: Any]{
            self.link = FilterCategoryData_link.init(dict: data)
        }
        
        var i = 0
    }
}
