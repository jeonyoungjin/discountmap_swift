                                                                                                                                                                                                                                                                                                                                                                                                                                                      //
//  LocationManager.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 8. 25..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import INTULocationManager

public typealias ResponseHandler = (Bool) -> ()

final class LocationManager {
    
    static let shared = LocationManager()
    
    var status : INTULocationStatus?
    
    var currentLocation : CLLocation?
    var manualLocation  : CLLocation?

    var isInitAddress = false
    var currentAddress  : String? {
        
        didSet(oldVal) {

            if currentAddress != nil, currentAddress != "", isInitAddress == true, manualAddress == nil {
                TrackingManager.E_1710120015(label: currentAddress!)
            }
            
            isInitAddress = true
        }
    }
    var manualAddress   : String? {
        didSet(oldVal) {
            
            if manualAddress != nil, manualAddress != "", isInitAddress == true {
                TrackingManager.E_1710120015(label: manualAddress!)
            }
            
            isInitAddress = true
        }
    }
    
    var forceAddress : String?
    
    private init() {}
        
    class func getIsCanReadGPSStatus() -> Bool {
        return INTULocationManager.locationServicesState() == .available
    }
    
    class func isExistManualLocation() -> Bool {
        return shared.manualLocation != nil ? true : false
    }
    
    class func autoLocation() -> CLLocation? {
        return shared.manualLocation ?? shared.currentLocation  
    }
    
    class func autoAddress() -> String? {
        return shared.manualAddress ?? shared.currentAddress
    }
    
    class func setManualLocation(location : CLLocation?, forceAddressName : String?, completion : (() -> ())?) {
        
        guard location != nil else {
            
            if isExistManualLocation() {
                TrackingManager.E_1710120015(label: shared.currentAddress ?? "")
            }
            
            shared.manualLocation = nil
            shared.manualAddress = nil

            return
        }
        
        shared.manualLocation = location
        
        if forceAddressName != nil {
            shared.manualAddress = forceAddressName
        } else {
            getAddressString(location: location!) { (address) in
                shared.manualAddress = forceAddressName ?? address
                
                completion?()
            }
        }
    }
    
    class func initLocationWithResponse(delayUntilAuthrized: Bool = false, response :ResponseHandler?) {
        
        INTULocationManager.sharedInstance().requestLocation(withDesiredAccuracy: .room,
                                                             timeout: 1, delayUntilAuthorized: delayUntilAuthrized) { (currentLocation, achievedAccuracy, status) in
                                                                
                                                                shared.status = status
                                                                shared.currentLocation = currentLocation
                                                                
                                                                if getIsCanReadGPSStatus() == true {
                                                                    
                                                                    getAddressString(location: currentLocation!, completion: { (address) in
                                                                        shared.currentAddress = address
                                                                        
                                                                        response?(true)
                                                                        
                                                                    })
                                                                }
                                                                
                                                                else if status == .servicesDenied {
                                                                    
//                                                                    guard delayUntilAuthrized == true else {
//                                                                        response?(false)
////                                                                        return
//                                                                    }
                                                                    
                                                                    if delayUntilAuthrized == false {
                                                                        response?(false)
                                                                    }
                                                                    
                                                                    struct Temp {
                                                                        static var isShowAlert = false
                                                                    }
                                                                    
                                                                    if Temp.isShowAlert == true {
                                                                        return
                                                                    }
                                                                    
                                                                    if let vc = UIApplication.topViewController() {
                                                                        
                                                                        vc.showConfirmAlert(message: "GPS가 꺼져있습니다.\n위치서비스를 사용하시려면 GPS를 켜주세요",
                                                                                            positiveHandler: {
                                                                                                Temp.isShowAlert = false
                                                                                                UIApplication.shared.openURL(URL.init(string: UIApplicationOpenSettingsURLString)!)
                                                                                                response?(false)
                                                                                                Temp.isShowAlert = false
                                                                        },
                                                                                            negativeHandler: {
                                                                                                response?(false)
                                                                                                Temp.isShowAlert = false
                                                                        })
                                                                 
                                                                        Temp.isShowAlert = true
                                                                    }
                                                                }
                                                                else {
                                                                    response?(false)
                                                                }
        }
    }
    
    class func getAddressString(location : CLLocation, completion : ((String) -> ())?) {
        
        let geo = CLGeocoder()
        
        geo.reverseGeocodeLocation(location) { (placemarks, error) in
            
            guard let current = placemarks?.first else {
                
                completion?("")
                return
            }
            
            
            var trimLocality = ""
            
            if let locality = current.locality {
                trimLocality = "\(locality) \(current.subLocality ?? "")"
                
                completion?(trimLocality)
                
            } else {
                completion?("")
            }
        }
    }
}
