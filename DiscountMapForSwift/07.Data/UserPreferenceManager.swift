//
//  UserPreferenceManager.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 19..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation


class UserPreferenceManager {
    
    static let shared = UserPreferenceManager()
    private init() {}
    
    class func getSplashImageInfo() -> [String:String]? {
        
        return UserDefaults.standard.dictionary(forKey: "introInfo") as? [String:String]
    }
    
    class func setSplashImageInfo(info: [String:String]) {
    
        UserDefaults.standard.set(info, forKey: "introInfo")
        UserDefaults.standard.synchronize()
    }
    
    class func isShowTutorial() ->Bool {
        
        let isShowTutorial = UserDefaults.standard.bool(forKey: "tutorial")
        
        if isShowTutorial == false {
            
            UserDefaults.standard.set(true, forKey: "tutorial")
            UserDefaults.standard.synchronize()
        }
        
        return isShowTutorial
    }
    
    class func isShowApnsPopup() ->Bool {
        
        let isShowTutorial = UserDefaults.standard.bool(forKey: "apnspopup")
        if isShowTutorial == false {
            
            UserDefaults.standard.set(true, forKey: "apnspopup")
            UserDefaults.standard.synchronize()
        }
        
        return isShowTutorial
    }

    
    class func getAccessToken() -> String? {
    
        return UserDefaults.standard.string(forKey: "accessToken") as String?
    }
    
    class func setAccessToken(token: String?) {
    
        UserDefaults.standard.setValue(token, forKeyPath: "accessToken")
        UserDefaults.standard.synchronize()
    }
    
    class func getDeviceId() -> String? {
        if let token = UserDefaults.standard.string(forKey: "deviceId") {
            return token
        }
        
        let token = UUID().uuidString
        UserDefaults.standard.set(token, forKey: "deviceId")
        UserDefaults.standard.synchronize()

        return UserDefaults.standard.string(forKey: "deviceId")
    }
    
    class func getApnsStatus() -> Bool {
        
        return UserDefaults.standard.bool(forKey: "apnsStatus")
    }
    
    class func setApnsStatus(isOn: Bool) {
        
        UserDefaults.standard.set(isOn, forKey: "apnsStatus")
        UserDefaults.standard.synchronize()
    }
}
