//
//  DataManager.swift
//  DiscountMapForSwift
//
//  Created by we on 2017. 9. 19..
//  Copyright © 2017년 Young jin Jeon. All rights reserved.
//

import Foundation
import SDWebImage

class DataManager {
    
    static let shared = DataManager()
    private init() {}
    
//    var categoryDealList    = [CategoryDealData]()
    var filterCategory      = [FilterCategoryData]()
    var filterHashTag       = [FilterTagData]()
    var markerList          = [MarkerData]()
    var gnbList             = [GNBData]()
    var hotPlaceList        = [HotPlaceData]()
    var serverLocation      : CLLocationCoordinate2D?
    var recommendData       : RecommendData?
    var myData              : MyData?

    
   
//    final func getCategoryDealList() {
//        
//    }
//    
//    final func setCategoryDeal(list: [CategoryDealData]) {
//        
//    }
    
//    final func
    
    final class func setGNBList(list: [GNBData]) {
        DataManager.shared.gnbList = list
    }
    
    final class func getGNBList() -> [GNBData] {
        return DataManager.shared.gnbList
    }
    
    final class func setHotPlaceList(list: [HotPlaceData]) {
        DataManager.shared.hotPlaceList = list
    }
    
    final class func getHotPlaceList() -> [HotPlaceData] {
        return DataManager.shared.hotPlaceList
    }
    
    final class func setMarkerList(list: [MarkerData]) {
        DataManager.shared.markerList = list
        
        let onImages =  list.map{  ($0.icons?.off)! }
        let offImages =  list.map{ ($0.icons?.on)!  }
        
        var prefetchList = [String]()
        prefetchList.append(contentsOf: onImages)
        prefetchList.append(contentsOf: offImages)
        
        for urlString in prefetchList {
            
            SDWebImageManager.shared().imageDownloader?.downloadImage(with: URL.init(string: urlString),
                                                                      options: .highPriority,
                                                                      progress: nil,
                                                                      completed: { (image, data, error, finished) in
                                                                        if let img = image {
                                                                            SDImageCache.shared().store(img, forKey: urlString, completion: nil)
                                                                        }
            })
        }
    }
    
    final class func getMarkerList() -> [MarkerData] {
        return DataManager.shared.markerList
    }
    
    final class func setFilterCategory(list: [FilterCategoryData]) {
        DataManager.shared.filterCategory = list
    }
    
    final class func getFilterCategory() -> [FilterCategoryData] {
        return DataManager.shared.filterCategory
    }
    
    final class func setFilterHashTag(list: [FilterTagData]) {
        DataManager.shared.filterHashTag = list
    }
    
    final class func getFilterHashTag() -> [FilterTagData] {
        return DataManager.shared.filterHashTag
    }
    
    final class func setRecommendData(list: RecommendData?) {
        DataManager.shared.recommendData = list
    }
    
    final class func getRecommendData() -> RecommendData? {
        return DataManager.shared.recommendData
    }
    
    final class func setMyData(data: MyData?) {
        DataManager.shared.myData = data
    }
    
    final class func getMyData() -> MyData? {
        return DataManager.shared.myData
    }

}
